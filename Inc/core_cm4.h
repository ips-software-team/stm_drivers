/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : core_cm4.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 01-09-2021
**
** Description: Cortex-M4 Core Peripheral Access Layer Header File
**
*/


/* Revision Log:
**
** Rev 1.0  : 04-11-2020 -- Original version created.
** Rev 1.1  : 01-01-2021 -- Updated due to verification remarks
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
** Rev 1.3  : 01-09-2021 -- Updated due to verification remarks
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CORE_CM4_H
#define CORE_CM4_H


/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_stdint.h"/*replaced stdint.h*/
#include "cmsis_gcc.h"


/* -----Definitions ----------------------------------------------------------------*/



/* IO definitions (access restrictions to peripheral registers) */
#define     __I     volatile const       /*!< Defines 'read only' permissions */
#define     __O     volatile             /*!< Defines 'write only' permissions */
#define     __IO    volatile             /*!< Defines 'read / write' permissions */

/* following defines should be used for structure members */
#define     __IM     volatile const      /*! Defines 'read only' structure member permissions */
#define     __OM     volatile            /*! Defines 'write only' structure member permissions */
#define     __IOM    volatile            /*! Defines 'read / write' structure member permissions */

/* SCB Application Interrupt and Reset Control Register Definitions */
#define SCB_AIRCR_VECTKEY_POS              16U                                            /*!< SCB AIRCR: VECTKEY POSition */
#define SCB_AIRCR_VECTKEY_MSK              (0xFFFFUL << SCB_AIRCR_VECTKEY_POS)            /*!< SCB AIRCR: VECTKEY Mask */

#define SCB_AIRCR_PRIGROUP_POS              8U                                            /*!< SCB AIRCR: PRIGROUP POSition */
#define SCB_AIRCR_PRIGROUP_MSK             (7UL << SCB_AIRCR_PRIGROUP_POS)                /*!< SCB AIRCR: PRIGROUP Mask */

#define SCB_AIRCR_SYSRESETREQ_POS           2U                                            /*!< SCB AIRCR: SYSRESETREQ POSition */
#define SCB_AIRCR_SYSRESETREQ_MSK          (1UL << SCB_AIRCR_SYSRESETREQ_POS)             /*!< SCB AIRCR: SYSRESETREQ Mask */


/* SysTick Control / Status Register Definitions */
#define SysTick_CTRL_COUNTFLAG_POS         16U                                            /*!< SysTick CTRL: COUNTFLAG POSition */
#define SysTick_CTRL_COUNTFLAG_MSK         (1UL << SysTick_CTRL_COUNTFLAG_POS)            /*!< SysTick CTRL: COUNTFLAG Mask */

#define SysTick_CTRL_CLKSOURCE_POS          2U                                            /*!< SysTick CTRL: CLKSOURCE POSition */
#define SysTick_CTRL_CLKSOURCE_MSK         (1UL << SysTick_CTRL_CLKSOURCE_POS)            /*!< SysTick CTRL: CLKSOURCE Mask */

#define SysTick_CTRL_TICKINT_POS            1U                                            /*!< SysTick CTRL: TICKINT POSition */
#define SysTick_CTRL_TICKINT_MSK           (1UL << SysTick_CTRL_TICKINT_POS)              /*!< SysTick CTRL: TICKINT Mask */

#define SysTick_CTRL_ENABLE_POS             0U                                            /*!< SysTick CTRL: ENABLE POSition */
#define SysTick_CTRL_ENABLE_MSK            (1UL /*<< SysTick_CTRL_ENABLE_POS*/)           /*!< SysTick CTRL: ENABLE Mask */

/* SysTick Reload Register Definitions */
#define SysTick_LOAD_RELOAD_POS             0U                                            /*!< SysTick LOAD: RELOAD POSition */
#define SysTick_LOAD_RELOAD_MSK            (0xFFFFFFUL /*<< SysTick_LOAD_RELOAD_POS*/)    /*!< SysTick LOAD: RELOAD Mask */

/* Memory mapping of Cortex-M4 Hardware */
#define SCS_BASE            (0xE000E000UL)                            /*!< System Control Space Base Address */
#define ITM_BASE            (0xE0000000UL)                            /*!< ITM Base Address */
#define DWT_BASE            (0xE0001000UL)                            /*!< DWT Base Address */
#define TPI_BASE            (0xE0040000UL)                            /*!< TPI Base Address */
#define CoreDebug_BASE      (0xE000EDF0UL)                            /*!< Core Debug Base Address */
#define SysTick_BASE        (SCS_BASE +  0x0010UL)                    /*!< SysTick Base Address */
#define NVIC_BASE           (SCS_BASE +  0x0100UL)                    /*!< NVIC Base Address */
#define SCB_BASE            (SCS_BASE +  0x0D00UL)                    /*!< System Control Block Base Address */

#define SCnSCB              ((SCnSCB_Type    *)     SCS_BASE      )   /*!< System control Register not in SCB */
#define SCB                 ((SCB_Type       *)     SCB_BASE      )   /*!< SCB configuration struct */
#define SysTick             ((SysTick_Type   *)     SysTick_BASE  )   /*!< SysTick configuration struct */
#define NVIC                ((NVIC_Type      *)     NVIC_BASE     )   /*!< NVIC configuration struct */
#define ITM                 ((ITM_Type       *)     ITM_BASE      )   /*!< ITM configuration struct */
#define DWT                 ((DWT_Type       *)     DWT_BASE      )   /*!< DWT configuration struct */
#define TPI                 ((TPI_Type       *)     TPI_BASE      )   /*!< TPI configuration struct */
#define CoreDebug           ((CoreDebug_Type *)     CoreDebug_BASE)   /*!< Core Debug configuration struct */
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Structure type to access the Nested Vectored Interrupt Controller (NVIC)*/
typedef struct
{
  __IOM uint32_t ISER[8U];               /*!< Offset: 0x000 (R/W)  Interrupt Set Enable Register (Range: var type | Unit: None)*/
        uint32_t RESERVED0[24U];         /*!< Reserved registers (Range: var type | Unit: None)*/
  __IOM uint32_t ICER[8U];               /*!< Offset: 0x080 (R/W)  Interrupt Clear Enable Register (Range: var type | Unit: None)*/
        uint32_t RSERVED1[24U];          /*!< Reserved registers (Range: var type | Unit: None)*/
  __IOM uint32_t ISPR[8U];               /*!< Offset: 0x100 (R/W)  Interrupt Set Pending Register (Range: var type | Unit: None)*/
        uint32_t RESERVED2[24U];         /*!< Reserved registers (Range: var type | Unit: None)*/
  __IOM uint32_t ICPR[8U];               /*!< Offset: 0x180 (R/W)  Interrupt Clear Pending Register (Range: var type | Unit: None)*/
        uint32_t RESERVED3[24U];         /*!< Reserved registers (Range: var type | Unit: None)*/
  __IOM uint32_t IABR[8U];               /*!< Offset: 0x200 (R/W)  Interrupt Active bit Register (Range: var type | Unit: None)*/
        uint32_t RESERVED4[56U];		 /*!< Reserved registers (Range: var type | Unit: None)*/
  __IOM uint8_t  IP[240U];               /*!< Offset: 0x300 (R/W)  Interrupt Priority Register (8Bit wide) (Range: var type | Unit: None)*/
        uint32_t RESERVED5[644U];		 /*!< Reserved registers (Range: var type | Unit: None)*/
  __OM  uint32_t STIR;                   /*!< Offset: 0xE00 ( /W)  Software Trigger Interrupt Register (Range: var type | Unit: None)*/
}  NVIC_Type;



/*! Structure type to access the System Control Block (SCB)*/
typedef struct
{
  __IM  uint32_t CPUID;                  /*!< Offset: 0x000 (R/ )  CPUID Base Register (Range: var type | Unit: None)*/
  __IOM uint32_t ICSR;                   /*!< Offset: 0x004 (R/W)  Interrupt Control and State Register (Range: var type | Unit: None)*/
  __IOM uint32_t VTOR;                   /*!< Offset: 0x008 (R/W)  Vector Table Offset Register (Range: var type | Unit: None)*/
  __IOM uint32_t AIRCR;                  /*!< Offset: 0x00C (R/W)  Application Interrupt and Reset Control Register (Range: var type | Unit: None)*/
  __IOM uint32_t SCR;                    /*!< Offset: 0x010 (R/W)  System Control Register (Range: var type | Unit: None)*/
  __IOM uint32_t CCR;                    /*!< Offset: 0x014 (R/W)  Configuration Control Register (Range: var type | Unit: None)*/
  __IOM uint8_t  SHP[12U];               /*!< Offset: 0x018 (R/W)  System Handlers Priority Registers (4-7, 8-11, 12-15) (Range: var type | Unit: None)*/
  __IOM uint32_t SHCSR;                  /*!< Offset: 0x024 (R/W)  System Handler Control and State Register (Range: var type | Unit: None)*/
  __IOM uint32_t CFSR;                   /*!< Offset: 0x028 (R/W)  Configurable Fault Status Register (Range: var type | Unit: None)*/
  __IOM uint32_t HFSR;                   /*!< Offset: 0x02C (R/W)  HardFault Status Register (Range: var type | Unit: None)*/
  __IOM uint32_t DFSR;                   /*!< Offset: 0x030 (R/W)  Debug Fault Status Register (Range: var type | Unit: None)*/
  __IOM uint32_t MMFAR;                  /*!< Offset: 0x034 (R/W)  MemManage Fault Address Register (Range: var type | Unit: None)*/
  __IOM uint32_t BFAR;                   /*!< Offset: 0x038 (R/W)  BusFault Address Register (Range: var type | Unit: None)*/
  __IOM uint32_t AFSR;                   /*!< Offset: 0x03C (R/W)  Auxiliary Fault Status Register (Range: var type | Unit: None)*/
  __IM  uint32_t PFR[2U];                /*!< Offset: 0x040 (R/ )  Processor Feature Register (Range: var type | Unit: None)*/
  __IM  uint32_t DFR;                    /*!< Offset: 0x048 (R/ )  Debug Feature Register (Range: var type | Unit: None)*/
  __IM  uint32_t ADR;                    /*!< Offset: 0x04C (R/ )  Auxiliary Feature Register (Range: var type | Unit: None)*/
  __IM  uint32_t MMFR[4U];               /*!< Offset: 0x050 (R/ )  Memory Model Feature Register (Range: var type | Unit: None)*/
  __IM  uint32_t ISAR[5U];               /*!< Offset: 0x060 (R/ )  Instruction Set Attributes Register (Range: var type | Unit: None)*/
        uint32_t RESERVED0[5U];			 /*!< Reserved registers (Range: var type | Unit: None)*/
  __IOM uint32_t CPACR;                  /*!< Offset: 0x088 (R/W)  Coprocessor Access Control Register (Range: var type | Unit: None)*/
} SCB_Type;

/*! Structure type to access the System Timer (SysTick)*/
typedef struct
{
  __IOM uint32_t CTRL;                   /*!< Offset: 0x000 (R/W)  SysTick Control and Status Register (Range: var type | Unit: None)*/
  __IOM uint32_t LOAD;                   /*!< Offset: 0x004 (R/W)  SysTick Reload Value Register (Range: var type | Unit: None)*/
  __IOM uint32_t VAL;                    /*!< Offset: 0x008 (R/W)  SysTick Current Value Register (Range: var type | Unit: None)*/
  __IM  uint32_t CALIB;                  /*!< Offset: 0x00C (R/ )  SysTick Calibration Register (Range: var type | Unit: None)*/
} SysTick_Type;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

#endif /* CORE_CM4_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
