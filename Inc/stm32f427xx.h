/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : stm32f427xx.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 01-09-2021
**
** Description: STM32F427xx Device Peripheral Access Layer Header File.
**
*/


/* Revision Log:
**
** Rev 1.0  : 04-11-2020 -- Original version created.
** Rev 1.1  : 20-11-2020 -- Static analysis update
** Rev 1.2  : 01-01-2021 -- Updated due to verification remarks
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
** Rev 1.4  : 01-09-2021 -- Updated due to verification remarks
*************************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef STM32F427XX_H
#define STM32F427XX_H
/* -----Includes -------------------------------------------------------------------*/
#include "core_cm4.h"

/* -----Definitions ----------------------------------------------------------------*/
/*Peripheral_memory_map*/
#define FLASH_BASE            0x08000000U /*!< FLASH(up to 2 MB) base address in the alias region                         */
#define CCMDATARAM_BASE       0x10000000U /*!< CCM(core coupled memory) data RAM(64 KB) base address in the alias region  */
#define SRAM1_BASE            0x20000000U /*!< SRAM1(112 KB) base address in the alias region                              */
#define SRAM2_BASE            0x2001C000U /*!< SRAM2(16 KB) base address in the alias region                              */
#define PERIPH_BASE           0x40000000U /*!< Peripheral base address in the alias region                                */
#define BKPSRAM_BASE          0x40024000U /*!< Backup SRAM(4 KB) base address in the alias region                         */
#define FMC_R_BASE            0xA0000000U /*!< FMC registers base address                                                 */
#define SRAM1_BB_BASE         0x22000000U /*!< SRAM1(112 KB) base address in the bit-band region                          */
#define SRAM2_BB_BASE         0x22380000U /*!< SRAM2(16 KB) base address in the bit-band region                           */
#define PERIPH_BB_BASE        0x42000000U /*!< Peripheral base address in the bit-band region                             */
#define BKPSRAM_BB_BASE       0x42480000U /*!< Backup SRAM(4 KB) base address in the bit-band region                      */
#define FLASH_END             0x081FFFFFU /*!< FLASH end address                                                          */
#define FLASH_OTP_BASE        0x1FFF7800U /*!< Base address of : (up to 528 Bytes) embedded FLASH OTP Area                */
#define FLASH_OTP_END         0x1FFF7A0FU /*!< End address of : (up to 528 Bytes) embedded FLASH OTP Area                 */
#define CCMDATARAM_END        0x1000FFFFU /*!< CCM data RAM end address                                                   */

/* Legacy defines */
#define SRAM_BASE             SRAM1_BASE
#define SRAM_BB_BASE          SRAM1_BB_BASE

/*!< Peripheral memory map */
#define APB1PERIPH_BASE       PERIPH_BASE
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x00010000U)
#define AHB1PERIPH_BASE       (PERIPH_BASE + 0x00020000U)
#define AHB2PERIPH_BASE       (PERIPH_BASE + 0x10000000U)

/*!< APB1 peripherals */
#define TIM2_BASE             (APB1PERIPH_BASE + 0x0000U)
#define TIM3_BASE             (APB1PERIPH_BASE + 0x0400U)
#define TIM4_BASE             (APB1PERIPH_BASE + 0x0800U)
#define RTC_BASE              (APB1PERIPH_BASE + 0x2800U)
#define USART3_BASE           (APB1PERIPH_BASE + 0x4800U)
#define I2C1_BASE             (APB1PERIPH_BASE + 0x5400U)
#define I2C2_BASE             (APB1PERIPH_BASE + 0x5800U)
#define I2C3_BASE             (APB1PERIPH_BASE + 0x5C00U)
#define CAN1_BASE             (APB1PERIPH_BASE + 0x6400U)
#define PWR_BASE              (APB1PERIPH_BASE + 0x7000U)

/*!< APB2 peripherals */
#define TIM1_BASE             (APB2PERIPH_BASE + 0x0000U)
#define TIM8_BASE             (APB2PERIPH_BASE + 0x0400U)
#define USART1_BASE           (APB2PERIPH_BASE + 0x1000U)
#define ADC1_BASE             (APB2PERIPH_BASE + 0x2000U)
#define ADC123_COMMON_BASE    (APB2PERIPH_BASE + 0x2300U)



/*!< AHB1 peripherals */
#define GPIOA_BASE            (AHB1PERIPH_BASE + 0x0000U)
#define GPIOB_BASE            (AHB1PERIPH_BASE + 0x0400U)
#define GPIOC_BASE            (AHB1PERIPH_BASE + 0x0800U)
#define GPIOD_BASE            (AHB1PERIPH_BASE + 0x0C00U)
#define GPIOE_BASE            (AHB1PERIPH_BASE + 0x1000U)
#define CRC_BASE              (AHB1PERIPH_BASE + 0x3000U)
#define RCC_BASE              (AHB1PERIPH_BASE + 0x3800U)
#define FLASH_R_BASE          (AHB1PERIPH_BASE + 0x3C00U)



/*Peripheral_declaration*/
#define TIM2                ((TIM_TypeDef *) TIM2_BASE)
#define TIM3                ((TIM_TypeDef *) TIM3_BASE)
#define TIM4                ((TIM_TypeDef *) TIM4_BASE)
#define RTC                 ((RTC_TypeDef *) RTC_BASE)
#define USART3              ((USART_TypeDef *) USART3_BASE)
#define I2C1                ((I2C_TypeDef *) I2C1_BASE)
#define I2C2                ((I2C_TypeDef *) I2C2_BASE)
#define I2C3                ((I2C_TypeDef *) I2C3_BASE)
#define CAN1                ((CAN_TypeDef *) CAN1_BASE)
#define PWR                 ((PWR_TypeDef *) PWR_BASE)
#define USART1              ((USART_TypeDef *) USART1_BASE)
#define ADC1                ((ADC_TypeDef *) ADC1_BASE)
#define ADC123_COMMON       ((ADC_Common_TypeDef *) ADC123_COMMON_BASE)
/* Legacy define */

#define GPIOA               ((GPIO_TypeDef *) GPIOA_BASE)
#define GPIOB               ((GPIO_TypeDef *) GPIOB_BASE)
#define GPIOC               ((GPIO_TypeDef *) GPIOC_BASE)
#define GPIOD               ((GPIO_TypeDef *) GPIOD_BASE)
#define GPIOE               ((GPIO_TypeDef *) GPIOE_BASE)
#define CRC                 ((CRC_TypeDef *) CRC_BASE)
#define RCC                 ((RCC_TypeDef *) RCC_BASE)
#define FLASH               ((FLASH_TypeDef *) FLASH_R_BASE)

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for STM32F4XX Interrupt Number Definition*/
typedef enum
{
/******  Cortex-M4 Processor Exceptions Numbers ****************************************************************/
  NonMaskableInt_IRQn         = -14,    /*!<Val: -14 - 2 Non Maskable Interrupt                                          */
  MemoryManagement_IRQn       = -12,    /*!<Val: -12 - 4 Cortex-M4 Memory Management Interrupt                           */
  BusFault_IRQn               = -11,    /*!<Val: -11 - 5 Cortex-M4 Bus Fault Interrupt                                   */
  UsageFault_IRQn             = -10,    /*!<Val: -10 - 6 Cortex-M4 Usage Fault Interrupt                                 */
  SVCall_IRQn                 = -5,     /*!<Val: -5  - 11 Cortex-M4 SV Call Interrupt                                    */
  DebugMonitor_IRQn           = -4,     /*!<Val: -4  - 12 Cortex-M4 Debug Monitor Interrupt                              */
  PendSV_IRQn                 = -2,     /*!<Val: -2  - 14 Cortex-M4 Pend SV Interrupt                                    */
  SysTick_IRQn                = -1,     /*!<Val: -1  - 15 Cortex-M4 System Tick Interrupt                                */
/******  STM32 specific Interrupt Numbers **********************************************************************/
  WWDG_IRQn                   = 0,      /*!<Val: 00 - Window WatchDog Interrupt                                         */
  PVD_IRQn                    = 1,      /*!<Val: 01 - PVD through EXTI Line detection Interrupt                         */
  TAMP_STAMP_IRQn             = 2,      /*!<Val: 02 - Tamper and TimeStamp interrupts through the EXTI line             */
  RTC_WKUP_IRQn               = 3,      /*!<Val: 03 - RTC Wakeup interrupt through the EXTI line                        */
  FLASH_IRQn                  = 4,      /*!<Val: 04 - FLASH global Interrupt                                            */
  RCC_IRQn                    = 5,      /*!<Val: 05 - RCC global Interrupt                                              */
  EXTI0_IRQn                  = 6,      /*!<Val: 06 - EXTI Line0 Interrupt                                              */
  EXTI1_IRQn                  = 7,      /*!<Val: 07 - EXTI Line1 Interrupt                                              */
  EXTI2_IRQn                  = 8,      /*!<Val: 08 - EXTI Line2 Interrupt                                              */
  EXTI3_IRQn                  = 9,      /*!<Val: 09 - EXTI Line3 Interrupt                                              */
  EXTI4_IRQn                  = 10,     /*!<Val: 10 - EXTI Line4 Interrupt                                              */
  DMA1_Stream0_IRQn           = 11,     /*!<Val: 11 - DMA1 Stream 0 global Interrupt                                    */
  DMA1_Stream1_IRQn           = 12,     /*!<Val: 12 - DMA1 Stream 1 global Interrupt                                    */
  DMA1_Stream2_IRQn           = 13,     /*!<Val: 13 - DMA1 Stream 2 global Interrupt                                    */
  DMA1_Stream3_IRQn           = 14,     /*!<Val: 14 - DMA1 Stream 3 global Interrupt                                    */
  DMA1_Stream4_IRQn           = 15,     /*!<Val: 15 - DMA1 Stream 4 global Interrupt                                    */
  DMA1_Stream5_IRQn           = 16,     /*!<Val: 16 - DMA1 Stream 5 global Interrupt                                    */
  DMA1_Stream6_IRQn           = 17,     /*!<Val: 17 - DMA1 Stream 6 global Interrupt                                    */
  ADC_IRQn                    = 18,     /*!<Val: 18 - ADC1, ADC2 and ADC3 global Interrupts                             */
  CAN1_TX_IRQn                = 19,     /*!<Val: 19 - CAN1 TX Interrupt                                                 */
  CAN1_RX0_IRQn               = 20,     /*!<Val: 20 - CAN1 RX0 Interrupt                                                */
  CAN1_RX1_IRQn               = 21,     /*!<Val: 21 - CAN1 RX1 Interrupt                                                */
  CAN1_SCE_IRQn               = 22,     /*!<Val: 22 - CAN1 SCE Interrupt                                                */
  EXTI9_5_IRQn                = 23,     /*!<Val: 23 - External Line[9:5] Interrupts                                     */
  TIM1_BRK_TIM9_IRQn          = 24,     /*!<Val: 24 - TIM1 Break interrupt and TIM9 global interrupt                    */
  TIM1_UP_TIM10_IRQn          = 25,     /*!<Val: 25 - TIM1 Update Interrupt and TIM10 global interrupt                  */
  TIM1_TRG_COM_TIM11_IRQn     = 26,     /*!<Val: 26 - TIM1 Trigger and Commutation Interrupt and TIM11 global interrupt */
  TIM1_CC_IRQn                = 27,     /*!<Val: 27 - TIM1 Capture Compare Interrupt                                    */
  TIM2_IRQn                   = 28,     /*!<Val: 28 - TIM2 global Interrupt                                             */
  TIM3_IRQn                   = 29,     /*!<Val: 29 - TIM3 global Interrupt                                             */
  TIM4_IRQn                   = 30,     /*!<Val: 30 - TIM4 global Interrupt                                             */
  I2C1_EV_IRQn                = 31,     /*!<Val: 31 - I2C1 Event Interrupt                                              */
  I2C1_ER_IRQn                = 32,     /*!<Val: 32 - I2C1 Error Interrupt                                              */
  I2C2_EV_IRQn                = 33,     /*!<Val: 33 - I2C2 Event Interrupt                                              */
  I2C2_ER_IRQn                = 34,     /*!<Val: 34 - I2C2 Error Interrupt                                              */
  SPI1_IRQn                   = 35,     /*!<Val: 35 - SPI1 global Interrupt                                             */
  SPI2_IRQn                   = 36,     /*!<Val: 36 - SPI2 global Interrupt                                             */
  USART1_IRQn                 = 37,     /*!<Val: 37 - USART1 global Interrupt                                           */
  USART2_IRQn                 = 38,     /*!<Val: 38 - USART2 global Interrupt                                           */
  USART3_IRQn                 = 39,     /*!<Val: 39 - USART3 global Interrupt                                           */
  EXTI15_10_IRQn              = 40,     /*!<Val: 40 - External Line[15:10] Interrupts                                   */
  RTC_Alarm_IRQn              = 41,     /*!<Val: 41 - RTC Alarm (A and B) through EXTI Line Interrupt                   */
  OTG_FS_WKUP_IRQn            = 42,     /*!<Val: 42 - USB OTG FS Wakeup through EXTI line interrupt                     */
  TIM8_BRK_TIM12_IRQn         = 43,     /*!<Val: 43 - TIM8 Break Interrupt and TIM12 global interrupt                   */
  TIM8_UP_TIM13_IRQn          = 44,     /*!<Val: 44 - TIM8 Update Interrupt and TIM13 global interrupt                  */
  TIM8_TRG_COM_TIM14_IRQn     = 45,     /*!<Val: 45 - TIM8 Trigger and Commutation Interrupt and TIM14 global interrupt */
  TIM8_CC_IRQn                = 46,     /*!<Val: 46 - TIM8 Capture Compare global interrupt                             */
  DMA1_Stream7_IRQn           = 47,     /*!<Val: 47 - DMA1 Stream7 Interrupt                                            */
  FMC_IRQn                    = 48,     /*!<Val: 48 - FMC global Interrupt                                              */
  SDIO_IRQn                   = 49,     /*!<Val: 49 - SDIO global Interrupt                                             */
  TIM5_IRQn                   = 50,     /*!<Val: 50 - TIM5 global Interrupt                                             */
  SPI3_IRQn                   = 51,     /*!<Val: 51 - SPI3 global Interrupt                                             */
  UART4_IRQn                  = 52,     /*!<Val: 52 - UART4 global Interrupt                                            */
  UART5_IRQn                  = 53,     /*!<Val: 53 - UART5 global Interrupt                                            */
  TIM6_DAC_IRQn               = 54,     /*!<Val: 54 - TIM6 global and DAC1&2 underrun error  interrupts                 */
  TIM7_IRQn                   = 55,     /*!<Val: 55 - TIM7 global interrupt                                             */
  DMA2_Stream0_IRQn           = 56,     /*!<Val: 56 - DMA2 Stream 0 global Interrupt                                    */
  DMA2_Stream1_IRQn           = 57,     /*!<Val: 57 - DMA2 Stream 1 global Interrupt                                    */
  DMA2_Stream2_IRQn           = 58,     /*!<Val: 58 - DMA2 Stream 2 global Interrupt                                    */
  DMA2_Stream3_IRQn           = 59,     /*!<Val: 59 - DMA2 Stream 3 global Interrupt                                    */
  DMA2_Stream4_IRQn           = 60,     /*!<Val: 60 - DMA2 Stream 4 global Interrupt                                    */
  ETH_IRQn                    = 61,     /*!<Val: 61 - Ethernet global Interrupt                                         */
  ETH_WKUP_IRQn               = 62,     /*!<Val: 62 - Ethernet Wakeup through EXTI line Interrupt                       */
  CAN2_TX_IRQn                = 63,     /*!<Val: 63 - CAN2 TX Interrupt                                                 */
  CAN2_RX0_IRQn               = 64,     /*!<Val: 64 - CAN2 RX0 Interrupt                                                */
  CAN2_RX1_IRQn               = 65,     /*!<Val: 65 - CAN2 RX1 Interrupt                                                */
  CAN2_SCE_IRQn               = 66,     /*!<Val: 66 - CAN2 SCE Interrupt                                                */
  OTG_FS_IRQn                 = 67,     /*!<Val: 67 - USB OTG FS global Interrupt                                       */
  DMA2_Stream5_IRQn           = 68,     /*!<Val: 68 - DMA2 Stream 5 global interrupt                                    */
  DMA2_Stream6_IRQn           = 69,     /*!<Val: 69 - DMA2 Stream 6 global interrupt                                    */
  DMA2_Stream7_IRQn           = 70,     /*!<Val: 70 - DMA2 Stream 7 global interrupt                                    */
  USART6_IRQn                 = 71,     /*!<Val: 71 - USART6 global interrupt                                           */
  I2C3_EV_IRQn                = 72,     /*!<Val: 72 - I2C3 event interrupt                                              */
  I2C3_ER_IRQn                = 73,     /*!<Val: 73 - I2C3 error interrupt                                              */
  OTG_HS_EP1_OUT_IRQn         = 74,     /*!<Val: 74 - USB OTG HS End Point 1 Out global interrupt                       */
  OTG_HS_EP1_IN_IRQn          = 75,     /*!<Val: 75 - USB OTG HS End Point 1 In global interrupt                        */
  OTG_HS_WKUP_IRQn            = 76,     /*!<Val: 76 - USB OTG HS Wakeup through EXTI interrupt                          */
  OTG_HS_IRQn                 = 77,     /*!<Val: 77 - USB OTG HS global interrupt                                       */
  DCMI_IRQn                   = 78,     /*!<Val: 78 - DCMI global interrupt                                             */
  HASH_RNG_IRQn               = 80,     /*!<Val: 80 - Hash and Rng global interrupt                                     */
  FPU_IRQn                    = 81,     /*!<Val: 81 - FPU global interrupt                                              */
  UART7_IRQn                  = 82,     /*!<Val: 82 - UART7 global interrupt                                            */
  UART8_IRQn                  = 83,     /*!<Val: 83 - UART8 global interrupt                                            */
  SPI4_IRQn                   = 84,     /*!<Val: 84 - SPI4 global Interrupt                                             */
  SPI5_IRQn                   = 85,     /*!<Val: 85 - SPI5 global Interrupt                                             */
  SPI6_IRQn                   = 86,     /*!<Val: 86 - SPI6 global Interrupt                                             */
  SAI1_IRQn                   = 87,     /*!<Val: 87 - SAI1 global Interrupt                                             */
  DMA2D_IRQn                  = 90      /*!<Val: 90 - DMA2D global Interrupt                                            */
} IRQn_Type;



/*-----------Peripheral_registers_structures-----------------------*/

/*! Structure for Analog to Digital Converter*/
typedef struct
{
  __IO uint32_t SR;     /*!< ADC status register,                         Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t CR1;    /*!< ADC control register 1,                      Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t CR2;    /*!< ADC control register 2,                      Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t SMPR1;  /*!< ADC sample time register 1,                  Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t SMPR2;  /*!< ADC sample time register 2,                  Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t JOFR1;  /*!< ADC injected channel data offset register 1, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t JOFR2;  /*!< ADC injected channel data offset register 2, Address offset: 0x18 (Range: var type | Unit: None)*/
  __IO uint32_t JOFR3;  /*!< ADC injected channel data offset register 3, Address offset: 0x1C (Range: var type | Unit: None)*/
  __IO uint32_t JOFR4;  /*!< ADC injected channel data offset register 4, Address offset: 0x20 (Range: var type | Unit: None)*/
  __IO uint32_t HTR;    /*!< ADC watchdog higher threshold register,      Address offset: 0x24 (Range: var type | Unit: None)*/
  __IO uint32_t LTR;    /*!< ADC watchdog lower threshold register,       Address offset: 0x28 (Range: var type | Unit: None)*/
  __IO uint32_t SQR1;   /*!< ADC regular sequence register 1,             Address offset: 0x2C (Range: var type | Unit: None)*/
  __IO uint32_t SQR2;   /*!< ADC regular sequence register 2,             Address offset: 0x30 (Range: var type | Unit: None)*/
  __IO uint32_t SQR3;   /*!< ADC regular sequence register 3,             Address offset: 0x34 (Range: var type | Unit: None)*/
  __IO uint32_t JSQR;   /*!< ADC injected sequence register,              Address offset: 0x38 (Range: var type | Unit: None)*/
  __IO uint32_t JDR1;   /*!< ADC injected data register 1,                Address offset: 0x3C (Range: var type | Unit: None)*/
  __IO uint32_t JDR2;   /*!< ADC injected data register 2,                Address offset: 0x40 (Range: var type | Unit: None)*/
  __IO uint32_t JDR3;   /*!< ADC injected data register 3,                Address offset: 0x44 (Range: var type | Unit: None)*/
  __IO uint32_t JDR4;   /*!< ADC injected data register 4,                Address offset: 0x48 (Range: var type | Unit: None)*/
  __IO uint32_t DR;     /*!< ADC regular data register,                   Address offset: 0x4C (Range: var type | Unit: None)*/
} ADC_TypeDef;

/*! Structure for common Analog to Digital Converter*/
typedef struct
{
  __IO uint32_t CSR;    /*!< ADC Common status register,                  Address offset: ADC1 base address + 0x300 (Range: var type | Unit: None)*/
  __IO uint32_t CCR;    /*!< ADC common control register,                 Address offset: ADC1 base address + 0x304 (Range: var type | Unit: None)*/
  __IO uint32_t CDR;    /*!< ADC common regular data register for dual
                             AND triple modes,                            Address offset: ADC1 base address + 0x308 (Range: var type | Unit: None)*/
} ADC_Common_TypeDef;


/*! Structure for Controller Area Network(CAN) TxMailBox */
typedef struct
{
  __IO uint32_t TIR;  /*!< CAN TX mailbox identifier register (Range: var type | Unit: None)*/
  __IO uint32_t TDTR; /*!< CAN mailbox data length control and time stamp register (Range: var type | Unit: None)*/
  __IO uint32_t TDLR; /*!< CAN mailbox data low register (Range: var type | Unit: None)*/
  __IO uint32_t TDHR; /*!< CAN mailbox data high register (Range: var type | Unit: None)*/
} CAN_TxMailBox_TypeDef;

/*! Structure for Controller Area Network FIFOMailBox*/
typedef struct
{
  __IO uint32_t RIR;  /*!< CAN receive FIFO mailbox identifier register (Range: var type | Unit: None)*/
  __IO uint32_t RDTR; /*!< CAN receive FIFO mailbox data length control and time stamp register (Range: var type | Unit: None)*/
  __IO uint32_t RDLR; /*!< CAN receive FIFO mailbox data low register (Range: var type | Unit: None)*/
  __IO uint32_t RDHR; /*!< CAN receive FIFO mailbox data high register (Range: var type | Unit: None)*/
} CAN_FIFOMailBox_TypeDef;

/*! Structure for Controller Area Network FilterRegister */
typedef struct
{
  __IO uint32_t FR1; /*!< CAN Filter bank register 1 (Range: var type | Unit: None)*/
  __IO uint32_t FR2; /*!< CAN Filter bank register 1 (Range: var type | Unit: None)*/
} CAN_FilterRegister_TypeDef;

/*! Structure for Controller Area Network*/
typedef struct
{
  __IO uint32_t              MCR;                 /*!< CAN master control register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t              MSR;                 /*!< CAN master status register,  Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t              TSR;                 /*!< CAN transmit status register, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t              RF0R;                /*!< CAN receive FIFO 0 register, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t              RF1R;                /*!< CAN receive FIFO 1 register, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t              IER;                 /*!< CAN interrupt enable register, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t              ESR;                 /*!< CAN error status register, Address offset: 0x18 (Range: var type | Unit: None)*/
  __IO uint32_t              BTR;                 /*!< CAN bit timing register, Address offset: 0x1C (Range: var type | Unit: None)*/
  uint32_t                   RESERVED0[88];       /*!< Reserved, 0x020 - 0x17F (Range: var type | Unit: None)*/
  CAN_TxMailBox_TypeDef      sTxMailBox[3];       /*!< CAN Tx MailBox, Address offset: 0x180 - 0x1AC (Range: var type | Unit: None)*/
  CAN_FIFOMailBox_TypeDef    sFIFOMailBox[2];     /*!< CAN FIFO MailBox, Address offset: 0x1B0 - 0x1CC (Range: var type | Unit: None)*/
  uint32_t                   RESERVED1[12];       /*!< Reserved, 0x1D0 - 0x1FF (Range: var type | Unit: None)*/
  __IO uint32_t              FMR;                 /*!< CAN filter master register, Address offset: 0x200 (Range: var type | Unit: None)*/
  __IO uint32_t              FM1R;                /*!< CAN filter mode register, Address offset: 0x204 (Range: var type | Unit: None)*/
  uint32_t                   RESERVED2;           /*!< Reserved, 0x208 (Range: var type | Unit: None)*/
  __IO uint32_t              FS1R;                /*!< CAN filter scale register, Address offset: 0x20C (Range: var type | Unit: None)*/
  uint32_t                   RESERVED3;           /*!< Reserved, 0x210 (Range: var type | Unit: None)*/
  __IO uint32_t              FFA1R;               /*!< CAN filter FIFO assignment register, Address offset: 0x214 (Range: var type | Unit: None)*/
  uint32_t                   RESERVED4;           /*!< Reserved, 0x218 (Range: var type | Unit: None)*/
  __IO uint32_t              FA1R;                /*!< CAN filter activation register, Address offset: 0x21C (Range: var type | Unit: None) */
  uint32_t                   RESERVED5[8];        /*!< Reserved, 0x220-0x23F (Range: var type | Unit: None)*/
  CAN_FilterRegister_TypeDef sFilterRegister[28]; /*!< CAN Filter Register, Address offset: 0x240-0x31C (Range: var type | Unit: None)*/
} CAN_TypeDef;

/*! Structure for CRC calculation unit*/
typedef struct
{
  __IO uint32_t DR;         /*!< CRC Data register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint8_t  IDR;        /*!< CRC Independent data register, Address offset: 0x04 (Range: var type | Unit: None)*/
  uint8_t       RESERVED0;  /*!< Reserved, 0x05 (Range: var type | Unit: None)*/
  uint16_t      RESERVED1;  /*!< Reserved, 0x06 (Range: var type | Unit: None)*/
  __IO uint32_t CR;         /*!< CRC Control register,  Address offset: 0x08 (Range: var type | Unit: None)*/
} CRC_TypeDef;



/*! Structure for FLASH Registers*/
typedef struct
{
  __IO uint32_t ACR;      /*!< FLASH access control register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t KEYR;     /*!< FLASH key register, Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t OPTKEYR;  /*!< FLASH option key register, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t SR;       /*!< FLASH status register, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t CR;       /*!< FLASH control register, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t OPTCR;    /*!< FLASH option control register , Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t OPTCR1;   /*!< FLASH option control register 1, Address offset: 0x18 (Range: var type | Unit: None)*/
} FLASH_TypeDef;


/*! Structure for General Purpose I/O*/

typedef struct
{
  __IO uint32_t MODER;    /*!< GPIO port mode register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t OTYPER;   /*!< GPIO port output type register, Address offset: 0x04  (Range: var type | Unit: None)*/
  __IO uint32_t OSPEEDR;  /*!< GPIO port output speed register, Address offset: 0x08  (Range: var type | Unit: None)*/
  __IO uint32_t PUPDR;    /*!< GPIO port pull-up/pull-down register, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t IDR;      /*!< GPIO port input data register, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t ODR;      /*!< GPIO port output data register, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t BSRR;     /*!< GPIO port bit set/reset register, Address offset: 0x18 (Range: var type | Unit: None)*/
  __IO uint32_t LCKR;     /*!< GPIO port configuration lock register, Address offset: 0x1C (Range: var type | Unit: None)*/
  __IO uint32_t AFR[2];   /*!< GPIO alternate function registers, Address offset: 0x20-0x24 (Range: var type | Unit: None)*/
} GPIO_TypeDef;



/*! Structure for Inter-integrated Circuit(I2C) Interface*/
typedef struct
{
  __IO uint32_t CR1;        /*!< I2C Control register 1,     Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t CR2;        /*!< I2C Control register 2,     Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t OAR1;       /*!< I2C Own address register 1, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t OAR2;       /*!< I2C Own address register 2, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t DR;         /*!< I2C Data register,          Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t SR1;        /*!< I2C Status register 1,      Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t SR2;        /*!< I2C Status register 2,      Address offset: 0x18 (Range: var type | Unit: None)*/
  __IO uint32_t CCR;        /*!< I2C Clock control register, Address offset: 0x1C (Range: var type | Unit: None)*/
  __IO uint32_t TRISE;      /*!< I2C TRISE register,         Address offset: 0x20 (Range: var type | Unit: None)*/
  __IO uint32_t FLTR;       /*!< I2C FLTR register,          Address offset: 0x24 (Range: var type | Unit: None)*/
} I2C_TypeDef;


/*! Structure for Power Control*/
typedef struct
{
  __IO uint32_t CR;   /*!< PWR power control register,        Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t CSR;  /*!< PWR power control/status register, Address offset: 0x04 (Range: var type | Unit: None)*/
} PWR_TypeDef;

/*! Structure for Reset and Clock Control(RCC)*/
typedef struct
{
  __IO uint32_t CR;            /*!< RCC clock control register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t PLLCFGR;       /*!< RCC PLL configuration register, Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t CFGR;          /*!< RCC clock configuration register, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t CIR;           /*!< RCC clock interrupt register, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t AHB1RSTR;      /*!< RCC AHB1 peripheral reset register, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t AHB2RSTR;      /*!< RCC AHB2 peripheral reset register, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t AHB3RSTR;      /*!< RCC AHB3 peripheral reset register, Address offset: 0x18 (Range: var type | Unit: None)*/
  uint32_t      RESERVED0;     /*!< Reserved, 0x1C (Range: var type | Unit: None)*/
  __IO uint32_t APB1RSTR;      /*!< RCC APB1 peripheral reset register, Address offset: 0x20 (Range: var type | Unit: None)*/
  __IO uint32_t APB2RSTR;      /*!< RCC APB2 peripheral reset register, Address offset: 0x24 (Range: var type | Unit: None)*/
  uint32_t      RESERVED1[2];  /*!< Reserved, 0x28-0x2C (Range: var type | Unit: None)*/
  __IO uint32_t AHB1ENR;       /*!< RCC AHB1 peripheral clock register, Address offset: 0x30 (Range: var type | Unit: None)*/
  __IO uint32_t AHB2ENR;       /*!< RCC AHB2 peripheral clock register, Address offset: 0x34 (Range: var type | Unit: None)*/
  __IO uint32_t AHB3ENR;       /*!< RCC AHB3 peripheral clock register, Address offset: 0x38 (Range: var type | Unit: None)*/
  uint32_t      RESERVED2;     /*!< Reserved, 0x3C (Range: var type | Unit: None)*/
  __IO uint32_t APB1ENR;       /*!< RCC APB1 peripheral clock enable register, Address offset: 0x40 (Range: var type | Unit: None)*/
  __IO uint32_t APB2ENR;       /*!< RCC APB2 peripheral clock enable register, Address offset: 0x44 (Range: var type | Unit: None)*/
  uint32_t      RESERVED3[2];  /*!< Reserved, 0x48-0x4C (Range: var type | Unit: None)*/
  __IO uint32_t AHB1LPENR;     /*!< RCC AHB1 peripheral clock enable in low power mode register, Address offset: 0x50 (Range: var type | Unit: None)*/
  __IO uint32_t AHB2LPENR;     /*!< RCC AHB2 peripheral clock enable in low power mode register, Address offset: 0x54 (Range: var type | Unit: None)*/
  __IO uint32_t AHB3LPENR;     /*!< RCC AHB3 peripheral clock enable in low power mode register, Address offset: 0x58 (Range: var type | Unit: None)*/
  uint32_t      RESERVED4;     /*!< Reserved, 0x5C (Range: var type | Unit: None)*/
  __IO uint32_t APB1LPENR;     /*!< RCC APB1 peripheral clock enable in low power mode register, Address offset: 0x60 (Range: var type | Unit: None)*/
  __IO uint32_t APB2LPENR;     /*!< RCC APB2 peripheral clock enable in low power mode register, Address offset: 0x64 (Range: var type | Unit: None)*/
  uint32_t      RESERVED5[2];  /*!< Reserved, 0x68-0x6C (Range: var type | Unit: None)*/
  __IO uint32_t BDCR;          /*!< RCC Backup domain control register, Address offset: 0x70 (Range: var type | Unit: None)*/
  __IO uint32_t CSR;           /*!< RCC clock control & status register, Address offset: 0x74 (Range: var type | Unit: None)*/
  uint32_t      RESERVED6[2];  /*!< Reserved, 0x78-0x7C  (Range: var type | Unit: None)*/
  __IO uint32_t SSCGR;         /*!< RCC spread spectrum clock generation register, Address offset: 0x80 (Range: var type | Unit: None)*/
  __IO uint32_t PLLI2SCFGR;    /*!< RCC PLLI2S configuration register, Address offset: 0x84 (Range: var type | Unit: None)*/
  __IO uint32_t PLLSAICFGR;    /*!< RCC PLLSAI configuration register, Address offset: 0x88 (Range: var type | Unit: None)*/
  __IO uint32_t DCKCFGR;       /*!< RCC Dedicated Clocks configuration register, Address offset: 0x8C (Range: var type | Unit: None)*/
} RCC_TypeDef;

/*! Structure for Real-Time Clock(RTC)*/
typedef struct
{
  __IO uint32_t TR;      /*!< RTC time register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t DR;      /*!< RTC date register, Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t CR;      /*!< RTC control register, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t ISR;     /*!< RTC initialization and status register (Range: var type | Unit: None)*/
  __IO uint32_t PRER;    /*!< RTC prescaler register, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t WUTR;    /*!< RTC wakeup timer register, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t CALIBR;  /*!< RTC calibration register, Address offset: 0x18 (Range: var type | Unit: None)*/
  __IO uint32_t ALRMAR;  /*!< RTC alarm A register, Address offset: 0x1C (Range: var type | Unit: None)*/
  __IO uint32_t ALRMBR;  /*!< RTC alarm B register, Address offset: 0x20 (Range: var type | Unit: None)*/
  __IO uint32_t WPR;     /*!< RTC write protection register, Address offset: 0x24 (Range: var type | Unit: None)*/
  __IO uint32_t SSR;     /*!< RTC sub second register, Address offset: 0x28 (Range: var type | Unit: None)*/
  __IO uint32_t SHIFTR;  /*!< RTC shift control register, Address offset: 0x2C (Range: var type | Unit: None)*/
  __IO uint32_t TSTR;    /*!< RTC time stamp time register, Address offset: 0x30 (Range: var type | Unit: None)*/
  __IO uint32_t TSDR;    /*!< RTC time stamp date register, Address offset: 0x34 (Range: var type | Unit: None)*/
  __IO uint32_t TSSSR;   /*!< RTC time-stamp sub second register,  Address offset: 0x38 (Range: var type | Unit: None)*/
  __IO uint32_t CALR;    /*!< RTC calibration register, Address offset: 0x3C (Range: var type | Unit: None)*/
  __IO uint32_t TAFCR;   /*!< RTC tamper and alternate function configuration register, Address offset: 0x40 (Range: var type | Unit: None)*/
  __IO uint32_t ALRMASSR;/*!< RTC alarm A sub second register, Address offset: 0x44 (Range: var type | Unit: None)*/
  __IO uint32_t ALRMBSSR;/*!< RTC alarm B sub second register, Address offset: 0x48 (Range: var type | Unit: None)*/
  uint32_t RESERVED7;    /*!< Reserved, 0x4C (Range: var type | Unit: None)*/
  __IO uint32_t BKP0R;   /*!< RTC backup register 1,  Address offset: 0x50 (Range: var type | Unit: None)*/
  __IO uint32_t BKP1R;   /*!< RTC backup register 1,  Address offset: 0x54 (Range: var type | Unit: None)*/
  __IO uint32_t BKP2R;   /*!< RTC backup register 2,  Address offset: 0x58 (Range: var type | Unit: None)*/
  __IO uint32_t BKP3R;   /*!< RTC backup register 3,  Address offset: 0x5C (Range: var type | Unit: None)*/
  __IO uint32_t BKP4R;   /*!< RTC backup register 4,  Address offset: 0x60 (Range: var type | Unit: None)*/
  __IO uint32_t BKP5R;   /*!< RTC backup register 5,  Address offset: 0x64 (Range: var type | Unit: None)*/
  __IO uint32_t BKP6R;   /*!< RTC backup register 6,  Address offset: 0x68 (Range: var type | Unit: None)*/
  __IO uint32_t BKP7R;   /*!< RTC backup register 7,  Address offset: 0x6C (Range: var type | Unit: None)*/
  __IO uint32_t BKP8R;   /*!< RTC backup register 8,  Address offset: 0x70 (Range: var type | Unit: None)*/
  __IO uint32_t BKP9R;   /*!< RTC backup register 9,  Address offset: 0x74 (Range: var type | Unit: None)*/
  __IO uint32_t BKP10R;  /*!< RTC backup register 10, Address offset: 0x78 (Range: var type | Unit: None)*/
  __IO uint32_t BKP11R;  /*!< RTC backup register 11, Address offset: 0x7C (Range: var type | Unit: None)*/
  __IO uint32_t BKP12R;  /*!< RTC backup register 12, Address offset: 0x80 (Range: var type | Unit: None)*/
  __IO uint32_t BKP13R;  /*!< RTC backup register 13, Address offset: 0x84 (Range: var type | Unit: None)*/
  __IO uint32_t BKP14R;  /*!< RTC backup register 14, Address offset: 0x88 (Range: var type | Unit: None)*/
  __IO uint32_t BKP15R;  /*!< RTC backup register 15, Address offset: 0x8C (Range: var type | Unit: None)*/
  __IO uint32_t BKP16R;  /*!< RTC backup register 16, Address offset: 0x90 (Range: var type | Unit: None)*/
  __IO uint32_t BKP17R;  /*!< RTC backup register 17, Address offset: 0x94 (Range: var type | Unit: None)*/
  __IO uint32_t BKP18R;  /*!< RTC backup register 18, Address offset: 0x98 (Range: var type | Unit: None)*/
  __IO uint32_t BKP19R;  /*!< RTC backup register 19, Address offset: 0x9C (Range: var type | Unit: None)*/
} RTC_TypeDef;

/*! Structure for Timers (TIM)*/
typedef struct
{
  __IO uint32_t CR1;         /*!< TIM control register 1, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t CR2;         /*!< TIM control register 2, Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t SMCR;        /*!< TIM slave mode control register, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t DIER;        /*!< TIM DMA/interrupt enable register, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t SR;          /*!< TIM status register, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t EGR;         /*!< TIM event generation register, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t CCMR1;       /*!< TIM capture/compare mode register 1, Address offset: 0x18 (Range: var type | Unit: None)*/
  __IO uint32_t CCMR2;       /*!< TIM capture/compare mode register 2, Address offset: 0x1C (Range: var type | Unit: None)*/
  __IO uint32_t CCER;        /*!< TIM capture/compare enable register, Address offset: 0x20 (Range: var type | Unit: None)*/
  __IO uint32_t CNT;         /*!< TIM counter register, Address offset: 0x24 (Range: var type | Unit: None)*/
  __IO uint32_t PSC;         /*!< TIM prescaler, Address offset: 0x28 (Range: var type | Unit: None)*/
  __IO uint32_t ARR;         /*!< TIM auto-reload register, Address offset: 0x2C (Range: var type | Unit: None)*/
  __IO uint32_t RCR;         /*!< TIM repetition counter register, Address offset: 0x30 (Range: var type | Unit: None)*/
  __IO uint32_t CCR1;        /*!< TIM capture/compare register 1, Address offset: 0x34 (Range: var type | Unit: None)*/
  __IO uint32_t CCR2;        /*!< TIM capture/compare register 2, Address offset: 0x38 (Range: var type | Unit: None)*/
  __IO uint32_t CCR3;        /*!< TIM capture/compare register 3, Address offset: 0x3C (Range: var type | Unit: None)*/
  __IO uint32_t CCR4;        /*!< TIM capture/compare register 4, Address offset: 0x40 (Range: var type | Unit: None)*/
  __IO uint32_t BDTR;        /*!< TIM break and dead-time register, Address offset: 0x44 (Range: var type | Unit: None)*/
  __IO uint32_t DCR;         /*!< TIM DMA control register, Address offset: 0x48 (Range: var type | Unit: None)*/
  __IO uint32_t DMAR;        /*!< TIM DMA address for full transfer, Address offset: 0x4C (Range: var type | Unit: None)*/
  __IO uint32_t OR;          /*!< TIM option register, Address offset: 0x50 (Range: var type | Unit: None)*/
} TIM_TypeDef;

/*!Structure for  Universal Synchronous Asynchronous Receiver Transmitter(USART)*/
typedef struct
{
  __IO uint32_t SR;         /*!< USART Status register, Address offset: 0x00 (Range: var type | Unit: None)*/
  __IO uint32_t DR;         /*!< USART Data register, Address offset: 0x04 (Range: var type | Unit: None)*/
  __IO uint32_t BRR;        /*!< USART Baud rate register, Address offset: 0x08 (Range: var type | Unit: None)*/
  __IO uint32_t CR1;        /*!< USART Control register 1, Address offset: 0x0C (Range: var type | Unit: None)*/
  __IO uint32_t CR2;        /*!< USART Control register 2, Address offset: 0x10 (Range: var type | Unit: None)*/
  __IO uint32_t CR3;        /*!< USART Control register 3, Address offset: 0x14 (Range: var type | Unit: None)*/
  __IO uint32_t GTPR;       /*!< USART Guard time and prescaler register, Address offset: 0x18 (Range: var type | Unit: None)*/
} USART_TypeDef;


/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

#endif /* STM32F427XX_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
