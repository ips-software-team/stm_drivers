/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_def.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 01-09-2021
**
** Description: Header for global definitions
**
*/


/* Revision Log:
**
** Rev 1.0  : 04-11-2020 -- Original version created.
** Rev 1.1  : 01-01-2021 -- Updated due to verification remarks
** Rev 1.2  : 06-07-2021 -- Added NULL define instead of stddef.h
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
** Rev 1.4  : 01-09-2021 -- Updated due to verification remarks
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_DEF_H
#define GIPSY_HAL_DEF_H

/* -----Includes -------------------------------------------------------------------*/
#include "stm32f427xx.h"
#include "main.h"

/* -----Definitions ----------------------------------------------------------------*/
#define NULL ((void *)0)/*!< null pointer definition */


#define RESET 0U
#define SET   1U

#define FALSE 0U
#define TRUE  1U

#define TEST_OK  0U
#define TEST_FL  1U

#define HAL_MAX_DELAY      0xFFFFFFFFU
/* -----Macros ---------------------------------------------------------------------*/
#define SET_BIT(REG, BIT)     ((REG) |= (BIT))

#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))

#define READ_BIT(REG, BIT)    ((REG) & (BIT))

#define CLEAR_REG(REG)        ((REG) = (0x0))

#define WRITE_REG(REG, VAL)   ((REG) = (VAL))

#define READ_REG(REG)         ((REG))

#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))

#define UNUSED(X) (void)X      /* To avoid gcc/g++ warnings */

#define HAL_IS_BIT_SET(REG, BIT)         (((REG) & (BIT)) != RESET)
#define HAL_IS_BIT_CLR(REG, BIT)         (((REG) & (BIT)) == RESET)


/* Macro to get variable aligned on 4-bytes, for __ICCARM__ the directive "#pragma data_alignment=4" must be used instead */
#if defined ( __GNUC__ ) && !defined (__CC_ARM) /* GNU Compiler */
  #ifndef __ALIGN_END
#define __ALIGN_END    __attribute__ ((aligned (4)))
  #endif /* __ALIGN_END */
  #ifndef __ALIGN_BEGIN  
    #define __ALIGN_BEGIN
  #endif /* __ALIGN_BEGIN */
#endif



/* GNU Compiler
   ------------
  RAM functions are defined using a specific toolchain attribute 
   "__attribute__((section(".RamFunc")))".
*/
#define __RAM_FUNC __attribute__((section(".RamFunc")))



/*__NOINLINE definition*/

#define __NOINLINE __attribute__ ( (noinline) )
/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for general states*/
typedef enum
{
  DISABLE = 0U,			/*!<Val:0x00 -  Disable */
  ENABLE = !DISABLE		/*!<Val:0x01 -  Enable */
} FunctionalState;


/*! Enumerator for general HAL states*/
typedef enum
{
  HAL_OK       = 0x00U,  	/*!<Val:0x00 -  OK */
  HAL_ERROR    = 0x01U,		/*!<Val:0x01 -  Error */
  HAL_BUSY     = 0x02U,		/*!<Val:0x02 -  Busy */
  HAL_TIMEOUT  = 0x03U		/*!<Val:0x03 -  Timeout */
} HAL_StatusTypeDef;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/


#endif /* GIPSY_HAL_DEF_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
