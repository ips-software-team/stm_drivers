/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gipsy_hal_rtc.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 27-06-2021
**
** Description: Header for gipsy_hal_rtc.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 21-01-2020 -- Original version created.
** Rev 1.1  : 03-02-2020 -- Updated __HAL_RTC_WRITEPROTECTION_ENABLE macro
** Rev 1.2  : 06-03-2020 -- Removed local defines and macros, updated structures
** Rev 1.3  : 28-07-2020 -- Added RTC backup registers Read/Write functions
** Rev 1.4	: 27-06-2021 -- Updated due to WDOG test changes.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_RTC_H
#define GIPSY_HAL_RTC_H



/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"

/* -----Definitions ----------------------------------------------------------------*/

#define RTC_BKUP_REG_ERR (uint32_t)0xFFFFFFFFU

/* -----Macros ---------------------------------------------------------------------*/



/* -----Type Definitions -----------------------------------------------------------*/

 /*RTC Struct for date/time*/
typedef struct {
	uint8_t Seconds;     /*!< Seconds parameter, from 00 to 59 */
	uint8_t Minutes;     /*!< Minutes parameter, from 00 to 59 */
	uint8_t Hours;       /*!< Hours parameter, 24Hour mode, 00 to 23 */
	uint8_t WeekDay;     /*!< Day in a week, 1 to 7 */
	uint8_t Day;         /*!< Day in a month, 1 to 31 */
	uint8_t Month;       /*!< Month in a year, 1 to 12 */
	uint8_t Year;        /*!< Year parameter, 00 to 99, 00 is 2000 and 99 is 2099 */
} RTC_TimeDate_t;


/*RTC Configuration Structure definition*/
typedef struct
{
  uint32_t HourFormat;      /*!< Specifies the RTC Hour Format.
                                 This parameter can be a value of @ref RTC_Hour_Formats */

  uint32_t AsynchPrediv;    /*!< Specifies the RTC Asynchronous Predivider value.
                                 This parameter must be a number between Min_Data = 0x00 and Max_Data = 0x7F */

  uint32_t SynchPrediv;     /*!< Specifies the RTC Synchronous Predivider value.
                                 This parameter must be a number between Min_Data = 0x00 and Max_Data = 0x7FFFU */

  uint32_t OutPut;          /*!< Specifies which signal will be routed to the RTC output.
                                 This parameter can be a value of @ref RTC_Output_selection_Definitions */

  uint32_t OutPutPolarity;  /*!< Specifies the polarity of the output signal.
                                 This parameter can be a value of @ref RTC_Output_Polarity_Definitions */

  uint32_t OutPutType;      /*!< Specifies the RTC Output Pin mode.
                                 This parameter can be a value of @ref RTC_Output_Type_ALARM_OUT */
}RTC_InitTypeDef;


/*RTC Handle Structure definition*/
typedef struct
{
  RTC_TypeDef                 *Instance;  /*!< Register base address    */
  RTC_InitTypeDef             Init;       /*!< RTC required parameters  */
}RTC_HandleTypeDef;




/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

HAL_StatusTypeDef RTC_StartUp_Initialization(void);
void RTC_GetDateTime(RTC_TimeDate_t *data);
HAL_StatusTypeDef RTC_SetDateTime(RTC_TimeDate_t *data);
#endif /* GIPSY_HAL_RTC_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
