/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_stdin.h
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 08-07-2021
**
** Description: Header for common variables type definitions
**
*/


/* Revision Log:
**
** Rev 1.0  : 08-07-2021 -- Original version created.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_STDINT_H
#define GIPSY_STDINT_H

/* -----Includes -------------------------------------------------------------------*/



/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
 typedef signed char        int8_t; 	/*!< Range: -128 . .127*/
 typedef unsigned char      uint8_t;    /*!< Range: 0 .. 255*/
 typedef signed short       int16_t;	/*!< Range:	-32,768 .. 32,767*/
 typedef unsigned short     uint16_t;	/*!< Range:	0 .. 65,535*/
 typedef signed long        int32_t;	/*!< Range:	-2,147,483,648 .. 2,147,483,647*/
 typedef unsigned long      uint32_t;	/*!< Range:	0 .. 4,294,967,295*/
 typedef signed long long   int64_t;	/*!< Range:	-9,223,372,036,854,775,808 .. 9,223,372,036,854,775,807*/
 typedef unsigned long long uint64_t;	/*!< Range:	0 .. 18,446,744,073,709,551,615*/
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/


#endif /* GIPSY_STDINT_H */

 /************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
