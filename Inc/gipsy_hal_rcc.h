/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_rcc.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 11-07-2021
**
** Description: Header for gipsy_hal_rcc.c file.
**
** Notes:

*/

/* Revision Log:
**
** Rev 1.0  : 28-09-2020 -- Original version created.
** Rev 1.1  : 05-11-2020 -- Addeded FLASH Registers definitions.
** Rev 1.2  : 13-12-2020 -- Added procedure: HAL_RCC_GetPCLK2Freq()
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_RCC_H
#define GIPSY_HAL_RCC_H


/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/
/*******************  Bits definition for FLASH_ACR register  *****************/
#define FLASH_ACR_LATENCY_POS          (0U)
#define FLASH_ACR_LATENCY_MSK          (0xFU << FLASH_ACR_LATENCY_POS)         /*!< 0x0000000F */
#define FLASH_ACR_LATENCY              FLASH_ACR_LATENCY_MSK

#define FLASH_ACR_LATENCY_5WS          0x00000005U

#define FLASH_ACR_PRFTEN_POS           (8U)
#define FLASH_ACR_PRFTEN_MSK           (0x1UL << FLASH_ACR_PRFTEN_POS)          /*!< 0x00000100 */
#define FLASH_ACR_PRFTEN               FLASH_ACR_PRFTEN_MSK
#define FLASH_ACR_ICEN_POS             (9U)
#define FLASH_ACR_ICEN_MSK             (0x1UL << FLASH_ACR_ICEN_POS)            /*!< 0x00000200 */
#define FLASH_ACR_ICEN                 FLASH_ACR_ICEN_MSK
#define FLASH_ACR_DCEN_POS             (10U)
#define FLASH_ACR_DCEN_MSK             (0x1U << FLASH_ACR_DCEN_POS)            /*!< 0x00000400 */
#define FLASH_ACR_DCEN                 FLASH_ACR_DCEN_MSK
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

HAL_StatusTypeDef HAL_RCC_OscConfig(void);
HAL_StatusTypeDef HAL_RCC_ClockConfig(void);
HAL_StatusTypeDef HAL_RCCEx_PeriphCLKConfig(void);
uint32_t HAL_RCC_GetPCLK1Freq(void);
#ifdef _DIMC_
uint32_t HAL_RCC_GetPCLK2Freq(void);
#endif
void HAL_IncTick(void);
uint32_t HAL_GetTick(void);
void HAL_Delay(uint32_t milliseconds);
#endif /* GIPSY_HAL_RCC_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
