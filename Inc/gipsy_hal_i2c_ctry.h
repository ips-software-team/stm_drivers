
 /************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gipsy_hal_i2c_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 11-07-2021
**
** Description: Header for gipsy_hal_i2c_ctry.c file
*/


/* Revision Log:
**
** Rev 1.0  : 26-07-2020 -- Original version created.
** Rev 1.1  : 25-11-2020 -- Updated function prototype
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_I2C_CTRY_H
#define GIPSY_HAL_I2C_CTRY_H

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>

/* -----Definitions ----------------------------------------------------------------*/

/*I2C_Memory_Address_Size I2C Memory Address Size*/
#define I2C_MEMADD_SIZE_8BIT            0x00000001U
#define I2C_MEMADD_SIZE_16BIT           0x00000010U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Structure for I2C BUS Configuration definition*/
typedef struct
{
  uint32_t ClockSpeed;       /*!< Specifies the clock frequency (Range: 0 - 400,000 | Unit: Hz) */
  uint32_t DutyCycle;        /*!< Specifies the I2C fast mode duty cycle (Range: var type | Unit: None)*/
  uint32_t OwnAddress1;      /*!< Specifies the first device own address (Range: var type | Unit: None)*/
  uint32_t AddressingMode;   /*!< Specifies type of addressing mode is selected (Range: var type | Unit: None)*/
  uint32_t DualAddressMode;  /*!< Specifies if dual addressing mode is selected (Range: var type | Unit: None)*/
  uint32_t OwnAddress2;      /*!< Specifies the second device own address if dual addressing mode is selected (Range: var type | Unit: None)*/
  uint32_t GeneralCallMode;  /*!< Specifies if general call mode is selected (Range: var type | Unit: None)*/
  uint32_t NoStretchMode;    /*!< Specifies if nostretch mode is selected (Range: var type | Unit: None)*/
}I2C_InitTypeDef;

/*!  Enumerator for I2C HAL State definition*/
typedef enum
{
  HAL_I2C_STATE_RESET             = 0x00U,   /*!<Val:0x00 - Peripheral is not yet Initialized         */
  HAL_I2C_STATE_READY             = 0x20U,   /*!<Val:0x20 - Peripheral Initialized and ready for use  */
  HAL_I2C_STATE_BUSY              = 0x24U,   /*!<Val:0x24 - An internal process is ongoing            */
  HAL_I2C_STATE_BUSY_TX           = 0x21U,   /*!<Val:0x21 - Data Transmission process is ongoing      */
  HAL_I2C_STATE_BUSY_RX           = 0x22U,   /*!<Val:0x22 - Data Reception process is ongoing         */
  HAL_I2C_STATE_LISTEN            = 0x28U,   /*!<Val:0x28 - Address Listen Mode is ongoing            */
  HAL_I2C_STATE_BUSY_TX_LISTEN    = 0x29U,   /*!<Val:0x29 - Address Listen Mode and Data Transmission process is ongoing */
  HAL_I2C_STATE_BUSY_RX_LISTEN    = 0x2AU,   /*!<Val:0x2A - Address Listen Mode and Data Reception process is ongoing */
  HAL_I2C_STATE_ABORT             = 0x60U,   /*!<Val:0x60 - Abort user request ongoing                */
  HAL_I2C_STATE_TIMEOUT           = 0xA0U,   /*!<Val:0xA0 - Timeout state                             */
  HAL_I2C_STATE_ERROR             = 0xE0U    /*!<Val:0xE0 - Error                                     */

}HAL_I2C_StateTypeDef;

/*! Enumerator for I2C HAL Mode definition*/
typedef enum
{
  HAL_I2C_MODE_NONE               = 0x00U,   /*!<Val:0x00 - No I2C communication on going             */
  HAL_I2C_MODE_MASTER             = 0x10U,   /*!<Val:0x10 - I2C communication is in Master Mode       */
  HAL_I2C_MODE_SLAVE              = 0x20U,   /*!<Val:0x20 - I2C communication is in Slave Mode        */
  HAL_I2C_MODE_MEM                = 0x40U    /*!<Val:0x40 - I2C communication is in Memory Mode       */

}HAL_I2C_ModeTypeDef;

/*! I2C handle Structure definition*/
typedef struct
{
  I2C_TypeDef                *Instance;      /*!< I2C registers base address (Range: var type | Unit: None)*/
  I2C_InitTypeDef            Init;           /*!< I2C communication parameters  (Range: var type | Unit: None)*/
  uint8_t                    *pBuffPtr;      /*!< Pointer to I2C transfer buffer (Range: var type | Unit: None)*/
  uint16_t                   XferSize;       /*!< I2C transfer size (Range: var type | Unit: None)*/
  __IO uint16_t              XferCount;      /*!< I2C transfer counter (Range: var type | Unit: None)*/
  __IO uint32_t              XferOptions;    /*!< I2C transfer options (Range: var type | Unit: None)*/
  __IO uint32_t              PreviousState;  /*!< I2C communication Previous state and mode context for internal usage (Range: var type | Unit: None) */
  HAL_I2C_StateTypeDef       State;          /*!< I2C communication state  (was volatile L.S.) (Range: var type | Unit: None) */
  __IO HAL_I2C_ModeTypeDef   Mode;           /*!< I2C communication mode (Range: var type | Unit: None) */
  __IO uint32_t              ErrorCode;      /*!< I2C Error code (Range: var type | Unit: None)*/
  __IO uint32_t              Devaddress;     /*!< I2C Target device address (Range: var type | Unit: None)*/
  __IO uint32_t              Memaddress;     /*!< I2C Target memory address (Range: var type | Unit: None)*/
  __IO uint32_t              MemaddSize;     /*!< I2C Target memory address  size (Range: var type | Unit: None)*/
  __IO uint32_t              EventCount;     /*!< I2C Event counter (Range: var type | Unit: None)*/
}I2C_HandleTypeDef;




/* -----Function prototypes --------------------------------------------------------*/
void HAL_I2C_Init(I2C_HandleTypeDef *hi2c);
HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout);


#endif /* GIPSY_HAL_I2C_CTRY_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
