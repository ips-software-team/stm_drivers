/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : cmsis_gcc.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 01-09-2021
**
** Description: Header file for compiler.
**
*/


/* Revision Log:
**
** Rev 1.0  : 04-11-2020 -- Original version created.
** Rev 1.1  : 01-01-2021 -- Updated due to verification remarks
** Rev 1.2  : 20-07-2021 -- Removed unused DSB command
** Rev 1.3  : 25-07-2021 -- Added DSB command for DEBUG reset
** Rev 1.4  : 01-09-2021 -- Updated due to verification remarks
**************************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CMSIS_GCC_H
#define CMSIS_GCC_H
/* -----Includes -------------------------------------------------------------------*/
/* -----Definitions ----------------------------------------------------------------*/
/*Definitions for ( __GNUC__ )*/
#define __ASM            __asm                                      /*!< asm keyword for GNU Compiler */
#define __INLINE         inline                                     /*!< inline keyword for GNU Compiler */
#define __STATIC_INLINE  static inline								/*!< static inline keyword for GNU Compiler */

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
/**
  \brief   No Operation
  \details No Operation does nothing. This instruction can be used for code alignment purposes.
 */
__attribute__((always_inline)) __STATIC_INLINE void __NOP(void)
{
  __ASM volatile ("nop");
}

#ifdef _DEBUG_
/**
  \brief   Data Synchronization Barrier
  \details Acts as a special kind of Data Memory Barrier.
           It completes when all explicit memory accesses before this instruction complete.
 */
__attribute__((always_inline)) __STATIC_INLINE void __DSB(void)
{
  __ASM volatile ("dsb 0xF":::"memory");
}
#endif
#endif /* CMSIS_GCC_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
