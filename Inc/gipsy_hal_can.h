/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_can.h
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 11-07-2021
**
** Description: Header for gipsy_hal_can.c file
**
*/


/* Revision Log:
** Rev 1.0  : 08-03-2020 -- Original version created.
** Rev 1.1  : 20-11-2020 -- Static analysis update
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_CAN_H
#define GIPSY_HAL_CAN_H


/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/
# define CAN_DATA_LENGTH_BYTES   8U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for CAN HAL State definition*/
typedef enum
{
  E_HAL_CAN_STATE_RESET             = 0x00U,  /*!<Val:0x00 -  CAN not yet initialized or disabled */
  E_HAL_CAN_STATE_READY             = 0x01U,  /*!<Val:0x01 -  CAN initialized and ready for use   */
  E_HAL_CAN_STATE_LISTENING         = 0x02U,  /*!<Val:0x02 -  CAN receive process is ongoing      */
  E_HAL_CAN_STATE_SLEEP_PENDING     = 0x03U,  /*!<Val:0x03 -  CAN sleep request is pending        */
  E_HAL_CAN_STATE_SLEEP_ACTIVE      = 0x04U,  /*!<Val:0x04 -  CAN sleep mode is active            */
  E_HAL_CAN_STATE_ERROR             = 0x05U   /*!<Val:0x05 -  CAN error state                     */

} HAL_CAN_StateTypeDef;

/*! Structure for CAN initialization definition*/
typedef struct
{
  uint32_t Prescaler;                  /*!< Specifies the length of a time quantum (Range: 1 - 1024 | Unit: None)						*/
  uint32_t Mode;                       /*!< Specifies the CAN operating mode (Range: var type | Unit: None)								*/
  uint32_t SyncJumpWidth;              /*!< Specifies the maximum number of time quanta the CAN hardware (Range: var type | Unit: None)	*/
  uint32_t TimeSeg1;                   /*!< Specifies the number of time quanta in Bit Segment 1 (Range: var type | Unit: None)			*/
  uint32_t TimeSeg2;                   /*!< Specifies the number of time quanta in Bit Segment 2 (Range: var type | Unit: None)			*/
  FunctionalState TimeTriggeredMode;   /*!< Enable or disable the time triggered communication mode (Range: 0 - 1 | Unit: None)			*/
  FunctionalState AutoBusOff;          /*!< Enable or disable the automatic bus-off management (Range: 0 - 1 | Unit: None)				*/
  FunctionalState AutoWakeUp;          /*!< Enable or disable the automatic wake-up mode (Range: 0 - 1 | Unit: None)					*/
  FunctionalState AutoRetransmission;  /*!< Enable or disable the non-automatic retransmission mode (Range: 0 - 1 | Unit: None)			*/
  FunctionalState ReceiveFifoLocked;   /*!< Enable or disable the Receive FIFO Locked mode (Range: 0 - 1 | Unit: None)					*/
  FunctionalState TransmitFifoPriority;/*!< Enable or disable the transmit FIFO priority (Range: 0 - 1 | Unit: None)					*/
} CAN_InitTypeDef;



/*! Structure for CAN Rx message header definition*/
typedef struct
{
  uint32_t StdId;    /*!< Specifies the standard identifier (Range: 0 - 0x7FF | Unit: None)											*/
  uint32_t IDE;      /*!< Specifies the type of identifier for the message that will be transmitted (Range: var type | Unit: None)	*/
  uint32_t RTR;      /*!< Specifies the type of frame for the message that will be transmitted (Range: var type | Unit: None)		*/
  uint32_t DLC;      /*!< Specifies the length of the frame that will be transmitted (Range: 0 - 8 | Unit: Byte)					*/
} CAN_RxHeaderTypeDef;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

HAL_StatusTypeDef HAL_CAN_Init(void);
HAL_StatusTypeDef HAL_CAN_AddTxMessage(uint32_t header_std_id, uint8_t aData[CAN_DATA_LENGTH_BYTES]);
HAL_StatusTypeDef HAL_CAN_GetRxMessage(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[CAN_DATA_LENGTH_BYTES]);
void HAL_CAN_IRQHandler(void);
void HAL_CAN_Stop(void);

#endif /* GIPSY_HAL_CAN_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
