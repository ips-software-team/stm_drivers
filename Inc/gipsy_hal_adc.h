/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_adc.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 11-07-2021
**
** Description: Header for gipsy_hal_adc.c file
** 		        Header file containing functions prototypes of ADC HAL library
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_ADC_H
#define GIPSY_HAL_ADC_H


/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/
#define ACD_RESOLUTION_VOLT      0.00080566  /*(3.3VDC / 2^12)*/

#define ADC_CHANNEL_2           (uint32_t)2U
#define ADC_CHANNEL_3           (uint32_t)3U
#define ADC_CHANNEL_4           (uint32_t)4U
#define ADC_CHANNEL_5           (uint32_t)5U
#define ADC_CHANNEL_6           (uint32_t)6U
#define ADC_CHANNEL_8           (uint32_t)8U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Structure definition of ADC and regular group initialization*/
typedef struct
{
  uint32_t ClockPrescaler;        /*!< Select ADC clock prescaler. The clock is common for all the ADCs (Range: var type | Unit: None)*/
  uint32_t Resolution;            /*!< Configures the ADC resolution (Range: var type | Unit: None)*/
  uint32_t DataAlign;             /*!< Specifies ADC data alignment (Range: var type | Unit: None)*/
  uint32_t ScanConvMode;          /*!< Configures the sequencer of regular and injected groups (Range: var type | Unit: None)*/
  uint32_t EOCSelection;          /*!< Specifies what EOC (End Of Conversion) flag is used for conversion by polling and interruption (Range: var type | Unit: None)*/
  uint32_t ContinuousConvMode;    /*!< Specifies whether the conversion is performed in single or continuous mode (Range: var type | Unit: None)*/
  uint32_t NbrOfConversion;       /*!< Specifies the number of ranks that will be converted within the regular group sequencer (Range: var type | Unit: None)*/
  uint32_t DiscontinuousConvMode; /*!< Specifies whether the conversions sequence of regular group is performed in Complete-sequence/Discontinuous-sequence (Range: var type | Unit: None)*/
  uint32_t NbrOfDiscConversion;   /*!< Specifies the number of discontinuous conversions (Range: var type | Unit: None)*/
  uint32_t ExternalTrigConv;      /*!< Selects the external event used to trigger the conversion start of regular group (Range: var type | Unit: None)*/
  uint32_t ExternalTrigConvEdge;  /*!< Selects the external trigger edge of regular group (Range: var type | Unit: None)*/
}ADC_InitTypeDef;





/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void HAL_ADC_Init(void);
HAL_StatusTypeDef HAL_ADC_Perform_Conversion(uint32_t channel, uint32_t *converted_value);
#endif /*GIPSY_HAL_ADC_H */

 /************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
