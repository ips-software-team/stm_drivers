/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gipsy_hal_tim_pwm_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 11-07-2021
**
** Description: Header for gipsy_hal_tim_pwm_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 05-09-2020 -- Original version created.
** Rev 1.1  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_HAL_TIM_PWM_CTRY
#define GIPSY_HAL_TIM_PWM_CTRY


/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>

/* -----Definitions ----------------------------------------------------------------*/

/*TIM Channel*/
#define TIM_CHANNEL_1                      0x00000000U
#define TIM_CHANNEL_2                      0x00000004U
#define TIM_CHANNEL_3                      0x00000008U
#define TIM_CHANNEL_4                      0x0000000CU

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Structure for TIM Time Base Handle definition*/
typedef struct
{
  TIM_TypeDef                 *Instance;     /*!<TIM Registers structure base address (Range: var type | Unit: None)*/
}TIM_HandleTypeDef;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
HAL_StatusTypeDef HAL_TIM_PWM_Init(TIM_HandleTypeDef *htim, uint32_t prescaler, uint32_t period);

#endif /* GIPSY_HAL_TIM_PWM_CTRY */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
