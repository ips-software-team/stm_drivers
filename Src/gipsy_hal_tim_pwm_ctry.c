/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gipsy_hal_tim_pwm_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 25-06-2021
**
** Description: This file provides procedures for configuration Timer's PWM output channels.
*/


/* Revision Log:
**
** Rev 1.0  : 05-09-2020 -- Original version created.
** Rev 1.1  : 11-10-2020 -- Updated after review.
** Rev 1.2  : 20-11-2020 -- Static analysis update
** Rev 1.3  : 25-06-2021 -- Static analysis update
**************************************************************************************
*/
/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_tim_pwm_ctry.h"



/* -----Definitions ----------------------------------------------------------------*/
/*******************  Bit definition for TIM_CR1 register  ********************/
#define TIM_CR1_CEN_POS           (0U)
#define TIM_CR1_CEN_MSK           (0x1UL << TIM_CR1_CEN_POS)                    /*!< 0x00000001 */
#define TIM_CR1_CEN               TIM_CR1_CEN_MSK                              /*!<Counter enable        */

#define TIM_CR1_DIR_POS           (4U)
#define TIM_CR1_DIR_MSK           (0x1UL << TIM_CR1_DIR_POS)                    /*!< 0x00000010 */
#define TIM_CR1_DIR               TIM_CR1_DIR_MSK                              /*!<Direction             */

#define TIM_CR1_CMS_POS           (5U)
#define TIM_CR1_CMS_MSK           (0x3UL << TIM_CR1_CMS_POS)                    /*!< 0x00000060 */
#define TIM_CR1_CMS               TIM_CR1_CMS_MSK                              /*!<CMS[1:0] bits (Center-aligned mode selection) */
#define TIM_CR1_CMS_0             (0x1UL << TIM_CR1_CMS_POS)                    /*!< 0x0020 */
#define TIM_CR1_CMS_1             (0x2UL << TIM_CR1_CMS_POS)                    /*!< 0x0040 */

#define TIM_CR1_CKD_POS           (8U)
#define TIM_CR1_CKD_MSK           (0x3UL << TIM_CR1_CKD_POS)                    /*!< 0x00000300 */
#define TIM_CR1_CKD               TIM_CR1_CKD_MSK                              /*!<CKD[1:0] bits (clock division) */
#define TIM_CR1_CKD_0             (0x1UL << TIM_CR1_CKD_POS)                    /*!< 0x0100 */
#define TIM_CR1_CKD_1             (0x2UL << TIM_CR1_CKD_POS)                    /*!< 0x0200 */

/*******************  Bit definition for TIM_CR2 register  ********************/
#define TIM_CR2_MMS_POS           (4U)
#define TIM_CR2_MMS_MSK           (0x7UL << TIM_CR2_MMS_POS)                    /*!< 0x00000070 */
#define TIM_CR2_MMS               TIM_CR2_MMS_MSK                              /*!<MMS[2:0] bits (Master Mode Selection) */
#define TIM_CR2_MMS_0             (0x1UL << TIM_CR2_MMS_POS)                    /*!< 0x0010 */
#define TIM_CR2_MMS_1             (0x2UL << TIM_CR2_MMS_POS)                    /*!< 0x0020 */
#define TIM_CR2_MMS_2             (0x4UL << TIM_CR2_MMS_POS)                    /*!< 0x0040 */

/*******************  Bit definition for TIM_SMCR register  *******************/
#define TIM_SMCR_MSM_POS          (7U)
#define TIM_SMCR_MSM_MSK          (0x1UL << TIM_SMCR_MSM_POS)                   /*!< 0x00000080 */
#define TIM_SMCR_MSM              TIM_SMCR_MSM_MSK                             /*!<Master/slave mode                       */

/*******************  Bit definition for TIM_CCER register  *******************/
#define TIM_CCER_CC1E_POS         (0U)
#define TIM_CCER_CC1E_MSK         (0x1UL << TIM_CCER_CC1E_POS)                  /*!< 0x00000001 */
#define TIM_CCER_CC1E             TIM_CCER_CC1E_MSK                            /*!<Capture/Compare 1 output enable                 */
#define TIM_CCER_CC1P_POS         (1U)
#define TIM_CCER_CC1P_MSK         (0x1UL << TIM_CCER_CC1P_POS)                  /*!< 0x00000002 */
#define TIM_CCER_CC1P             TIM_CCER_CC1P_MSK                            /*!<Capture/Compare 1 output Polarity               */
#define TIM_CCER_CC2E_POS         (4U)
#define TIM_CCER_CC2E_MSK         (0x1UL << TIM_CCER_CC2E_POS)                  /*!< 0x00000010 */
#define TIM_CCER_CC2E             TIM_CCER_CC2E_MSK                            /*!<Capture/Compare 2 output enable                 */
#define TIM_CCER_CC2P_POS         (5U)
#define TIM_CCER_CC2P_MSK         (0x1UL << TIM_CCER_CC2P_POS)                  /*!< 0x00000020 */
#define TIM_CCER_CC2P             TIM_CCER_CC2P_MSK                            /*!<Capture/Compare 2 output Polarity               */
#define TIM_CCER_CC3E_POS         (8U)
#define TIM_CCER_CC3E_MSK         (0x1UL << TIM_CCER_CC3E_POS)                  /*!< 0x00000100 */
#define TIM_CCER_CC3E             TIM_CCER_CC3E_MSK                            /*!<Capture/Compare 3 output enable                 */
#define TIM_CCER_CC3P_POS         (9U)
#define TIM_CCER_CC3P_MSK         (0x1UL << TIM_CCER_CC3P_POS)                  /*!< 0x00000200 */
#define TIM_CCER_CC3P             TIM_CCER_CC3P_MSK                            /*!<Capture/Compare 3 output Polarity               */
#define TIM_CCER_CC4E_POS         (12U)
#define TIM_CCER_CC4E_MSK         (0x1UL << TIM_CCER_CC4E_POS)                  /*!< 0x00001000 */
#define TIM_CCER_CC4E             TIM_CCER_CC4E_MSK                            /*!<Capture/Compare 4 output enable                 */
#define TIM_CCER_CC4P_POS         (13U)
#define TIM_CCER_CC4P_MSK         (0x1UL << TIM_CCER_CC4P_POS)                  /*!< 0x00002000 */
#define TIM_CCER_CC4P             TIM_CCER_CC4P_MSK                            /*!<Capture/Compare 4 output Polarity               */


/******************  Bit definition for TIM_CCMR1 register  *******************/
#define TIM_CCMR1_CC1S_POS        (0U)
#define TIM_CCMR1_CC1S_MSK        (0x3UL << TIM_CCMR1_CC1S_POS)                 /*!< 0x00000003 */
#define TIM_CCMR1_CC1S            TIM_CCMR1_CC1S_MSK                           /*!<CC1S[1:0] bits (Capture/Compare 1 Selection) */
#define TIM_CCMR1_CC1S_0          (0x1UL << TIM_CCMR1_CC1S_POS)                 /*!< 0x0001 */
#define TIM_CCMR1_CC1S_1          (0x2UL << TIM_CCMR1_CC1S_POS)                 /*!< 0x0002 */

#define TIM_CCMR1_OC1FE_POS       (2U)
#define TIM_CCMR1_OC1FE_MSK       (0x1UL << TIM_CCMR1_OC1FE_POS)                /*!< 0x00000004 */
#define TIM_CCMR1_OC1FE           TIM_CCMR1_OC1FE_MSK                          /*!<Output Compare 1 Fast enable                 */
#define TIM_CCMR1_OC1PE_POS       (3U)
#define TIM_CCMR1_OC1PE_MSK       (0x1UL << TIM_CCMR1_OC1PE_POS)                /*!< 0x00000008 */
#define TIM_CCMR1_OC1PE           TIM_CCMR1_OC1PE_MSK                          /*!<Output Compare 1 Preload enable              */

#define TIM_CCMR1_OC1M_POS        (4U)
#define TIM_CCMR1_OC1M_MSK        (0x7UL << TIM_CCMR1_OC1M_POS)                 /*!< 0x00000070 */
#define TIM_CCMR1_OC1M            TIM_CCMR1_OC1M_MSK                           /*!<OC1M[2:0] bits (Output Compare 1 Mode)       */
#define TIM_CCMR1_OC1M_0          (0x1UL << TIM_CCMR1_OC1M_POS)                 /*!< 0x0010 */
#define TIM_CCMR1_OC1M_1          (0x2UL << TIM_CCMR1_OC1M_POS)                 /*!< 0x0020 */
#define TIM_CCMR1_OC1M_2          (0x4UL << TIM_CCMR1_OC1M_POS)                 /*!< 0x0040 */

#define TIM_CCMR1_CC2S_POS        (8U)
#define TIM_CCMR1_CC2S_MSK        (0x3UL << TIM_CCMR1_CC2S_POS)                 /*!< 0x00000300 */
#define TIM_CCMR1_CC2S            TIM_CCMR1_CC2S_MSK                           /*!<CC2S[1:0] bits (Capture/Compare 2 Selection) */
#define TIM_CCMR1_CC2S_0          (0x1UL << TIM_CCMR1_CC2S_POS)                 /*!< 0x0100 */
#define TIM_CCMR1_CC2S_1          (0x2UL << TIM_CCMR1_CC2S_POS)                 /*!< 0x0200 */


#define TIM_CCMR1_OC2FE_POS       (10U)
#define TIM_CCMR1_OC2FE_MSK       (0x1UL << TIM_CCMR1_OC2FE_POS)                /*!< 0x00000400 */
#define TIM_CCMR1_OC2FE           TIM_CCMR1_OC2FE_MSK                          /*!<Output Compare 2 Fast enable                 */
#define TIM_CCMR1_OC2PE_POS       (11U)
#define TIM_CCMR1_OC2PE_MSK       (0x1UL << TIM_CCMR1_OC2PE_POS)                /*!< 0x00000800 */
#define TIM_CCMR1_OC2PE           TIM_CCMR1_OC2PE_MSK                          /*!<Output Compare 2 Preload enable              */

#define TIM_CCMR1_OC2M_POS        (12U)
#define TIM_CCMR1_OC2M_MSK        (0x7UL << TIM_CCMR1_OC2M_POS)                 /*!< 0x00007000 */
#define TIM_CCMR1_OC2M            TIM_CCMR1_OC2M_MSK                           /*!<OC2M[2:0] bits (Output Compare 2 Mode)       */
#define TIM_CCMR1_OC2M_0          (0x1UL << TIM_CCMR1_OC2M_POS)                 /*!< 0x1000 */
#define TIM_CCMR1_OC2M_1          (0x2UL << TIM_CCMR1_OC2M_POS)                 /*!< 0x2000 */
#define TIM_CCMR1_OC2M_2          (0x4UL << TIM_CCMR1_OC2M_POS)                 /*!< 0x4000 */

/*******************  Bit definition for TIM_EGR register  ********************/
#define TIM_EGR_UG_POS            (0U)
#define TIM_EGR_UG_MSK            (0x1UL << TIM_EGR_UG_POS)                     /*!< 0x00000001 */
#define TIM_EGR_UG                TIM_EGR_UG_MSK
/******************  Bit definition for TIM_CCMR2 register  *******************/
#define TIM_CCMR2_CC3S_POS        (0U)
#define TIM_CCMR2_CC3S_MSK        (0x3UL << TIM_CCMR2_CC3S_POS)                 /*!< 0x00000003 */
#define TIM_CCMR2_CC3S            TIM_CCMR2_CC3S_MSK                           /*!<CC3S[1:0] bits (Capture/Compare 3 Selection)  */
#define TIM_CCMR2_CC3S_0          (0x1UL << TIM_CCMR2_CC3S_POS)                 /*!< 0x0001 */
#define TIM_CCMR2_CC3S_1          (0x2UL << TIM_CCMR2_CC3S_POS)                 /*!< 0x0002 */

#define TIM_CCMR2_OC3FE_POS       (2U)
#define TIM_CCMR2_OC3FE_MSK       (0x1UL << TIM_CCMR2_OC3FE_POS)                /*!< 0x00000004 */
#define TIM_CCMR2_OC3FE           TIM_CCMR2_OC3FE_MSK                          /*!<Output Compare 3 Fast enable           */
#define TIM_CCMR2_OC3PE_POS       (3U)
#define TIM_CCMR2_OC3PE_MSK       (0x1UL << TIM_CCMR2_OC3PE_POS)                /*!< 0x00000008 */
#define TIM_CCMR2_OC3PE           TIM_CCMR2_OC3PE_MSK                          /*!<Output Compare 3 Preload enable        */

#define TIM_CCMR2_OC3M_POS        (4U)
#define TIM_CCMR2_OC3M_MSK        (0x7UL << TIM_CCMR2_OC3M_POS)                 /*!< 0x00000070 */
#define TIM_CCMR2_OC3M            TIM_CCMR2_OC3M_MSK                           /*!<OC3M[2:0] bits (Output Compare 3 Mode) */
#define TIM_CCMR2_OC3M_0          (0x1UL << TIM_CCMR2_OC3M_POS)                 /*!< 0x0010 */
#define TIM_CCMR2_OC3M_1          (0x2UL << TIM_CCMR2_OC3M_POS)                 /*!< 0x0020 */
#define TIM_CCMR2_OC3M_2          (0x4UL << TIM_CCMR2_OC3M_POS)                 /*!< 0x0040 */

#define TIM_CCMR2_CC4S_POS        (8U)
#define TIM_CCMR2_CC4S_MSK        (0x3UL << TIM_CCMR2_CC4S_POS)                 /*!< 0x00000300 */
#define TIM_CCMR2_CC4S            TIM_CCMR2_CC4S_MSK                           /*!<CC4S[1:0] bits (Capture/Compare 4 Selection) */
#define TIM_CCMR2_CC4S_0          (0x1UL << TIM_CCMR2_CC4S_POS)                 /*!< 0x0100 */
#define TIM_CCMR2_CC4S_1          (0x2UL << TIM_CCMR2_CC4S_POS)                 /*!< 0x0200 */

#define TIM_CCMR2_OC4FE_POS       (10U)
#define TIM_CCMR2_OC4FE_MSK       (0x1UL << TIM_CCMR2_OC4FE_POS)                /*!< 0x00000400 */
#define TIM_CCMR2_OC4FE           TIM_CCMR2_OC4FE_MSK                          /*!<Output Compare 4 Fast enable    */
#define TIM_CCMR2_OC4PE_POS       (11U)
#define TIM_CCMR2_OC4PE_MSK       (0x1UL << TIM_CCMR2_OC4PE_POS)                /*!< 0x00000800 */
#define TIM_CCMR2_OC4PE           TIM_CCMR2_OC4PE_MSK                          /*!<Output Compare 4 Preload enable */

#define TIM_CCMR2_OC4M_POS        (12U)
#define TIM_CCMR2_OC4M_MSK        (0x7UL << TIM_CCMR2_OC4M_POS)                 /*!< 0x00007000 */
#define TIM_CCMR2_OC4M            TIM_CCMR2_OC4M_MSK                           /*!<OC4M[2:0] bits (Output Compare 4 Mode) */
#define TIM_CCMR2_OC4M_0          (0x1UL << TIM_CCMR2_OC4M_POS)                 /*!< 0x1000 */
#define TIM_CCMR2_OC4M_1          (0x2UL << TIM_CCMR2_OC4M_POS)                 /*!< 0x2000 */
#define TIM_CCMR2_OC4M_2          (0x4UL << TIM_CCMR2_OC4M_POS)                 /*!< 0x4000 */
/*******************  Bit definition for TIM_BDTR register  *******************/
#define TIM_BDTR_MOE_POS          (15U)
#define TIM_BDTR_MOE_MSK          (0x1UL << TIM_BDTR_MOE_POS)                   /*!< 0x00008000 */
#define TIM_BDTR_MOE              TIM_BDTR_MOE_MSK                             /*!<Main Output enable                */

/*TIM Counter Mode*/
#define TIM_COUNTERMODE_UP                 	0x00000000U

/*TIM Output Fast State*/
#define TIM_OCFAST_DISABLE                	0x00000000U
#define TIM_OCFAST_ENABLE                 	(TIM_CCMR1_OC1FE)

/*TIM Clock Division*/
#define TIM_CLOCKDIVISION_DIV1            	0x00000000U
#define TIM_CLOCKDIVISION_DIV2             	(TIM_CR1_CKD_0)
#define TIM_CLOCKDIVISION_DIV4              (TIM_CR1_CKD_1)

/*TIM Output Compare and PWM modes*/
#define TIM_OCMODE_TIMING                   0x00000000U
#define TIM_OCMODE_ACTIVE                   (TIM_CCMR1_OC1M_0)
#define TIM_OCMODE_INACTIVE                 (TIM_CCMR1_OC1M_1)
#define TIM_OCMODE_TOGGLE                   (TIM_CCMR1_OC1M_0 | TIM_CCMR1_OC1M_1)
#define TIM_OCMODE_PWM1                     (TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2)
#define TIM_OCMODE_PWM2                     (TIM_CCMR1_OC1M)
#define TIM_OCMODE_FORCED_ACTIVE            (TIM_CCMR1_OC1M_0 | TIM_CCMR1_OC1M_2)
#define TIM_OCMODE_FORCED_INACTIVE          (TIM_CCMR1_OC1M_2)

/*TIM Output Compare Polarity*/
#define TIM_OCPOLARITY_HIGH                	0x00000000U
#define TIM_OCPOLARITY_LOW                 	(TIM_CCER_CC1P)

/*Channel CC State*/
#define TIM_CCx_ENABLE                   	0x00000001U
#define TIM_CCx_DISABLE                  	0x00000000U

#define TIM_TRGO_RESET     				0x00000000U
#define TIM_MASTERSLAVEMODE_DISABLE     0x00000000U


#define DEFAULT_PULSE_VALUE  0U
/* -----Macros ---------------------------------------------------------------------*/

#define IS_TIM_CHANNELS(CHANNEL) (((CHANNEL) == TIM_CHANNEL_1) || \
                                  ((CHANNEL) == TIM_CHANNEL_2) || \
                                  ((CHANNEL) == TIM_CHANNEL_3) || \
                                  ((CHANNEL) == TIM_CHANNEL_4))

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
static HAL_StatusTypeDef hal_tim_pwm_config_channel(TIM_HandleTypeDef *htim, uint32_t tim_channel_number);
static HAL_StatusTypeDef hal_tim_pwm_start(TIM_HandleTypeDef *htim, uint32_t tim_channel_number);
/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** HAL_TIM_PWM_Init - Initializes the TIM PWM Time Base according to the specified
**                    parameters in the TIM_HandleTypeDef and create the associated handle.
**
** Params : TIM_HandleTypeDef *htim - Pointer to a TIM_HandleTypeDef structure that contains
**                                    the configuration information for TIM module.
**          uint32_t prescaler - Value of prescaler of the Timer frequency
**          uint32_t period - Value of the period of the Timer output signal
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_TIM_PWM_Init(TIM_HandleTypeDef *htim, uint32_t prescaler, uint32_t period)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint32_t tmpcr1 = 0U;

	/* Check the TIM handle allocation */
	if((NULL != htim) && ((TIM3 == htim->Instance) || (TIM4 == htim->Instance)))
	{
		/* Init the base time for the PWM (previously TIM_Base_SetConfig function)*/
		tmpcr1 = htim->Instance->CR1;

		/* Set TIM Time Base Unit parameters ---------------------------------------*/
		/* Select the Counter Mode (For TIM1-TIM5,TIM8)*/
		tmpcr1 &= ~(TIM_CR1_DIR | TIM_CR1_CMS);
		tmpcr1 |= TIM_COUNTERMODE_UP;

		/* Set the clock division (For TIM1-TIM5,TIM8-TIM14)*/
		tmpcr1 &= ~TIM_CR1_CKD;
		tmpcr1 |= TIM_CLOCKDIVISION_DIV1;

		htim->Instance->CR1 = (uint32_t)tmpcr1;
		/* Set the Auto-reload value */
		htim->Instance->ARR = (uint32_t)period;
		/* Set the Prescaler value */
		htim->Instance->PSC = (uint32_t)prescaler;

		/* END: Init the base time for the PWM*/

		/*Configures the TIM in master mode*/
		/* Reset the MMS Bits */
		htim->Instance->CR2 &= ~TIM_CR2_MMS;

		/* Reset the MSM Bit (configure Master Mode) */
		htim->Instance->SMCR &= ~TIM_SMCR_MSM;

		/*END : Configures the TIM in master mode*/

		/*Configure PWM Channels according to TIM instance name*/
		if(TIM3 == htim->Instance)
		{
			hal_status |= hal_tim_pwm_config_channel(htim, TIM_CHANNEL_1);
			hal_status |= hal_tim_pwm_config_channel(htim, TIM_CHANNEL_2);
			hal_status |= hal_tim_pwm_config_channel(htim, TIM_CHANNEL_3);
		}
		if (TIM4 == htim->Instance)
		{
			hal_status |= hal_tim_pwm_config_channel(htim, TIM_CHANNEL_2);
			hal_status |= hal_tim_pwm_config_channel(htim, TIM_CHANNEL_3);
			hal_status |= hal_tim_pwm_config_channel(htim, TIM_CHANNEL_4);
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}  

/*************************************************************************************
** hal_tim_pwm_start - Starts the PWM signal generation.
**
** Params : TIM_HandleTypeDef *htim - Pointer to a TIM_HandleTypeDef structure that contains
**                                    the configuration information for TIM module.
**
**          uint32_t tim_channel_number - TIM Channel to be enabled.
**          This parameter can be one of the following values:
**           TIM_CHANNEL_1: TIM Channel 1 selected
**           TIM_CHANNEL_2: TIM Channel 2 selected
**           TIM_CHANNEL_3: TIM Channel 3 selected
**           TIM_CHANNEL_4: TIM Channel 4 selected
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef hal_tim_pwm_start(TIM_HandleTypeDef *htim, uint32_t tim_channel_number)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint32_t tmp = 0U;

	/* Check the input  parameters */
	if((TRUE == IS_TIM_CHANNELS(tim_channel_number)) && (NULL != htim))
	{
		/* Enable the Capture compare channel */
		tmp = TIM_CCER_CC1E << (tim_channel_number & 0x1FU); /* 0x1FU = 31 bits max shift */

		/* Reset the CCxE Bit */
		htim->Instance->CCER &= ~tmp;

		/* Set the CCxE Bit */
		htim->Instance->CCER |= (uint32_t)(TIM_CCx_ENABLE << (tim_channel_number & 0x1FU)); /* 0x1FU = 31 bits max shift */

		/* Enable the Peripheral */
		htim->Instance->CR1 |= (uint32_t)TIM_CR1_CEN;
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/* Return function status */
	return hal_status;
} 

/*************************************************************************************
** hal_tim_pwm_config_channel - Initializes the TIM PWM required channel.
**
** Params : TIM_HandleTypeDef *htim - Pointer to a TIM_HandleTypeDef structure that contains
**                                    the configuration information for TIM module.
**         uint32_t tim_channel_number -  TIM Channel to be initialized.
**          This parameter can be one of the following values:
**           TIM_CHANNEL_1: TIM Channel 1 selected
**           TIM_CHANNEL_2: TIM Channel 2 selected
**           TIM_CHANNEL_3: TIM Channel 3 selected
**           TIM_CHANNEL_4: TIM Channel 4 selected
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes: Pulse value configured to - 0
*************************************************************************************/
static HAL_StatusTypeDef hal_tim_pwm_config_channel(TIM_HandleTypeDef *htim, uint32_t tim_channel_number)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint32_t tmpccmrx = 0U;
	uint32_t tmpccer  = 0U;

	/* Check the input  parameters */
	if((TRUE == IS_TIM_CHANNELS(tim_channel_number)) && (NULL != htim))
	{
		switch (tim_channel_number)
		{
		case TIM_CHANNEL_1:
			/* Start:----Configure the Channel 1 in PWM mode--- */

			/* Disable the Channel 1: Reset the CC1E Bit */
			htim->Instance->CCER &= ~TIM_CCER_CC1E;

			/* Get the TIMx CCER register value */
			tmpccer = htim->Instance->CCER;

			/* Get the TIMx CCMR1 register value */
			tmpccmrx = htim->Instance->CCMR1;

			/* Reset the Output Compare Mode Bits */
			tmpccmrx &= ~TIM_CCMR1_OC1M;
			tmpccmrx &= ~TIM_CCMR1_CC1S;
			/* Select the Output Compare Mode */
			tmpccmrx |= TIM_OCMODE_PWM1;

			/* Reset the Output Polarity level */
			tmpccer &= ~TIM_CCER_CC1P;

			/* Write to TIMx CCMR1 */
			htim->Instance->CCMR1 = (uint32_t)tmpccmrx;

			/* Set the Capture Compare Register value (PWM Pulse width) */
			htim->Instance->CCR1 = DEFAULT_PULSE_VALUE;

			/* Write to TIMx CCER */
			htim->Instance->CCER = (uint32_t)tmpccer;
			/* End:----Configure the Channel 1 in PWM mode--- */

			/* Set the Preload enable bit for channel1 */
			htim->Instance->CCMR1 |= (uint32_t)TIM_CCMR1_OC1PE;

			/* Configure the Output Fast mode */
			htim->Instance->CCMR1 &= ~TIM_CCMR1_OC1FE;
			htim->Instance->CCMR1 |= TIM_OCFAST_DISABLE;
			break;

		case TIM_CHANNEL_2:
			/* Start:----Configure the Channel 2 in PWM mode--- */

			/* Disable the Channel 2: Reset the CC2E Bit */
			htim->Instance->CCER &= ~TIM_CCER_CC2E;

			/* Get the TIMx CCER register value */
			tmpccer = htim->Instance->CCER;

			/* Get the TIMx CCMR1 register value */
			tmpccmrx = htim->Instance->CCMR1;

			/* Reset the Output Compare mode and Capture/Compare selection Bits */
			tmpccmrx &= ~TIM_CCMR1_OC2M;
			tmpccmrx &= ~TIM_CCMR1_CC2S;

			/* Select the Output Compare Mode */
			tmpccmrx |= (TIM_OCMODE_PWM1 << 8U);

			/* Reset the Output Polarity level */
			tmpccer &= ~TIM_CCER_CC2P;

			/* Write to TIMx CCMR1 */
			htim->Instance->CCMR1 = (uint32_t)tmpccmrx;

			/* Set the Capture Compare Register value (PWM Pulse width) */
			htim->Instance->CCR2 = DEFAULT_PULSE_VALUE;

			/* Write to TIMx CCER */
			htim->Instance->CCER = (uint32_t)tmpccer;
			/* End:----Configure the Channel 2 in PWM mode--- */

			/* Set the Preload enable bit for channel2 */
			htim->Instance->CCMR1 |= (uint32_t)TIM_CCMR1_OC2PE;

			/* Configure the Output Fast mode */
			htim->Instance->CCMR1 &= ~TIM_CCMR1_OC2FE;
			htim->Instance->CCMR1 |= TIM_OCFAST_DISABLE << 8U;
			break;

		case TIM_CHANNEL_3:
			/* Start:----Configure the Channel 3 in PWM mode--- */

			/* Disable the Channel 3: Reset the CC2E Bit */
			htim->Instance->CCER &= ~TIM_CCER_CC3E;

			/* Get the TIMx CCER register value */
			tmpccer = htim->Instance->CCER;

			/* Get the TIMx CCMR2 register value */
			tmpccmrx = htim->Instance->CCMR2;

			/* Reset the Output Compare mode and Capture/Compare selection Bits */
			tmpccmrx &= ~TIM_CCMR2_OC3M;
			tmpccmrx &= ~TIM_CCMR2_CC3S;
			/* Select the Output Compare Mode */
			tmpccmrx |= TIM_OCMODE_PWM1;

			/* Reset the Output Polarity level */
			tmpccer &= ~TIM_CCER_CC3P;

			/* Write to TIMx CCMR2 */
			htim->Instance->CCMR2 = (uint32_t)tmpccmrx;

			/* Set the Capture Compare Register value (PWM Pulse width) */
			htim->Instance->CCR3 = DEFAULT_PULSE_VALUE;

			/* Write to TIMx CCER */
			htim->Instance->CCER = (uint32_t)tmpccer;
			/* End:----Configure the Channel 3 in PWM mode--- */

			/* Set the Preload enable bit for channel3 */
			htim->Instance->CCMR2 |= (uint32_t)TIM_CCMR2_OC3PE;

			/* Configure the Output Fast mode */
			htim->Instance->CCMR2 &= ~TIM_CCMR2_OC3FE;
			htim->Instance->CCMR2 |= TIM_OCFAST_DISABLE;
			break;

		case TIM_CHANNEL_4:
			/* Start:----Configure the Channel 4 in PWM mode--- */

			/* Disable the Channel 4: Reset the CC4E Bit */
			htim->Instance->CCER &= ~TIM_CCER_CC4E;

			/* Get the TIMx CCER register value */
			tmpccer = htim->Instance->CCER;

			/* Get the TIMx CCMR2 register value */
			tmpccmrx = htim->Instance->CCMR2;

			/* Reset the Output Compare mode and Capture/Compare selection Bits */
			tmpccmrx &= ~TIM_CCMR2_OC4M;
			tmpccmrx &= ~TIM_CCMR2_CC4S;

			/* Select the Output Compare Mode */
			tmpccmrx |= (TIM_OCMODE_PWM1 << 8U);

			/* Reset the Output Polarity level */
			tmpccer &= ~TIM_CCER_CC4P;

			/* Write to TIMx CCMR2 */
			htim->Instance->CCMR2 = (uint32_t)tmpccmrx;

			/* Set the Capture Compare Register value (PWM Pulse width) */
			htim->Instance->CCR4 = DEFAULT_PULSE_VALUE;

			/* Write to TIMx CCER */
			htim->Instance->CCER = (uint32_t)tmpccer;
			/* End:----Configure the Channel 4 in PWM mode--- */

			/* Set the Preload enable bit for channel4 */
			htim->Instance->CCMR2 |= (uint32_t)TIM_CCMR2_OC4PE;

			/* Configure the Output Fast mode */
			htim->Instance->CCMR2 &= ~TIM_CCMR2_OC4FE;
			htim->Instance->CCMR2 |= TIM_OCFAST_DISABLE << 8U;
			break;

		default:
			/*Undefined TIM channel*/
			hal_status = HAL_ERROR;
			break;
		}
		if(HAL_OK == hal_status)
		{
			hal_status |= hal_tim_pwm_start(htim,tim_channel_number);
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
