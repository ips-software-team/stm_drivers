/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_can.c
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 20-11-2020
**
** Description: CAN HAL module driver
**            This file provides software functions to manage the following
**            functionalities of the Controller Area Network (CAN) peripheral:
**              + Initialization  functions
**              + Configuration functions
**              + Control functions
**
*/


/* Revision Log:
**
** Rev 1.0  : 08-03-2020 -- Original version created.
** Rev 1.1  : 05-11-2020 -- Addeded CAN Registers definitions.
** Rev 1.2  : 20-11-2020 -- Static analysis update
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_can.h"
#include "gipsy_hal_rcc.h"
#ifdef _DIMC_
#include "can_protocol_dimc.h"
#endif
#ifdef _CTRY_
#include "can_protocol_ctry.h"
#endif
/* -----Definitions ----------------------------------------------------------------*/
#define CAN_TIMEOUT_VALUE_MILIS   10U
#define CAN_DLC           8U /*CAN frame data length*/

#define LOOPBACK_HEADER_ID    0x555U
#define CAN_FLAG_MASK  (0x000000FFU)


/*CAN control and status registers */
/*******************  Bit definition for CAN_MCR register  ********************/
#define CAN_MCR_INRQ_POS       (0U)
#define CAN_MCR_INRQ_MSK       (0x1UL << CAN_MCR_INRQ_POS)                      /*!< 0x00000001 */
#define CAN_MCR_INRQ           CAN_MCR_INRQ_MSK                                /*!<Initialization Request */
#define CAN_MCR_SLEEP_POS      (1U)
#define CAN_MCR_SLEEP_MSK      (0x1UL << CAN_MCR_SLEEP_POS)                     /*!< 0x00000002 */
#define CAN_MCR_SLEEP          CAN_MCR_SLEEP_MSK                               /*!<Sleep Mode Request */
#define CAN_MCR_TXFP_POS       (2U)
#define CAN_MCR_TXFP_MSK       (0x1UL << CAN_MCR_TXFP_POS)                      /*!< 0x00000004 */
#define CAN_MCR_TXFP           CAN_MCR_TXFP_MSK                                /*!<Transmit FIFO Priority */
#define CAN_MCR_RFLM_POS       (3U)
#define CAN_MCR_RFLM_MSK       (0x1UL << CAN_MCR_RFLM_POS)                      /*!< 0x00000008 */
#define CAN_MCR_RFLM           CAN_MCR_RFLM_MSK                                /*!<Receive FIFO Locked Mode */
#define CAN_MCR_NART_POS       (4U)
#define CAN_MCR_NART_MSK       (0x1UL << CAN_MCR_NART_POS)                      /*!< 0x00000010 */
#define CAN_MCR_NART           CAN_MCR_NART_MSK                                /*!<No Automatic Retransmission */
#define CAN_MCR_AWUM_POS       (5U)
#define CAN_MCR_AWUM_MSK       (0x1UL << CAN_MCR_AWUM_POS)                      /*!< 0x00000020 */
#define CAN_MCR_AWUM           CAN_MCR_AWUM_MSK                                /*!<Automatic Wakeup Mode */
#define CAN_MCR_ABOM_POS       (6U)
#define CAN_MCR_ABOM_MSK       (0x1UL << CAN_MCR_ABOM_POS)                      /*!< 0x00000040 */
#define CAN_MCR_ABOM           CAN_MCR_ABOM_MSK                                /*!<Automatic Bus-Off Management */
#define CAN_MCR_TTCM_POS       (7U)
#define CAN_MCR_TTCM_MSK       (0x1UL << CAN_MCR_TTCM_POS)                      /*!< 0x00000080 */
#define CAN_MCR_TTCM           CAN_MCR_TTCM_MSK                                /*!<Time Triggered Communication Mode */

/*******************  Bit definition for CAN_MSR register  ********************/
#define CAN_MSR_INAK_POS       (0U)
#define CAN_MSR_INAK_MSK       (0x1UL << CAN_MSR_INAK_POS)                      /*!< 0x00000001 */
#define CAN_MSR_INAK           CAN_MSR_INAK_MSK                                /*!<Initialization Acknowledge */
#define CAN_MSR_SLAK_POS       (1U)
#define CAN_MSR_SLAK_MSK       (0x1UL << CAN_MSR_SLAK_POS)                      /*!< 0x00000002 */
#define CAN_MSR_SLAK           CAN_MSR_SLAK_MSK                                /*!<Sleep Acknowledge */

/*******************  Bit definition for CAN_TSR register  ********************/
#define CAN_TSR_CODE_POS       (24U)
#define CAN_TSR_CODE_MSK       (0x3UL << CAN_TSR_CODE_POS)                      /*!< 0x03000000 */
#define CAN_TSR_CODE           CAN_TSR_CODE_MSK                                /*!<Mailbox Code */

#define CAN_TSR_TME_POS        (26U)
#define CAN_TSR_TME_MSK        (0x7UL << CAN_TSR_TME_POS)                       /*!< 0x1C000000 */
#define CAN_TSR_TME            CAN_TSR_TME_MSK                                 /*!<TME[2:0] bits */
#define CAN_TSR_TME0_POS       (26U)
#define CAN_TSR_TME0_MSK       (0x1UL << CAN_TSR_TME0_POS)                      /*!< 0x04000000 */
#define CAN_TSR_TME0           CAN_TSR_TME0_MSK                                /*!<Transmit Mailbox 0 Empty */
#define CAN_TSR_TME1_POS       (27U)
#define CAN_TSR_TME1_MSK       (0x1UL << CAN_TSR_TME1_POS)                      /*!< 0x08000000 */
#define CAN_TSR_TME1           CAN_TSR_TME1_MSK                                /*!<Transmit Mailbox 1 Empty */
#define CAN_TSR_TME2_POS       (28U)
#define CAN_TSR_TME2_MSK       (0x1UL << CAN_TSR_TME2_POS)                      /*!< 0x10000000 */
#define CAN_TSR_TME2           CAN_TSR_TME2_MSK                                /*!<Transmit Mailbox 2 Empty */

/*******************  Bit definition for CAN_RF0R register  *******************/
#define CAN_RF0R_FMP0_POS      (0U)
#define CAN_RF0R_FMP0_MSK      (0x3UL << CAN_RF0R_FMP0_POS)                     /*!< 0x00000003 */
#define CAN_RF0R_FMP0          CAN_RF0R_FMP0_MSK                               /*!<FIFO 0 Message Pending */
#define CAN_RF0R_RFOM0_POS     (5U)
#define CAN_RF0R_RFOM0_MSK     (0x1UL << CAN_RF0R_RFOM0_POS)                    /*!< 0x00000020 */
#define CAN_RF0R_RFOM0         CAN_RF0R_RFOM0_MSK                              /*!<Release FIFO 0 Output Mailbox */



/********************  Bit definition for CAN_IER register  *******************/
#define CAN_IER_FMPIE0_POS     (1U)
#define CAN_IER_FMPIE0_MSK     (0x1UL << CAN_IER_FMPIE0_POS)                    /*!< 0x00000002 */
#define CAN_IER_FMPIE0         CAN_IER_FMPIE0_MSK                              /*!<FIFO Message Pending Interrupt Enable */


/*******************  Bit definition for CAN_BTR register  ********************/
#define CAN_BTR_TS1_POS        (16U)
#define CAN_BTR_TS1_MSK        (0xFUL << CAN_BTR_TS1_POS)                       /*!< 0x000F0000 */
#define CAN_BTR_TS1            CAN_BTR_TS1_MSK                                 /*!<Time Segment 1 */
#define CAN_BTR_TS1_0          (0x1UL << CAN_BTR_TS1_POS)                       /*!< 0x00010000 */
#define CAN_BTR_TS1_1          (0x2UL << CAN_BTR_TS1_POS)                       /*!< 0x00020000 */
#define CAN_BTR_TS2_POS        (20U)
#define CAN_BTR_TS2_MSK        (0x7UL << CAN_BTR_TS2_POS)                       /*!< 0x00700000 */
#define CAN_BTR_TS2            CAN_BTR_TS2_MSK                                 /*!<Time Segment 2 */
#define CAN_BTR_TS2_0          (0x1UL << CAN_BTR_TS2_POS)                       /*!< 0x00100000 */
#define CAN_BTR_LBKM_POS       (30U)
#define CAN_BTR_LBKM_MSK       (0x1UL << CAN_BTR_LBKM_POS)                      /*!< 0x40000000 */
#define CAN_BTR_LBKM           CAN_BTR_LBKM_MSK                                /*!<Loop Back Mode (Debug) */
#define CAN_BTR_SILM_POS       (31U)
#define CAN_BTR_SILM_MSK       (0x1UL << CAN_BTR_SILM_POS)                      /*!< 0x80000000 */
#define CAN_BTR_SILM           CAN_BTR_SILM_MSK                                /*!<Silent Mode */


/*CAN Mailbox registers */
/******************  Bit definition for CAN_TI0R register  ********************/
#define CAN_TI0R_TXRQ_POS      (0U)
#define CAN_TI0R_TXRQ_MSK      (0x1UL << CAN_TI0R_TXRQ_POS)                     /*!< 0x00000001 */
#define CAN_TI0R_TXRQ          CAN_TI0R_TXRQ_MSK                               /*!<Transmit Mailbox Request */
#define CAN_TI0R_RTR_POS       (1U)
#define CAN_TI0R_RTR_MSK       (0x1UL << CAN_TI0R_RTR_POS)                      /*!< 0x00000002 */
#define CAN_TI0R_RTR           CAN_TI0R_RTR_MSK                                /*!<Remote Transmission Request */
#define CAN_TI0R_STID_POS      (21U)
#define CAN_TI0R_STID_MSK      (0x7FFUL << CAN_TI0R_STID_POS)                   /*!< 0xFFE00000 */
#define CAN_TI0R_STID          CAN_TI0R_STID_MSK                               /*!<Standard Identifier or Extended Identifier */

/******************  Bit definition for CAN_TDL0R register  *******************/
#define CAN_TDL0R_DATA0_POS    (0U)
#define CAN_TDL0R_DATA0_MSK    (0xFFUL << CAN_TDL0R_DATA0_POS)                  /*!< 0x000000FF */
#define CAN_TDL0R_DATA0        CAN_TDL0R_DATA0_MSK                             /*!<Data byte 0 */
#define CAN_TDL0R_DATA1_POS    (8U)
#define CAN_TDL0R_DATA1_MSK    (0xFFUL << CAN_TDL0R_DATA1_POS)                  /*!< 0x0000FF00 */
#define CAN_TDL0R_DATA1        CAN_TDL0R_DATA1_MSK                             /*!<Data byte 1 */
#define CAN_TDL0R_DATA2_POS    (16U)
#define CAN_TDL0R_DATA2_MSK    (0xFFUL << CAN_TDL0R_DATA2_POS)                  /*!< 0x00FF0000 */
#define CAN_TDL0R_DATA2        CAN_TDL0R_DATA2_MSK                             /*!<Data byte 2 */
#define CAN_TDL0R_DATA3_POS    (24U)
#define CAN_TDL0R_DATA3_MSK    (0xFFUL << CAN_TDL0R_DATA3_POS)                  /*!< 0xFF000000 */
#define CAN_TDL0R_DATA3        CAN_TDL0R_DATA3_MSK                             /*!<Data byte 3 */

/******************  Bit definition for CAN_TDH0R register  *******************/
#define CAN_TDH0R_DATA4_POS    (0U)
#define CAN_TDH0R_DATA4_MSK    (0xFFUL << CAN_TDH0R_DATA4_POS)                  /*!< 0x000000FF */
#define CAN_TDH0R_DATA4        CAN_TDH0R_DATA4_MSK                             /*!<Data byte 4 */
#define CAN_TDH0R_DATA5_POS    (8U)
#define CAN_TDH0R_DATA5_MSK    (0xFFUL << CAN_TDH0R_DATA5_POS)                  /*!< 0x0000FF00 */
#define CAN_TDH0R_DATA5        CAN_TDH0R_DATA5_MSK                             /*!<Data byte 5 */
#define CAN_TDH0R_DATA6_POS    (16U)
#define CAN_TDH0R_DATA6_MSK    (0xFFUL << CAN_TDH0R_DATA6_POS)                  /*!< 0x00FF0000 */
#define CAN_TDH0R_DATA6        CAN_TDH0R_DATA6_MSK                             /*!<Data byte 6 */
#define CAN_TDH0R_DATA7_POS    (24U)
#define CAN_TDH0R_DATA7_MSK    (0xFFUL << CAN_TDH0R_DATA7_POS)                  /*!< 0xFF000000 */
#define CAN_TDH0R_DATA7        CAN_TDH0R_DATA7_MSK                             /*!<Data byte 7 */


/*******************  Bit definition for CAN_TDH2R register  ******************/
#define CAN_TDH2R_DATA4_POS    (0U)
#define CAN_TDH2R_DATA4_MSK    (0xFFUL << CAN_TDH2R_DATA4_POS)                  /*!< 0x000000FF */
#define CAN_TDH2R_DATA4        CAN_TDH2R_DATA4_MSK                             /*!<Data byte 4 */
#define CAN_TDH2R_DATA5_POS    (8U)
#define CAN_TDH2R_DATA5_MSK    (0xFFUL << CAN_TDH2R_DATA5_POS)                  /*!< 0x0000FF00 */
#define CAN_TDH2R_DATA5        CAN_TDH2R_DATA5_MSK                             /*!<Data byte 5 */
#define CAN_TDH2R_DATA6_POS    (16U)
#define CAN_TDH2R_DATA6_MSK    (0xFFUL << CAN_TDH2R_DATA6_POS)                  /*!< 0x00FF0000 */
#define CAN_TDH2R_DATA6        CAN_TDH2R_DATA6_MSK                             /*!<Data byte 6 */
#define CAN_TDH2R_DATA7_POS    (24U)
#define CAN_TDH2R_DATA7_MSK    (0xFFUL << CAN_TDH2R_DATA7_POS)                  /*!< 0xFF000000 */
#define CAN_TDH2R_DATA7        CAN_TDH2R_DATA7_MSK                             /*!<Data byte 7 */

/*******************  Bit definition for CAN_RI0R register  *******************/
#define CAN_RI0R_RTR_POS       (1U)
#define CAN_RI0R_RTR_MSK       (0x1UL << CAN_RI0R_RTR_POS)                      /*!< 0x00000002 */
#define CAN_RI0R_RTR           CAN_RI0R_RTR_MSK                                /*!<Remote Transmission Request */
#define CAN_RI0R_IDE_POS       (2U)
#define CAN_RI0R_IDE_MSK       (0x1UL << CAN_RI0R_IDE_POS)                      /*!< 0x00000004 */
#define CAN_RI0R_IDE           CAN_RI0R_IDE_MSK                                /*!<Identifier Extension */
#define CAN_RI0R_STID_POS      (21U)
#define CAN_RI0R_STID_MSK      (0x7FFUL << CAN_RI0R_STID_POS)                   /*!< 0xFFE00000 */
#define CAN_RI0R_STID          CAN_RI0R_STID_MSK                               /*!<Standard Identifier or Extended Identifier */

/*******************  Bit definition for CAN_RDT0R register  ******************/
#define CAN_RDT0R_DLC_POS      (0U)
#define CAN_RDT0R_DLC_MSK      (0xFUL << CAN_RDT0R_DLC_POS)                     /*!< 0x0000000F */
#define CAN_RDT0R_DLC          CAN_RDT0R_DLC_MSK                               /*!<Data Length Code */

/*******************  Bit definition for CAN_RDL0R register  ******************/
#define CAN_RDL0R_DATA0_POS    (0U)
#define CAN_RDL0R_DATA0_MSK    (0xFFUL << CAN_RDL0R_DATA0_POS)                  /*!< 0x000000FF */
#define CAN_RDL0R_DATA0        CAN_RDL0R_DATA0_MSK                             /*!<Data byte 0 */
#define CAN_RDL0R_DATA1_POS    (8U)
#define CAN_RDL0R_DATA1_MSK    (0xFFUL << CAN_RDL0R_DATA1_POS)                  /*!< 0x0000FF00 */
#define CAN_RDL0R_DATA1        CAN_RDL0R_DATA1_MSK                             /*!<Data byte 1 */
#define CAN_RDL0R_DATA2_POS    (16U)
#define CAN_RDL0R_DATA2_MSK    (0xFFUL << CAN_RDL0R_DATA2_POS)                  /*!< 0x00FF0000 */
#define CAN_RDL0R_DATA2        CAN_RDL0R_DATA2_MSK                             /*!<Data byte 2 */
#define CAN_RDL0R_DATA3_POS    (24U)
#define CAN_RDL0R_DATA3_MSK    (0xFFUL << CAN_RDL0R_DATA3_POS)                  /*!< 0xFF000000 */
#define CAN_RDL0R_DATA3        CAN_RDL0R_DATA3_MSK                             /*!<Data byte 3 */

/*******************  Bit definition for CAN_RDH0R register  ******************/
#define CAN_RDH0R_DATA4_POS    (0U)
#define CAN_RDH0R_DATA4_MSK    (0xFFUL << CAN_RDH0R_DATA4_POS)                  /*!< 0x000000FF */
#define CAN_RDH0R_DATA4        CAN_RDH0R_DATA4_MSK                             /*!<Data byte 4 */
#define CAN_RDH0R_DATA5_POS    (8U)
#define CAN_RDH0R_DATA5_MSK    (0xFFUL << CAN_RDH0R_DATA5_POS)                  /*!< 0x0000FF00 */
#define CAN_RDH0R_DATA5        CAN_RDH0R_DATA5_MSK                             /*!<Data byte 5 */
#define CAN_RDH0R_DATA6_POS    (16U)
#define CAN_RDH0R_DATA6_MSK    (0xFFUL << CAN_RDH0R_DATA6_POS)                  /*!< 0x00FF0000 */
#define CAN_RDH0R_DATA6        CAN_RDH0R_DATA6_MSK                             /*!<Data byte 6 */
#define CAN_RDH0R_DATA7_POS    (24U)
#define CAN_RDH0R_DATA7_MSK    (0xFFUL << CAN_RDH0R_DATA7_POS)                  /*!< 0xFF000000 */
#define CAN_RDH0R_DATA7        CAN_RDH0R_DATA7_MSK                             /*!<Data byte 7 */

/*!<CAN filter registers */
/*******************  Bit definition for CAN_FMR register  ********************/
#define CAN_FMR_FINIT_POS      (0U)
#define CAN_FMR_FINIT_MSK      (0x1UL << CAN_FMR_FINIT_POS)                     /*!< 0x00000001 */
#define CAN_FMR_FINIT          CAN_FMR_FINIT_MSK                               /*!<Filter Init Mode */
#define CAN_FMR_CAN2SB_POS     (8U)
#define CAN_FMR_CAN2SB_MSK     (0x3FUL << CAN_FMR_CAN2SB_POS)                   /*!< 0x00003F00 */
#define CAN_FMR_CAN2SB         CAN_FMR_CAN2SB_MSK                              /*!<CAN2 start bank */






#define CAN_RX_FIFO0                (0x00000000U)  /*!< CAN receive FIFO 0 */
#define CAN_RTR_DATA                (0x00000000U)  /*!< Data frame   */

#define INAK_ENTER_STATE            (0X00000001U)  /*INAK state when entering init mode*/
#define INAK_EXIT_STATE             (0X00000000U)  /*INAK state when leaving init mode*/
/*CAN_Error_Code CAN Error Code*/
#define HAL_CAN_ERROR_NONE            (0x00000000U)  /*!< No error                                             */
#define HAL_CAN_ERROR_EWG             (0x00000001U)  /*!< Protocol Error Warning                               */
#define HAL_CAN_ERROR_EPV             (0x00000002U)  /*!< Error Passive                                        */
#define HAL_CAN_ERROR_BOF             (0x00000004U)  /*!< Bus-off error                                        */
#define HAL_CAN_ERROR_STF             (0x00000008U)  /*!< Stuff error                                          */
#define HAL_CAN_ERROR_FOR             (0x00000010U)  /*!< Form error                                           */
#define HAL_CAN_ERROR_ACK             (0x00000020U)  /*!< Acknowledgment error                                 */
#define HAL_CAN_ERROR_BR              (0x00000040U)  /*!< Bit recessive error                                  */
#define HAL_CAN_ERROR_BD              (0x00000080U)  /*!< Bit dominant error                                   */
#define HAL_CAN_ERROR_CRC             (0x00000100U)  /*!< CRC error                                            */
#define HAL_CAN_ERROR_RX_FOV0         (0x00000200U)  /*!< Rx FIFO0 overrun error                               */
#define HAL_CAN_ERROR_RX_FOV1         (0x00000400U)  /*!< Rx FIFO1 overrun error                               */
#define HAL_CAN_ERROR_TX_ALST0        (0x00000800U)  /*!< TxMailbox 0 transmit failure due to arbitration lost */
#define HAL_CAN_ERROR_TX_TERR0        (0x00001000U)  /*!< TxMailbox 1 transmit failure due to tranmit error    */
#define HAL_CAN_ERROR_TX_ALST1        (0x00002000U)  /*!< TxMailbox 0 transmit failure due to arbitration lost */
#define HAL_CAN_ERROR_TX_TERR1        (0x00004000U)  /*!< TxMailbox 1 transmit failure due to tranmit error    */
#define HAL_CAN_ERROR_TX_ALST2        (0x00008000U)  /*!< TxMailbox 0 transmit failure due to arbitration lost */
#define HAL_CAN_ERROR_TX_TERR2        (0x00010000U)  /*!< TxMailbox 1 transmit failure due to tranmit error    */
#define HAL_CAN_ERROR_TIMEOUT         (0x00020000U)  /*!< Timeout error                                        */
#define HAL_CAN_ERROR_NOT_INITIALIZED (0x00040000U)  /*!< Peripheral not initialized                           */
#define HAL_CAN_ERROR_NOT_READY       (0x00080000U)  /*!< Peripheral not ready                                 */
#define HAL_CAN_ERROR_PARAM           (0x00200000U)  /*!< Parameter error                                      */
#define HAL_CAN_LOOPBACK_ERROR        (0x01000000U)  /*!< Loop back test error                                 */

/*CAN_operating_mode CAN Operating Mode*/
#define CAN_MODE_NORMAL             (0x00000000U)                              /*!< Normal mode   */
#define CAN_MODE_LOOPBACK           ((uint32_t)CAN_BTR_LBKM)                   /*!< Loopback mode */

/*CAN_synchronisation_jump_width CAN Synchronization Jump Width*/
#define CAN_SJW_1TQ                 (0x00000000U)              /*!< 1 time quantum */
#define CAN_SJW_2TQ                 ((uint32_t)CAN_BTR_SJW_0)  /*!< 2 time quantum */
#define CAN_SJW_3TQ                 ((uint32_t)CAN_BTR_SJW_1)  /*!< 3 time quantum */
#define CAN_SJW_4TQ                 ((uint32_t)CAN_BTR_SJW)    /*!< 4 time quantum */

/*CAN_time_quantum_in_bit_segment_1 CAN Time Quantum in Bit Segment 1*/
#define CAN_BS1_1TQ                 (0x00000000U)                                                /*!< 1 time quantum  */
#define CAN_BS1_2TQ                 ((uint32_t)CAN_BTR_TS1_0)                                    /*!< 2 time quantum  */
#define CAN_BS1_3TQ                 ((uint32_t)CAN_BTR_TS1_1)                                    /*!< 3 time quantum  */
#define CAN_BS1_4TQ                 ((uint32_t)(CAN_BTR_TS1_1 | CAN_BTR_TS1_0))                  /*!< 4 time quantum  */
#define CAN_BS1_5TQ                 ((uint32_t)CAN_BTR_TS1_2)                                    /*!< 5 time quantum  */
#define CAN_BS1_6TQ                 ((uint32_t)(CAN_BTR_TS1_2 | CAN_BTR_TS1_0))                  /*!< 6 time quantum  */
#define CAN_BS1_7TQ                 ((uint32_t)(CAN_BTR_TS1_2 | CAN_BTR_TS1_1))                  /*!< 7 time quantum  */
#define CAN_BS1_8TQ                 ((uint32_t)(CAN_BTR_TS1_2 | CAN_BTR_TS1_1 | CAN_BTR_TS1_0))  /*!< 8 time quantum  */
#define CAN_BS1_9TQ                 ((uint32_t)CAN_BTR_TS1_3)                                    /*!< 9 time quantum  */
#define CAN_BS1_10TQ                ((uint32_t)(CAN_BTR_TS1_3 | CAN_BTR_TS1_0))                  /*!< 10 time quantum */
#define CAN_BS1_11TQ                ((uint32_t)(CAN_BTR_TS1_3 | CAN_BTR_TS1_1))                  /*!< 11 time quantum */
#define CAN_BS1_12TQ                ((uint32_t)(CAN_BTR_TS1_3 | CAN_BTR_TS1_1 | CAN_BTR_TS1_0))  /*!< 12 time quantum */
#define CAN_BS1_13TQ                ((uint32_t)(CAN_BTR_TS1_3 | CAN_BTR_TS1_2))                  /*!< 13 time quantum */
#define CAN_BS1_14TQ                ((uint32_t)(CAN_BTR_TS1_3 | CAN_BTR_TS1_2 | CAN_BTR_TS1_0))  /*!< 14 time quantum */
#define CAN_BS1_15TQ                ((uint32_t)(CAN_BTR_TS1_3 | CAN_BTR_TS1_2 | CAN_BTR_TS1_1))  /*!< 15 time quantum */
#define CAN_BS1_16TQ                ((uint32_t)CAN_BTR_TS1) /*!< 16 time quantum */

/*CAN_time_quantum_in_bit_segment_2 CAN Time Quantum in Bit Segment 2*/
#define CAN_BS2_1TQ                 (0x00000000U)                                /*!< 1 time quantum */
#define CAN_BS2_2TQ                 ((uint32_t)CAN_BTR_TS2_0)                    /*!< 2 time quantum */
#define CAN_BS2_3TQ                 ((uint32_t)CAN_BTR_TS2_1)                    /*!< 3 time quantum */
#define CAN_BS2_4TQ                 ((uint32_t)(CAN_BTR_TS2_1 | CAN_BTR_TS2_0))  /*!< 4 time quantum */
#define CAN_BS2_5TQ                 ((uint32_t)CAN_BTR_TS2_2)                    /*!< 5 time quantum */
#define CAN_BS2_6TQ                 ((uint32_t)(CAN_BTR_TS2_2 | CAN_BTR_TS2_0))  /*!< 6 time quantum */
#define CAN_BS2_7TQ                 ((uint32_t)(CAN_BTR_TS2_2 | CAN_BTR_TS2_1))  /*!< 7 time quantum */
#define CAN_BS2_8TQ                 ((uint32_t)CAN_BTR_TS2)                      /*!< 8 time quantum */

/*CAN_Tx_Mailboxes CAN Tx Mailboxes*/
#define CAN_TX_MAILBOX0             (0x00000001U)  /*!< Tx Mailbox 0  */
#define CAN_TX_MAILBOX1             (0x00000002U)  /*!< Tx Mailbox 1  */
#define CAN_TX_MAILBOX2             (0x00000004U)  /*!< Tx Mailbox 2  */

/*CAN_flags CAN Flags*/
/* Transmit Flags */
#define CAN_FLAG_RQCP0              (0x00000500U)  /*!< Request complete MailBox 0 flag   */
#define CAN_FLAG_TXOK0              (0x00000501U)  /*!< Transmission OK MailBox 0 flag    */
#define CAN_FLAG_ALST0              (0x00000502U)  /*!< Arbitration Lost MailBox 0 flag   */
#define CAN_FLAG_TERR0              (0x00000503U)  /*!< Transmission error MailBox 0 flag */
#define CAN_FLAG_RQCP1              (0x00000508U)  /*!< Request complete MailBox1 flag    */
#define CAN_FLAG_TXOK1              (0x00000509U)  /*!< Transmission OK MailBox 1 flag    */
#define CAN_FLAG_ALST1              (0x0000050AU)  /*!< Arbitration Lost MailBox 1 flag   */
#define CAN_FLAG_TERR1              (0x0000050BU)  /*!< Transmission error MailBox 1 flag */
#define CAN_FLAG_RQCP2              (0x00000510U)  /*!< Request complete MailBox2 flag    */
#define CAN_FLAG_TXOK2              (0x00000511U)  /*!< Transmission OK MailBox 2 flag    */
#define CAN_FLAG_ALST2              (0x00000512U)  /*!< Arbitration Lost MailBox 2 flag   */
#define CAN_FLAG_TERR2              (0x00000513U)  /*!< Transmission error MailBox 2 flag */
#define CAN_FLAG_TME0               (0x0000051AU)  /*!< Transmit mailbox 0 empty flag     */
#define CAN_FLAG_TME1               (0x0000051BU)  /*!< Transmit mailbox 1 empty flag     */
#define CAN_FLAG_TME2               (0x0000051CU)  /*!< Transmit mailbox 2 empty flag     */
#define CAN_FLAG_LOW0               (0x0000051DU)  /*!< Lowest priority mailbox 0 flag    */
#define CAN_FLAG_LOW1               (0x0000051EU)  /*!< Lowest priority mailbox 1 flag    */
#define CAN_FLAG_LOW2               (0x0000051FU)  /*!< Lowest priority mailbox 2 flag    */

/* Receive Flags */
#define CAN_FLAG_FF0                (0x00000203U)  /*!< RX FIFO 0 Full flag               */
#define CAN_FLAG_FOV0               (0x00000204U)  /*!< RX FIFO 0 Overrun flag            */
#define CAN_FLAG_FF1                (0x00000403U)  /*!< RX FIFO 1 Full flag               */
#define CAN_FLAG_FOV1               (0x00000404U)  /*!< RX FIFO 1 Overrun flag            */

/* Operating Mode Flags */
#define CAN_FLAG_INAK               (0x00000100U)  /*!< Initialization acknowledge flag   */
#define CAN_FLAG_SLAK               (0x00000101U)  /*!< Sleep acknowledge flag            */
#define CAN_FLAG_ERRI               (0x00000102U)  /*!< Error flag                        */
#define CAN_FLAG_WKU                (0x00000103U)  /*!< Wake up interrupt flag            */
#define CAN_FLAG_SLAKI              (0x00000104U)  /*!< Sleep acknowledge interrupt flag  */

/* Error Flags */
#define CAN_FLAG_EWG                (0x00000300U)  /*!< Error warning flag                */
#define CAN_FLAG_EPV                (0x00000301U)  /*!< Error passive flag                */
#define CAN_FLAG_BOF                (0x00000302U)  /*!< Bus-Off flag                      */

/*CAN_Interrupts CAN Interrupts*/

/* Receive Interrupts */
#define CAN_IT_RX_FIFO0_MSG_PENDING ((uint32_t)CAN_IER_FMPIE0)  /*!< FIFO 0 message pending interrupt */
#define CAN_IT_RX_FIFO0_FULL        ((uint32_t)CAN_IER_FFIE0)   /*!< FIFO 0 full interrupt            */
#define CAN_IT_RX_FIFO0_OVERRUN     ((uint32_t)CAN_IER_FOVIE0)  /*!< FIFO 0 overrun interrupt         */
#define CAN_IT_RX_FIFO1_MSG_PENDING ((uint32_t)CAN_IER_FMPIE1)  /*!< FIFO 1 message pending interrupt */
#define CAN_IT_RX_FIFO1_FULL        ((uint32_t)CAN_IER_FFIE1)   /*!< FIFO 1 full interrupt            */
#define CAN_IT_RX_FIFO1_OVERRUN     ((uint32_t)CAN_IER_FOVIE1)  /*!< FIFO 1 overrun interrupt         */

/* Operating Mode Interrupts */
#define CAN_IT_WAKEUP               ((uint32_t)CAN_IER_WKUIE)   /*!< Wake-up interrupt                */
#define CAN_IT_SLEEP_ACK            ((uint32_t)CAN_IER_SLKIE)   /*!< Sleep acknowledge interrupt      */

/* Error Interrupts */
#define CAN_IT_ERROR_WARNING        ((uint32_t)CAN_IER_EWGIE)   /*!< Error warning interrupt          */
#define CAN_IT_ERROR_PASSIVE        ((uint32_t)CAN_IER_EPVIE)   /*!< Error passive interrupt          */
#define CAN_IT_BUSOFF               ((uint32_t)CAN_IER_BOFIE)   /*!< Bus-off interrupt                */
#define CAN_IT_LAST_ERROR_CODE      ((uint32_t)CAN_IER_LECIE)   /*!< Last error code interrupt        */
#define CAN_IT_ERROR                ((uint32_t)CAN_IER_ERRIE)   /*!< Error Interrupt                  */





/* -----Macros ---------------------------------------------------------------------*/
#define IS_CAN_STDID(STDID)   ((STDID) <= 0x7FFU)

/**
  * @brief  Enable the specified CAN interrupts.
  * @param  __HANDLE__ CAN handle.
  * @param  __INTERRUPT__ CAN Interrupt sources to enable.
  *           This parameter can be any combination of @arg CAN_Interrupts
  * @retval None
  */
#define _HAL_CAN_ENABLE_IT(__HANDLE__, __INTERRUPT__) (((__HANDLE__).Instance->IER) |= (__INTERRUPT__))

/**
  * @brief  Disable the specified CAN interrupts.
  * @param  __HANDLE__ CAN handle.
  * @param  __INTERRUPT__ CAN Interrupt sources to disable.
  *           This parameter can be any combination of @arg CAN_Interrupts
  * @retval None
  */
#define _HAL_CAN_DISABLE_IT(__HANDLE__, __INTERRUPT__) (((__HANDLE__).Instance->IER) &= ~(__INTERRUPT__))

/** @brief  Check if the specified CAN interrupt source is enabled or disabled.
  * @param  __HANDLE__ specifies the CAN Handle.
  * @param  __INTERRUPT__ specifies the CAN interrupt source to check.
  *           This parameter can be a value of @arg CAN_Interrupts
  * @retval The state of __IT__ (TRUE or FALSE).
  */
#define _HAL_CAN_GET_IT_SOURCE(__HANDLE__, __INTERRUPT__) (((__HANDLE__)->Instance->IER) & (__INTERRUPT__))


/** @brief  Clear the specified CAN pending flag.
  * @param  __HANDLE__ specifies the CAN Handle.
  * @param  __FLAG__ specifies the flag to check.
  *         This parameter can be one of the following values:
  *            @arg CAN_FLAG_RQCP0: Request complete MailBox 0 Flag
  *            @arg CAN_FLAG_TXOK0: Transmission OK MailBox 0 Flag
  *            @arg CAN_FLAG_ALST0: Arbitration Lost MailBox 0 Flag
  *            @arg CAN_FLAG_TERR0: Transmission error MailBox 0 Flag
  *            @arg CAN_FLAG_RQCP1: Request complete MailBox 1 Flag
  *            @arg CAN_FLAG_TXOK1: Transmission OK MailBox 1 Flag
  *            @arg CAN_FLAG_ALST1: Arbitration Lost MailBox 1 Flag
  *            @arg CAN_FLAG_TERR1: Transmission error MailBox 1 Flag
  *            @arg CAN_FLAG_RQCP2: Request complete MailBox 2 Flag
  *            @arg CAN_FLAG_TXOK2: Transmission OK MailBox 2 Flag
  *            @arg CAN_FLAG_ALST2: Arbitration Lost MailBox 2 Flag
  *            @arg CAN_FLAG_TERR2: Transmission error MailBox 2 Flag
  *            @arg CAN_FLAG_FF0:   RX FIFO 0 Full Flag
  *            @arg CAN_FLAG_FOV0:  RX FIFO 0 Overrun Flag
  *            @arg CAN_FLAG_FF1:   RX FIFO 1 Full Flag
  *            @arg CAN_FLAG_FOV1:  RX FIFO 1 Overrun Flag
  *            @arg CAN_FLAG_WKUI:  Wake up Interrupt Flag
  *            @arg CAN_FLAG_SLAKI: Sleep acknowledge Interrupt Flag
  * @retval None
  */
#define _HAL_CAN_CLEAR_FLAG(__HANDLE__, __FLAG__) \
  ((((__FLAG__) >> 8U) == 5U)? (((__HANDLE__).Instance->TSR) = (1U << ((__FLAG__) & CAN_FLAG_MASK))): \
   (((__FLAG__) >> 8U) == 2U)? (((__HANDLE__).Instance->RF0R) = (1U << ((__FLAG__) & CAN_FLAG_MASK))): \
   (((__FLAG__) >> 8U) == 4U)? (((__HANDLE__).Instance->RF1R) = (1U << ((__FLAG__) & CAN_FLAG_MASK))): \
   (((__FLAG__) >> 8U) == 1U)? (((__HANDLE__).Instance->MSR) = (1U << ((__FLAG__) & CAN_FLAG_MASK))): 0U)

/* -----Global variables -----------------------------------------------------------*/


/*CAN handle Structure*/
 struct CAN_HandleTypeDef
{
  CAN_TypeDef                    *Instance;                 /*!< Register base address */
  CAN_InitTypeDef                 Init;                      /*!< CAN required parameters */
  volatile HAL_CAN_StateTypeDef   State;                     /*!< CAN communication state */
  volatile uint32_t               ErrorCode;                 /*!< CAN Error code. This parameter can be a value of CAN_Error_Code */
} hCAN1=
{
	/*Configure the CAN1 peripheral*/
	.Instance = CAN1,
	.State = E_HAL_CAN_STATE_RESET,
	.Init.TimeTriggeredMode = (uint32_t)DISABLE,
	.Init.AutoBusOff = (uint32_t)DISABLE,
	.Init.AutoWakeUp = (uint32_t)DISABLE,
	.Init.AutoRetransmission = (uint32_t)ENABLE,
	.Init.ReceiveFifoLocked = (uint32_t)DISABLE,
	.Init.TransmitFifoPriority = (uint32_t)DISABLE,
	.Init.Mode = CAN_MODE_NORMAL,
	.Init.SyncJumpWidth = CAN_SJW_1TQ,
	.Init.TimeSeg1 = CAN_BS1_4TQ,
	.Init.TimeSeg2 = CAN_BS2_2TQ,
	.Init.Prescaler = 24  /* defines 250Khz speed*/
};


/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
	static void config_can_filter(void);
	static HAL_StatusTypeDef wait_init_ack(uint32_t inak_required_state);
	static HAL_StatusTypeDef can_loopback_test(void);
/* -----Modules implementation -----------------------------------------------------*/



/*************************************************************************************
** HAL_CAN_Init - 1. Initializes the CAN peripheral according to the specified
**                    parameters in the CAN_InitStruct.
**                 2. Configures CAN HW filters.
**                 3. Starts CAN peripheral
**                 4. Performs CAN Loop Test
**                 5. Enables CAN interrupts
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_CAN_Init(void)
{
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;
	HAL_StatusTypeDef loopback_test_status = HAL_OK;

	/* Initialize the error code */
	hCAN1.ErrorCode = HAL_CAN_ERROR_NONE;
	/* Initialize the CAN state */
	hCAN1.State = E_HAL_CAN_STATE_READY;

	/* Request CAN peripheral to exit from sleep mode (After power-up CAN automatically enters sleep mode SLEEP=1) */
	CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_SLEEP);

	tickstart = HAL_GetTick();

	/* Check Sleep mode exit acknowledge  SLAK bit shall be cleared by HW*/
	while ((uint32_t)RESET != (hCAN1.Instance->MSR & CAN_MSR_SLAK))
	{
		if ((HAL_GetTick() - tickstart) > CAN_TIMEOUT_VALUE_MILIS)
		{
			/* Update error code */
			hCAN1.ErrorCode |= HAL_CAN_ERROR_TIMEOUT;
			/* Change CAN state */
			hCAN1.State = E_HAL_CAN_STATE_ERROR;
			hal_status = HAL_ERROR;
			break; /*Exit loop on timeout*/
		}
	}
	/*Request to start CAN init mode*/
	if(HAL_OK == hal_status)
	{
		/* Enter CAN initialization mode*/
		SET_BIT(hCAN1.Instance->MCR, CAN_MCR_INRQ);
		hal_status = wait_init_ack(INAK_ENTER_STATE);
	}
	/*Init CAN1 peripheral parameters*/
	if(HAL_OK == hal_status)
	{
		/*Start CAN peripheral initialization*/
		/* Set the time triggered communication mode */
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_TTCM);
		/* Set the automatic bus-off management */
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_ABOM);
		/* Set the automatic wake-up mode */
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_AWUM);
		/* Set the automatic retransmission */
		SET_BIT(hCAN1.Instance->MCR, CAN_MCR_NART);
		/* Set the receive FIFO locked mode */
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_RFLM);
		/* Set the transmit FIFO priority */
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_TXFP);
		/* Set the bit timing register */
		WRITE_REG(hCAN1.Instance->BTR, (uint32_t)(hCAN1.Init.Mode |
				hCAN1.Init.SyncJumpWidth  |
				hCAN1.Init.TimeSeg1       |
				hCAN1.Init.TimeSeg2       |
				(hCAN1.Init.Prescaler - 1U)));

		/*----Config CAN HW filter-----------------------*/
		config_can_filter();

		/* Set CAN peripheral in Loop back combined with silent mode */
		SET_BIT(hCAN1.Instance->BTR, CAN_BTR_LBKM);
		SET_BIT(hCAN1.Instance->BTR, CAN_BTR_SILM);

		/* Request leave initialization and enter Loop back combined with silent mode*/
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_INRQ);
		hal_status = wait_init_ack(INAK_EXIT_STATE);
	}
	/*Perform Loopback test */
	if(HAL_OK == hal_status)
	{
		loopback_test_status = can_loopback_test();
	}
	/*Request to start CAN init mode*/
	if(HAL_OK == hal_status)
	{
		/* Enter CAN initialization mode*/
		SET_BIT(hCAN1.Instance->MCR, CAN_MCR_INRQ);
		hal_status = wait_init_ack(INAK_ENTER_STATE);
	}
	/* Set CAN peripheral in normal mode */
	if(HAL_OK == hal_status)
	{
		/* Deactivate CAN peripheral's Loop back combined with silent mode */
		CLEAR_BIT(hCAN1.Instance->BTR, CAN_BTR_LBKM);
		CLEAR_BIT(hCAN1.Instance->BTR, CAN_BTR_SILM);
		/* Request leave initialization and enter normal mode*/
		CLEAR_BIT(hCAN1.Instance->MCR, CAN_MCR_INRQ);
		hal_status = wait_init_ack(INAK_EXIT_STATE);
	}
	/* Enable CAN interrupts: CAN_IT_RX_FIFO0_MSG_PENDING*/
	if(HAL_OK == hal_status)
	{
		_HAL_CAN_ENABLE_IT(hCAN1, CAN_IT_RX_FIFO0_MSG_PENDING);
	}

	/*Update HAL status and CAN state*/
	if((HAL_OK != hal_status) || (HAL_OK != loopback_test_status))
	{
		hCAN1.State = E_HAL_CAN_STATE_ERROR;
		hal_status = HAL_ERROR;
	}


	/* Return function status */
	return hal_status;
}
/*************************************************************************************
** wait_init_ack - Waits till INAK required state set by HW or till timeout
**
** Params: uint32_t inak_required_state - INAK required state(INAK_EXIT_STATE or INAK_ENTER_STATE)
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef wait_init_ack(uint32_t inak_required_state)
{
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if((INAK_ENTER_STATE == inak_required_state) || (INAK_EXIT_STATE == inak_required_state))
	{
		/* Get tick */
		tickstart = HAL_GetTick();

		/* Wait for the acknowledge */
		while (inak_required_state != (hCAN1.Instance->MSR & CAN_MSR_INAK))
		{
			/* Check for the Timeout */
			if ((HAL_GetTick() - tickstart) > CAN_TIMEOUT_VALUE_MILIS)
			{
				/* Update error code */
				hCAN1.ErrorCode |= HAL_CAN_ERROR_TIMEOUT;
				/* Change CAN state */
				hCAN1.State = E_HAL_CAN_STATE_ERROR;
				hal_status = HAL_ERROR;
				break; /*Exit loop on timeout*/
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/* Return function status */
	return hal_status;
}
/*************************************************************************************
** config_can_filter - Performs CAN filter initialization and configuration.
**
** Params:  None.
** Returns: None.
** Notes:
*************************************************************************************/
static void config_can_filter(void)
{
	uint32_t filternbrbitpos = 0U;

	const uint32_t FILTER_BANK = 0U;
	const uint32_t FILTER_ID_MASK_HIGH = 0x00000000U;
	const uint32_t FILTER_ID_MASK_LOW = 0x000000000U;
	const uint32_t SLAVE_START_FILTER_BANK = 14U;

	/* Initialization mode for the filter */
	SET_BIT(hCAN1.Instance->FMR, CAN_FMR_FINIT);

	/* Select the start filter number  */
	CLEAR_BIT(hCAN1.Instance->FMR, CAN_FMR_CAN2SB);
	SET_BIT(hCAN1.Instance->FMR, SLAVE_START_FILTER_BANK << CAN_FMR_CAN2SB_POS);

	/* Convert filter number into bit position */
	filternbrbitpos = (1U) << FILTER_BANK;

	/* Filter De-activation */
	CLEAR_BIT(hCAN1.Instance->FA1R, filternbrbitpos);

	/* 32-bit scale for the filter */
	SET_BIT(hCAN1.Instance->FS1R, filternbrbitpos);

	/* 32-bit identifier or First 32-bit identifier */
	hCAN1.Instance->sFilterRegister[FILTER_BANK].FR1 =
			((0x0000FFFFU & FILTER_ID_MASK_HIGH) << 16U) |
			(0x0000FFFFU & FILTER_ID_MASK_LOW);

	/* 32-bit mask or Second 32-bit identifier */
	hCAN1.Instance->sFilterRegister[FILTER_BANK].FR2 =
			((0x0000FFFFU & FILTER_ID_MASK_HIGH) << 16U) |
			(0x0000FFFFU & FILTER_ID_MASK_LOW);


	/* Filter Mode configuration */
	/* Id/Mask mode for the filter*/
	CLEAR_BIT(hCAN1.Instance->FM1R, filternbrbitpos);

	/* Filter FIFO assignment */
	/* FIFO 0 assignation for the filter */
	CLEAR_BIT(hCAN1.Instance->FFA1R, filternbrbitpos);

	/* Filter activation */
	SET_BIT(hCAN1.Instance->FA1R, filternbrbitpos);

	/* Leave the initialization mode for the filter */
	CLEAR_BIT(hCAN1.Instance->FMR, CAN_FMR_FINIT);
}

/*************************************************************************************
** can_loopback_test - Performs CAN peripheral Loop-Back test by transmitting pre-defined
**                     frame and reading it from RX buffer
**
** Params:  None.
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef can_loopback_test(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	CAN_RxHeaderTypeDef rx_header = {0U};
	uint8_t test_failed_flag = FALSE;
	uint8_t rx_loopback_data[CAN_DLC] = {0U};
	uint8_t tx_loopback_data[CAN_DLC] = {0xAAU,0x55U,0xAAU,0x55U,0xAAU,0x55U,0xAAU,0x55U};
	if(HAL_OK == HAL_CAN_AddTxMessage(LOOPBACK_HEADER_ID,tx_loopback_data))
	{
		HAL_Delay(1U);
		if(HAL_OK == HAL_CAN_GetRxMessage(&rx_header, rx_loopback_data))
		{
			for(uint8_t i = 0U; i < CAN_DLC; i++)
			{
				if(tx_loopback_data[i] != rx_loopback_data[i])
				{
					test_failed_flag = TRUE;
					break; /*Exit test loop if single data byte comparison failed*/
				}
			}
			if((TRUE == test_failed_flag) || (LOOPBACK_HEADER_ID != rx_header.StdId))
			{
				hal_status = HAL_ERROR;
			}
		}
		else
		{
			hal_status = HAL_ERROR;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	if(hal_status == HAL_ERROR)
	{
		/* Update error code */
		hCAN1.ErrorCode |= HAL_CAN_LOOPBACK_ERROR;
		/* Change CAN state */
		hCAN1.State = E_HAL_CAN_STATE_ERROR;
	}
	/* Return function status */
	return hal_status;
}


/*************************************************************************************
** HAL_CAN_Stop - Resets CAN handle status and disables CAN RX interrupt.
**
** Params : None
**
** Returns: None.
** Notes:
*************************************************************************************/
void HAL_CAN_Stop(void)
{
	/*Reset CAN handle state*/
	hCAN1.State = E_HAL_CAN_STATE_RESET;

	/* Disable CAN RX interrupt*/
	_HAL_CAN_DISABLE_IT(hCAN1, CAN_IT_RX_FIFO0_MSG_PENDING);
	/* Optional HAL_NVIC_DisableIRQ passing CAN1_RX0_IRQn */
}
/*************************************************************************************
** HAL_CAN_AddTxMessage - Add a message to a mailbox and activate the corresponding transmission request.
**
** Params: uint32_t header_std_id - CAN frame id.
**         uint8_t aData[] - Data array containing the data bytes of the Tx frame.
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_CAN_AddTxMessage(uint32_t header_std_id, uint8_t aData[CAN_DLC])
{
	uint32_t transmitmailbox;
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Check input parameters */
	if((IS_CAN_STDID(header_std_id)) && (NULL != aData))
	{
		if (hCAN1.State == E_HAL_CAN_STATE_READY)
		{
			/* Check that all the Tx mailboxes are not full */
			if (((uint32_t)RESET != (hCAN1.Instance->TSR & CAN_TSR_TME0)) ||\
					((uint32_t)RESET != (hCAN1.Instance->TSR & CAN_TSR_TME1)) ||\
					((uint32_t)RESET != (hCAN1.Instance->TSR & CAN_TSR_TME2)))
			{
				/* Select an empty transmit mailbox */
				transmitmailbox = (hCAN1.Instance->TSR & CAN_TSR_CODE) >> CAN_TSR_CODE_POS;

				/* Set up the Id (STD_ID)*/
				hCAN1.Instance->sTxMailBox[transmitmailbox].TIR = ((header_std_id << CAN_TI0R_STID_POS) | CAN_RTR_DATA);

				/* Set up the DLC */
				hCAN1.Instance->sTxMailBox[transmitmailbox].TDTR = CAN_DLC;

				/* Set up the data field */
				WRITE_REG(hCAN1.Instance->sTxMailBox[transmitmailbox].TDHR,
						((uint32_t)aData[7] << CAN_TDH0R_DATA7_POS) |
						((uint32_t)aData[6] << CAN_TDH0R_DATA6_POS) |
						((uint32_t)aData[5] << CAN_TDH0R_DATA5_POS) |
						((uint32_t)aData[4] << CAN_TDH0R_DATA4_POS));
				WRITE_REG(hCAN1.Instance->sTxMailBox[transmitmailbox].TDLR,
						((uint32_t)aData[3] << CAN_TDL0R_DATA3_POS) |
						((uint32_t)aData[2] << CAN_TDL0R_DATA2_POS) |
						((uint32_t)aData[1] << CAN_TDL0R_DATA1_POS) |
						((uint32_t)aData[0] << CAN_TDL0R_DATA0_POS));

				/* Request transmission */
				SET_BIT(hCAN1.Instance->sTxMailBox[transmitmailbox].TIR, CAN_TI0R_TXRQ);
			}
			else
			{
				/* Update error code */
				hCAN1.ErrorCode |= HAL_CAN_ERROR_PARAM;
				hal_status = HAL_ERROR;
			}
		}
		else
		{
			/* Update error code */
			hCAN1.ErrorCode |= HAL_CAN_ERROR_NOT_INITIALIZED;
			hal_status = HAL_ERROR;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/* Return function status */
	return hal_status;
}

/*************************************************************************************
** HAL_CAN_GetRxMessage - Get an CAN frame from the Rx FIFO zone into the message RAM.
**
**
** Params : CAN_RxHeaderTypeDef *pHeader - Pointer to a CAN_RxHeaderTypeDef structure where the
**                                         header of the Rx frame will be stored.
**          uint8_t aData[] - Data array containing the payload of the Tx frame.
**
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_CAN_GetRxMessage(CAN_RxHeaderTypeDef *pHeader, uint8_t aData[CAN_DLC])
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Check input parameters */
	if ((hCAN1.State == E_HAL_CAN_STATE_READY) && (NULL != pHeader) && (NULL != aData))
	{
		/* Check the Rx FIFO */
		/* Check that the Rx FIFO 0 is not empty (at least one message present*/
		if ((uint32_t)RESET == (hCAN1.Instance->RF0R & CAN_RF0R_FMP0))
		{
			/* Update error code */
			hCAN1.ErrorCode |= HAL_CAN_ERROR_PARAM;
			hal_status = HAL_ERROR;
		}

		if(hal_status == HAL_OK)
		{
			/* Get the header */
			pHeader->IDE = CAN_RI0R_IDE & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RIR;
			pHeader->StdId = (CAN_RI0R_STID & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RIR) >> CAN_TI0R_STID_POS;
			pHeader->RTR = (CAN_RI0R_RTR & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RIR) >> CAN_RI0R_RTR_POS;
			pHeader->DLC = (CAN_RDT0R_DLC & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDTR) >> CAN_RDT0R_DLC_POS;

			/* Get the data */
			aData[0] = (CAN_RDL0R_DATA0 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDLR) >> CAN_RDL0R_DATA0_POS;
			aData[1] = (CAN_RDL0R_DATA1 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDLR) >> CAN_RDL0R_DATA1_POS;
			aData[2] = (CAN_RDL0R_DATA2 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDLR) >> CAN_RDL0R_DATA2_POS;
			aData[3] = (CAN_RDL0R_DATA3 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDLR) >> CAN_RDL0R_DATA3_POS;
			aData[4] = (CAN_RDH0R_DATA4 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDHR) >> CAN_RDH0R_DATA4_POS;
			aData[5] = (CAN_RDH0R_DATA5 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDHR) >> CAN_RDH0R_DATA5_POS;
			aData[6] = (CAN_RDH0R_DATA6 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDHR) >> CAN_RDH0R_DATA6_POS;
			aData[7] = (CAN_RDH0R_DATA7 & hCAN1.Instance->sFIFOMailBox[CAN_RX_FIFO0].RDHR) >> CAN_RDH0R_DATA7_POS;

			/* Release RX FIFO 0 */
			SET_BIT(hCAN1.Instance->RF0R, CAN_RF0R_RFOM0);

			if((uint32_t)CAN_DLC != pHeader->DLC)
			{
				/* Update error code */
				hCAN1.ErrorCode |= HAL_CAN_ERROR_PARAM;
				hal_status = HAL_ERROR;
			}
		}
	}
	else
	{
		/* Update error code */
		hCAN1.ErrorCode |= HAL_CAN_ERROR_NOT_INITIALIZED;
		hal_status = HAL_ERROR;
	}
	/* Return function status */
	return hal_status ;
}


/*************************************************************************************
** HAL_CAN_IRQHandler - Handles CAN interrupt request
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void HAL_CAN_IRQHandler(void)
{
  uint32_t interrupts = READ_REG(hCAN1.Instance->IER);


  /* Receive FIFO 0 message pending interrupt management *********************/
  /* Check if message is still pending */
  if ((RESET != (interrupts & CAN_IT_RX_FIFO0_MSG_PENDING)) && (RESET != (hCAN1.Instance->RF0R & CAN_RF0R_FMP0)))
  {
      /* Receive FIFO 0 message pending Callback */
      HAL_CAN_RxFifo0MsgPendingCallback();
  }

}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
