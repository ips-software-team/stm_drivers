/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : GIPSY_hal_adc.c
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 30-06-2021
**
** Description: This file contains functions for initialization and use of internal ADC channels.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 05-11-2020 -- Added ADC Registers definitions.
** Rev 1.2  : 20-11-2020 -- Static analysis update
** Rev 1.3  : 01-02-2021 -- Updated HAL_ADC_Init()-"Set the new sample time"
** Rev 1.4  : 30-06-2021 -- Updated due to verification remarks
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_adc.h"
#include "gipsy_hal_rcc.h"
/* -----Definitions ----------------------------------------------------------------*/
#define CONV_TIMEOUT_MILIS       0x00000001U /*Maximal period for ADC conversion*/



/********************  Bit definition for ADC_SR register  ********************/

#define ADC_SR_EOC_POS            (1U)
#define ADC_SR_EOC_MSK            (0x1UL << ADC_SR_EOC_POS)                     /*!< 0x00000002 */
#define ADC_SR_EOC                ADC_SR_EOC_MSK                               /*!<End of conversion */
#define ADC_SR_STRT_POS           (4U)
#define ADC_SR_STRT_MSK           (0x1UL << ADC_SR_STRT_POS)                    /*!< 0x00000010 */
#define ADC_SR_STRT               ADC_SR_STRT_MSK                              /*!<Regular channel Start flag */


/*******************  Bit definition for ADC_CR1 register  ********************/
#define ADC_CR1_SCAN_POS          (8U)
#define ADC_CR1_SCAN_MSK          (0x1UL << ADC_CR1_SCAN_POS)                   /*!< 0x00000100 */
#define ADC_CR1_SCAN              ADC_CR1_SCAN_MSK                             /*!<Scan mode */
#define ADC_CR1_DISCEN_POS        (11U)
#define ADC_CR1_DISCEN_MSK        (0x1UL << ADC_CR1_DISCEN_POS)                 /*!< 0x00000800 */
#define ADC_CR1_DISCEN            ADC_CR1_DISCEN_MSK                           /*!<Discontinuous mode on regular channels */
#define ADC_CR1_RES_POS           (24U)
#define ADC_CR1_RES_MSK           (0x3UL << ADC_CR1_RES_POS)                    /*!< 0x03000000 */
#define ADC_CR1_RES               ADC_CR1_RES_MSK                              /*!<RES[2:0] bits (Resolution) */
#define ADC_CR1_RES_0             (0x1UL << ADC_CR1_RES_POS)                    /*!< 0x01000000 */
#define ADC_CR1_RES_1             (0x2UL << ADC_CR1_RES_POS)                    /*!< 0x02000000 */


/*******************  Bit definition for ADC_CR2 register  ********************/
#define ADC_CR2_ADON_POS          (0U)
#define ADC_CR2_ADON_MSK          (0x1UL << ADC_CR2_ADON_POS)                   /*!< 0x00000001 */
#define ADC_CR2_ADON              ADC_CR2_ADON_MSK                             /*!<A/D Converter ON / OFF */
#define ADC_CR2_CONT_POS          (1U)
#define ADC_CR2_CONT_MSK          (0x1UL << ADC_CR2_CONT_POS)                   /*!< 0x00000002 */
#define ADC_CR2_CONT              ADC_CR2_CONT_MSK                             /*!<Continuous Conversion */
#define ADC_CR2_EOCS_POS          (10U)
#define ADC_CR2_EOCS_MSK          (0x1UL << ADC_CR2_EOCS_POS)                   /*!< 0x00000400 */
#define ADC_CR2_EOCS              ADC_CR2_EOCS_MSK                             /*!<End of conversion selection */
#define ADC_CR2_ALIGN_POS         (11U)
#define ADC_CR2_ALIGN_MSK         (0x1UL << ADC_CR2_ALIGN_POS)                  /*!< 0x00000800 */
#define ADC_CR2_ALIGN             ADC_CR2_ALIGN_MSK                            /*!<Data Alignment */
#define ADC_CR2_EXTSEL_POS        (24U)
#define ADC_CR2_EXTSEL_MSK        (0xFUL << ADC_CR2_EXTSEL_POS)                 /*!< 0x0F000000 */
#define ADC_CR2_EXTSEL            ADC_CR2_EXTSEL_MSK                           /*!<EXTSEL[3:0] bits (External Event Select for regular group) */
#define ADC_CR2_EXTEN_POS         (28U)
#define ADC_CR2_EXTEN_MSK         (0x3UL << ADC_CR2_EXTEN_POS)                  /*!< 0x30000000 */
#define ADC_CR2_EXTEN             ADC_CR2_EXTEN_MSK                            /*!<EXTEN[1:0] bits (External Trigger Conversion mode for regular channelsp) */
#define ADC_CR2_SWSTART_POS       (30U)
#define ADC_CR2_SWSTART_MSK       (0x1UL << ADC_CR2_SWSTART_POS)                /*!< 0x40000000 */
#define ADC_CR2_SWSTART           ADC_CR2_SWSTART_MSK                          /*!<Start Conversion of regular channels */


/*******************  Bit definition for ADC_SQR1 register  *******************/
#define ADC_SQR1_L_POS            (20U)
#define ADC_SQR1_L_MSK            (0xFUL << ADC_SQR1_L_POS)                     /*!< 0x00F00000 */
#define ADC_SQR1_L                ADC_SQR1_L_MSK                               /*!<L[3:0] bits (Regular channel sequence length) */


/*******************  Bit definition for ADC_CCR register  ********************/
#define ADC_CCR_ADCPRE_POS        (16U)
#define ADC_CCR_ADCPRE_MSK        (0x3UL << ADC_CCR_ADCPRE_POS)                 /*!< 0x00030000 */
#define ADC_CCR_ADCPRE            ADC_CCR_ADCPRE_MSK                           /*!<ADCPRE[1:0] bits (ADC prescaler) */
#define ADC_CCR_ADCPRE_0          (0x1UL << ADC_CCR_ADCPRE_POS)                 /*!< 0x00010000 */



/*ADC_ClockPrescaler  ADC Clock Prescaler*/
#define ADC_CLOCK_SYNC_PCLK_DIV4    ((uint32_t)ADC_CCR_ADCPRE_0)

/*ADC_Resolution ADC Resolution*/
#define ADC_RESOLUTION_12B  0x00000000U

/*ADC_External_trigger_edge_Regular ADC External Trigger Edge Regular*/
#define ADC_EXTERNALTRIGCONVEDGE_NONE           0x00000000U

/*ADC_External_trigger_Source_Regular ADC External Trigger Source Regular*/
#define ADC_SOFTWARE_START             ((uint32_t)ADC_CR2_EXTSEL + 1U)

/*ADC_Data_align ADC Data Align*/
#define ADC_DATAALIGN_RIGHT      0x00000000U

/*ADC_EOCSelection ADC EOC Selection*/
#define ADC_EOC_SINGLE_CONV           0x00000001U

/*ADC_flags_definition ADC Flags Definition*/

#define ADC_FLAG_EOC    ((uint32_t)ADC_SR_EOC)
#define ADC_FLAG_STRT   ((uint32_t)ADC_SR_STRT)
/* -----Macros ---------------------------------------------------------------------*/
#define ADC_COMMON_REGISTER(HANDLE)                ADC123_COMMON

/**
  * @brief  Set ADC Regular channel sequence length.
  * @param  _NbrOfConversion_ Regular channel sequence length.
  * @retval None
  */
#define ADC_SQR1(_NbrOfConversion_) ((uint32_t)((_NbrOfConversion_) - 1U) << 20U)



/**
  * @brief  Get the selected ADC's flag state.
  * @param  HANDLE - specifies the ADC Handle.
  * @param  FLAG - ADC flag.
  * @retval TRUE (if FLAG = 1), FALSE(if FLAG = 0)
  */
#define __HAL_ADC_GET_FLAG(HANDLE, FLAG) ((((HANDLE).Instance->SR) & (FLAG)) == (FLAG))



/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
/*ADC handle Structure definition*/
static struct ADC_HandleTypeDef
{
  ADC_TypeDef                   *Instance;                   /*!< Register base address */
  ADC_InitTypeDef               Init;                        /*!< ADC required parameters */
}hADC1 =
{
		/*Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)*/
		.Instance = ADC1,
		.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4,
		.Init.Resolution = ADC_RESOLUTION_12B,
		.Init.ScanConvMode = (uint32_t)DISABLE,
		.Init.ContinuousConvMode = (uint32_t)DISABLE,
		.Init.DiscontinuousConvMode = (uint32_t)DISABLE,
		.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE,
		.Init.ExternalTrigConv = ADC_SOFTWARE_START,
		.Init.DataAlign = ADC_DATAALIGN_RIGHT,
		.Init.NbrOfConversion = 1U,
		.Init.EOCSelection = ADC_EOC_SINGLE_CONV
};


/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/************************************************************************************
** HAL_ADC_Init - Initializes the ADCx peripheral according to the specified parameters
**            in the ADC_InitStruct.
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void HAL_ADC_Init(void)
{
	ADC_Common_TypeDef *tmp_adc_common = NULL;

	/* Set ADC parameters */
	/* Pointer to the common control register to which is belonging hadc    */
	/* (Depending on STM32F4 product, there may be up to 3 ADCs and 1 common */
	/* control register)                                                    */
	tmp_adc_common = ADC_COMMON_REGISTER(hADC1);

	/* Set the ADC clock prescaler */
	tmp_adc_common->CCR &= ~(ADC_CCR_ADCPRE);
	tmp_adc_common->CCR |=  (uint32_t)hADC1.Init.ClockPrescaler;

	/* Set ADC scan mode */
	hADC1.Instance->CR1 &= ~(ADC_CR1_SCAN);
	hADC1.Instance->CR1 |=  (uint32_t)(hADC1.Init.ScanConvMode << 8U);

	/* Set ADC resolution */
	hADC1.Instance->CR1 &= ~(ADC_CR1_RES);
	hADC1.Instance->CR1 |=  (uint32_t)hADC1.Init.Resolution;

	/* Set ADC data alignment */
	hADC1.Instance->CR2 &= ~(ADC_CR2_ALIGN);
	hADC1.Instance->CR2 |= (uint32_t)hADC1.Init.DataAlign;

	/* Reset the external trigger */
	hADC1.Instance->CR2 &= ~(ADC_CR2_EXTSEL);
	hADC1.Instance->CR2 &= ~(ADC_CR2_EXTEN);

	/* Enable or disable ADC continuous conversion mode */
	hADC1.Instance->CR2 &= ~(ADC_CR2_CONT);
	hADC1.Instance->CR2 |= (uint32_t)(hADC1.Init.ContinuousConvMode << 1U);

	/* Disable the selected ADC regular discontinuous mode */
	hADC1.Instance->CR1 &= ~(ADC_CR1_DISCEN);

	/* Set ADC number of conversion */
	hADC1.Instance->SQR1 &= ~(ADC_SQR1_L);
	hADC1.Instance->SQR1 |=  (uint32_t)ADC_SQR1(hADC1.Init.NbrOfConversion);

	/* Enable or disable ADC end of conversion selection */
	hADC1.Instance->CR2 &= ~(ADC_CR2_EOCS);
	hADC1.Instance->CR2 |= (hADC1.Init.EOCSelection << 10U);

	/* Set the new sample time */
	hADC1.Instance->SMPR2 |= 0x49249249;/*set CH 0-9 with sample time of 15 cycles(001)*/

	/* Enable the ADC peripheral and wait during Tstab time the ADC's stabilization */
	/* Enable the SDC Peripheral */
	hADC1.Instance->CR2 |=  (uint32_t)ADC_CR2_ADON;
}

/************************************************************************************
** HAL_ADC_Perform_Conversion - Performs conversion of single regular ADC channel.
**
**
** Params : uint32_t channel - Number of converted channel
** 			uint32_t *converted_value - Pointer to conversion result
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_ADC_Perform_Conversion(uint32_t channel, uint32_t *converted_value)
{
	uint32_t tmp_val = 0U;
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if(((channel == ADC_CHANNEL_2) || (channel == ADC_CHANNEL_3) || (channel == ADC_CHANNEL_4) ||\
			(channel == ADC_CHANNEL_5) ||(channel == ADC_CHANNEL_6) ||(channel == ADC_CHANNEL_8))\
			&& (NULL != converted_value))
	{
		/* Clear sequence channels*/
		hADC1.Instance->SQR3 = 0x00000000U;
		/* Set the channel number for rank 1 only*/
		hADC1.Instance->SQR3 |= (uint32_t)channel;

		/* Clear regular group conversion flag  */
		/* (To ensure of no unknown state from potential previous ADC operations) */
		hADC1.Instance->SR = ~(ADC_FLAG_EOC);

		/* Enable the selected ADC software conversion for regular group */
		hADC1.Instance->CR2 |= (uint32_t)ADC_CR2_SWSTART;

		/*-----Poll for regular conversion complete--------*/
		tickstart = HAL_GetTick();/* Get tick */

		/* Check End of conversion flag */
		while(!__HAL_ADC_GET_FLAG(hADC1, ADC_FLAG_EOC))
		{
			if((HAL_GetTick() - tickstart ) > CONV_TIMEOUT_MILIS)
			{
				/* Update ADC status to timeout */
				hal_status = HAL_TIMEOUT;
				break;
			}
		}
		if(HAL_OK == hal_status)
		{
			/* Clear regular group conversion flag */
			hADC1.Instance->SR = ~(ADC_FLAG_STRT | ADC_FLAG_EOC);
			tmp_val = hADC1.Instance->DR;
			*converted_value = tmp_val;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/* Return function status */
	return hal_status;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
