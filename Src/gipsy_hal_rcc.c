/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_rcc.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 25-07-2021
**
** Description: Clocks configuration HAL module driver
**
*/


/* Revision Log:
**
** Rev 1.0  : 29-08-2020 -- Original version created.
** Rev 1.1  : 20-11-2020 -- Static analysis update
** Rev 1.2  : 13-12-2020 -- Added procedure: HAL_RCC_GetPCLK2Freq() for DIMC
** Rev 1.3  : 25-07-2021 -- Added procedure: HAL_RCC_GetPCLK2Freq() for DEBUG
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include <gipsy_hal_cortex.h>
#include "gipsy_hal_rcc.h"


/* -----Definitions ----------------------------------------------------------------*/
/********************  Bit definition for RCC_CR register  ********************/
#define RCC_CR_HSEON_POS                   (16U)
#define RCC_CR_HSEON_MSK                   (0x1UL << RCC_CR_HSEON_POS)          /*!< 0x00010000 */
#define RCC_CR_HSEON                       RCC_CR_HSEON_MSK


/********************  Bit definition for RCC_PLLCFGR register  ***************/
#define RCC_PLLCFGR_PLLM_POS               (0U)
#define RCC_PLLCFGR_PLLM_MSK               (0x3FU << RCC_PLLCFGR_PLLM_POS)     /*!< 0x0000003F */
#define RCC_PLLCFGR_PLLM                   RCC_PLLCFGR_PLLM_MSK
#define RCC_PLLCFGR_PLLN_POS               (6U)
#define RCC_PLLCFGR_PLLN_MSK               (0x1FFU << RCC_PLLCFGR_PLLN_POS)    /*!< 0x00007FC0 */
#define RCC_PLLCFGR_PLLN                   RCC_PLLCFGR_PLLN_MSK
#define RCC_PLLCFGR_PLLP_POS               (16U)
#define RCC_PLLCFGR_PLLP_MSK               (0x3U << RCC_PLLCFGR_PLLP_POS)      /*!< 0x00030000 */
#define RCC_PLLCFGR_PLLP                   RCC_PLLCFGR_PLLP_MSK
#define RCC_PLLCFGR_PLLSRC_HSE_POS         (22U)
#define RCC_PLLCFGR_PLLSRC_HSE_MSK         (0x1U << RCC_PLLCFGR_PLLSRC_HSE_POS) /*!< 0x00400000 */
#define RCC_PLLCFGR_PLLSRC_HSE             RCC_PLLCFGR_PLLSRC_HSE_MSK
#define RCC_PLLCFGR_PLLQ_POS               (24U)
#define RCC_PLLCFGR_PLLQ_MSK               (0xFU << RCC_PLLCFGR_PLLQ_POS)      /*!< 0x0F000000 */
#define RCC_PLLCFGR_PLLQ                   RCC_PLLCFGR_PLLQ_MSK



/********************  Bit definition for RCC_CFGR register  ******************/
/*!< SW configuration */
#define RCC_CFGR_SW_POS                    (0U)
#define RCC_CFGR_SW_MSK                    (0x3U << RCC_CFGR_SW_POS)           /*!< 0x00000003 */
#define RCC_CFGR_SW                        RCC_CFGR_SW_MSK                     /*!< SW[1:0] bits (System clock Switch) */
#define RCC_CFGR_SW_0                      (0x1U << RCC_CFGR_SW_POS)           /*!< 0x00000001 */
#define RCC_CFGR_SW_1                      (0x2U << RCC_CFGR_SW_POS)           /*!< 0x00000002 */

#define RCC_CFGR_SW_HSI                    0x00000000U                         /*!< HSI selected as system clock */
#define RCC_CFGR_SW_HSE                    0x00000001U                         /*!< HSE selected as system clock */
#define RCC_CFGR_SW_PLL                    0x00000002U                         /*!< PLL selected as system clock */

/*!< SWS configuration */
#define RCC_CFGR_SWS_POS                   (2U)
#define RCC_CFGR_SWS_MSK                   (0x3UL << RCC_CFGR_SWS_POS)          /*!< 0x0000000C */
#define RCC_CFGR_SWS                       RCC_CFGR_SWS_MSK                    /*!< SWS[1:0] bits (System Clock Switch Status) */
#define RCC_CFGR_SWS_0                     (0x1UL << RCC_CFGR_SWS_POS)          /*!< 0x00000004 */
#define RCC_CFGR_SWS_1                     (0x2UL << RCC_CFGR_SWS_POS)          /*!< 0x00000008 */

#define RCC_CFGR_SWS_HSI                   0x00000000U                         /*!< HSI oscillator used as system clock        */
#define RCC_CFGR_SWS_HSE                   0x00000004U                         /*!< HSE oscillator used as system clock        */
#define RCC_CFGR_SWS_PLL                   0x00000008U                         /*!< PLL used as system clock                   */

/*!< HPRE configuration */
#define RCC_CFGR_HPRE_POS                  (4U)
#define RCC_CFGR_HPRE_MSK                  (0xFUL << RCC_CFGR_HPRE_POS)         /*!< 0x000000F0 */
#define RCC_CFGR_HPRE                      RCC_CFGR_HPRE_MSK                   /*!< HPRE[3:0] bits (AHB prescaler) */
#define RCC_CFGR_HPRE_0                    (0x1UL << RCC_CFGR_HPRE_POS)         /*!< 0x00000010 */
#define RCC_CFGR_HPRE_1                    (0x2UL << RCC_CFGR_HPRE_POS)         /*!< 0x00000020 */
#define RCC_CFGR_HPRE_2                    (0x4UL << RCC_CFGR_HPRE_POS)         /*!< 0x00000040 */
#define RCC_CFGR_HPRE_3                    (0x8UL << RCC_CFGR_HPRE_POS)         /*!< 0x00000080 */

#define RCC_CFGR_HPRE_DIV1                 0x00000000U                         /*!< SYSCLK not divided    */
#define RCC_CFGR_HPRE_DIV2                 0x00000080U                         /*!< SYSCLK divided by 2   */
#define RCC_CFGR_HPRE_DIV4                 0x00000090U                         /*!< SYSCLK divided by 4   */
#define RCC_CFGR_HPRE_DIV8                 0x000000A0U                         /*!< SYSCLK divided by 8   */
#define RCC_CFGR_HPRE_DIV16                0x000000B0U                         /*!< SYSCLK divided by 16  */
#define RCC_CFGR_HPRE_DIV64                0x000000C0U                         /*!< SYSCLK divided by 64  */
#define RCC_CFGR_HPRE_DIV128               0x000000D0U                         /*!< SYSCLK divided by 128 */
#define RCC_CFGR_HPRE_DIV256               0x000000E0U                         /*!< SYSCLK divided by 256 */
#define RCC_CFGR_HPRE_DIV512               0x000000F0U                         /*!< SYSCLK divided by 512 */

/*!< PPRE1 configuration */
#define RCC_CFGR_PPRE1_POS                 (10U)
#define RCC_CFGR_PPRE1_MSK                 (0x7UL << RCC_CFGR_PPRE1_POS)        /*!< 0x00001C00 */
#define RCC_CFGR_PPRE1                     RCC_CFGR_PPRE1_MSK                  /*!< PRE1[2:0] bits (APB1 prescaler) */
#define RCC_CFGR_PPRE1_0                   (0x1UL << RCC_CFGR_PPRE1_POS)        /*!< 0x00000400 */
#define RCC_CFGR_PPRE1_1                   (0x2UL << RCC_CFGR_PPRE1_POS)        /*!< 0x00000800 */
#define RCC_CFGR_PPRE1_2                   (0x4UL << RCC_CFGR_PPRE1_POS)        /*!< 0x00001000 */

#define RCC_CFGR_PPRE1_DIV1                0x00000000U                         /*!< HCLK not divided   */
#define RCC_CFGR_PPRE1_DIV2                0x00001000U                         /*!< HCLK divided by 2  */
#define RCC_CFGR_PPRE1_DIV4                0x00001400U                         /*!< HCLK divided by 4  */
#define RCC_CFGR_PPRE1_DIV8                0x00001800U                         /*!< HCLK divided by 8  */
#define RCC_CFGR_PPRE1_DIV16               0x00001C00U                         /*!< HCLK divided by 16 */

/*!< PPRE2 configuration */
#define RCC_CFGR_PPRE2_POS                 (13U)
#define RCC_CFGR_PPRE2_MSK                 (0x7UL << RCC_CFGR_PPRE2_POS)        /*!< 0x0000E000 */
#define RCC_CFGR_PPRE2                     RCC_CFGR_PPRE2_MSK                  /*!< PRE2[2:0] bits (APB2 prescaler) */

/*!< RTCPRE configuration */
#define RCC_CFGR_RTCPRE_POS                (16U)
#define RCC_CFGR_RTCPRE_MSK                (0x1FUL << RCC_CFGR_RTCPRE_POS)      /*!< 0x001F0000 */
#define RCC_CFGR_RTCPRE                    RCC_CFGR_RTCPRE_MSK


/********************  Bit definition for RCC_BDCR register  ******************/
#define RCC_BDCR_LSEON_POS                 (0U)
#define RCC_BDCR_LSEON_MSK                 (0x1UL << RCC_BDCR_LSEON_POS)        /*!< 0x00000001 */
#define RCC_BDCR_LSEON                     RCC_BDCR_LSEON_MSK
#define RCC_BDCR_RTCSEL_POS                (8U)
#define RCC_BDCR_RTCSEL_MSK                (0x3UL << RCC_BDCR_RTCSEL_POS)       /*!< 0x00000300 */
#define RCC_BDCR_RTCSEL                    RCC_BDCR_RTCSEL_MSK
/**
  *        Adjust the value of External High Speed oscillator (HSE) used in your application.
  *        This value is used by the RCC HAL module to compute the system frequency
  *        (when HSE is used as system clock source, directly or through the PLL).
  */
#define HSE_VALUE    ((uint32_t)25000000U) /*!< Value of the External oscillator in Hz */
#define LSE_VALUE  ((uint32_t)32768U)    /*!< Value of the External Low Speed oscillator in Hz */
#define RCC_PLLSOURCE_HSE                RCC_PLLCFGR_PLLSRC_HSE

#define PLL_TIMEOUT_VALUE_MILIS          2U  /* 2 ms */
#define RCC_DBP_TIMEOUT_VALUE_MILIS      2U

/*RCC_System_Clock_Source System Clock Source*/
#define RCC_SYSCLKSOURCE_HSE             RCC_CFGR_SW_HSE
#define RCC_SYSCLKSOURCE_PLLCLK          RCC_CFGR_SW_PLL

#define HSE_STARTUP_TIMEOUT_MILIS    ((uint32_t)100U)   /*!< Time out for HSE start up, in ms */
#define LSE_STARTUP_TIMEOUT_MILIS    ((uint32_t)100U)   /*!< Time out for LSE start up, in ms */

/*RCC_AHB_Clock_Source AHB Clock Source*/
#define RCC_SYSCLK_DIV1                  RCC_CFGR_HPRE_DIV1
#define RCC_SYSCLK_DIV2                  RCC_CFGR_HPRE_DIV2
#define RCC_SYSCLK_DIV4                  RCC_CFGR_HPRE_DIV4
#define RCC_SYSCLK_DIV8                  RCC_CFGR_HPRE_DIV8
#define RCC_SYSCLK_DIV16                 RCC_CFGR_HPRE_DIV16
#define RCC_SYSCLK_DIV64                 RCC_CFGR_HPRE_DIV64
#define RCC_SYSCLK_DIV128                RCC_CFGR_HPRE_DIV128
#define RCC_SYSCLK_DIV256                RCC_CFGR_HPRE_DIV256
#define RCC_SYSCLK_DIV512                RCC_CFGR_HPRE_DIV512

/*RCC_PLLP_Clock_Divider PLLP Clock Divider*/
#define RCC_PLLP_DIV2                  0x00000002U
#define RCC_PLLP_DIV4                  0x00000004U
#define RCC_PLLP_DIV6                  0x00000006U
#define RCC_PLLP_DIV8                  0x00000008U

/*RCC_APB1_APB2_Clock_Source APB1/APB2 Clock Source*/
#define RCC_HCLK_DIV1                    RCC_CFGR_PPRE1_DIV1
#define RCC_HCLK_DIV2                    RCC_CFGR_PPRE1_DIV2
#define RCC_HCLK_DIV4                    RCC_CFGR_PPRE1_DIV4
#define RCC_HCLK_DIV8                    RCC_CFGR_PPRE1_DIV8
#define RCC_HCLK_DIV16                   RCC_CFGR_PPRE1_DIV16

/*RCC_RTC_Clock_Source RTC Clock Source*/
#define RCC_RTCCLKSOURCE_LSE             0x00000100U

/* Flags in the CR register */
#define RCC_FLAG_HSERDY                  0x00020000U
#define RCC_FLAG_PLLRDY                  0x02000000U


/* Flags in the BDCR register */
#define RCC_FLAG_LSERDY                  0x00000002U

/*RCC BitAddress AliasRegion*/
#define RCC_OFFSET                 (RCC_BASE - PERIPH_BASE)
#define RCC_CR_OFFSET              (RCC_OFFSET + 0x00U)

/* Alias word address of PLLON bit */
#define RCC_PLLON_BIT_NUMBER       0x18U
#define RCC_CR_PLLON_BB            (PERIPH_BB_BASE + (RCC_CR_OFFSET * 32U) + (RCC_PLLON_BIT_NUMBER * 4U))
/* --- BDCR Register --- */
/*RCC BitAddress AliasRegion*/
#define RCC_OFFSET                 (RCC_BASE - PERIPH_BASE)

/* Alias word address of RTCEN bit */
#define RCC_BDCR_OFFSET            (RCC_OFFSET + 0x70U)

/* Alias word address of BDRST bit */
#define RCC_BDRST_BIT_NUMBER       0x10U
#define RCC_BDCR_BDRST_BB          (PERIPH_BB_BASE + (RCC_BDCR_OFFSET * 32U) + (RCC_BDRST_BIT_NUMBER * 4U))

#define RCC_LSE_TIMEOUT_VALUE_MILIS       LSE_STARTUP_TIMEOUT_MILIS
#define HSE_TIMEOUT_VALUE_MILIS           HSE_STARTUP_TIMEOUT_MILIS
#define CLOCKSWITCH_TIMEOUT_VALUE_MILIS  500U /* 5 s */

#define  TICK_INT_PRIORITY            ((uint32_t)0U)   /*!< tick interrupt priority */

#define RCC_PLLON_ENABLE_BIT_MASK   0x01000000U
#define RCC_PLLON_DISABLE_BIT_MASK  0xFEFFFFFFU

#define RCC_BDRST_FORCE_RESET_BIT_MASK   0x00010000U
#define RCC_BDRST_RELEASE_REET_BIT_MASK  0xFFFEFFFFU

/*FLASH_Latency FLASH Latency*/
#define FLASH_LATENCY_0           FLASH_ACR_LATENCY_0WS                /*!< FLASH Zero wait state */
#define FLASH_LATENCY_1           FLASH_ACR_LATENCY_1WS                /*!< FLASH One wait state */
#define FLASH_LATENCY_2           FLASH_ACR_LATENCY_2WS                /*!< FLASH Two wait states */
#define FLASH_LATENCY_3           FLASH_ACR_LATENCY_3WS                /*!< FLASH Three wait states */
#define FLASH_LATENCY_4           FLASH_ACR_LATENCY_4WS                /*!< FLASH Four wait states */

#define FLASH_LATENCY_5           FLASH_ACR_LATENCY_5WS                /*!< FLASH Five wait state */
#define FLASH_LATENCY_6           FLASH_ACR_LATENCY_6WS                /*!< FLASH Six wait state */
#define FLASH_LATENCY_7           FLASH_ACR_LATENCY_7WS                /*!< FLASH Seven wait states */
#define FLASH_LATENCY_8           FLASH_ACR_LATENCY_8WS                /*!< FLASH Eight wait states */
#define FLASH_LATENCY_9           FLASH_ACR_LATENCY_9WS                /*!< FLASH Nine wait states */
#define FLASH_LATENCY_10          FLASH_ACR_LATENCY_10WS               /*!< FLASH Ten wait state */
#define FLASH_LATENCY_11          FLASH_ACR_LATENCY_11WS               /*!< FLASH Eleven wait state */
#define FLASH_LATENCY_12          FLASH_ACR_LATENCY_12WS               /*!< FLASH Twelve wait states */
#define FLASH_LATENCY_13          FLASH_ACR_LATENCY_13WS               /*!< FLASH Thirteen wait states */
#define FLASH_LATENCY_14          FLASH_ACR_LATENCY_14WS               /*!< FLASH Fourteen wait states */
#define FLASH_LATENCY_15          FLASH_ACR_LATENCY_15WS               /*!< FLASH Fifteen wait states */

/*ACR register byte 0 (Bits[7:0]) base address*/
#define ACR_BYTE0_ADDRESS           0x40023C00U

#define RCC_FLAG_MASK  ((uint8_t)0x1FU)

#define PWR_CR_DBP_POS         (8U)
#define PWR_CR_DBP_MSK         (0x1UL << PWR_CR_DBP_POS)                        /*!< 0x00000100 */
#define PWR_CR_DBP             PWR_CR_DBP_MSK                                  /*!< Disable Backup Domain write protection                     */
/* -----Macros ---------------------------------------------------------------------*/




/*Get the FLASH Latency - The value of this parameter depend on device used within the same series*/
#define HAL_FLASH_GET_LATENCY()     (READ_BIT((FLASH->ACR), FLASH_ACR_LATENCY))



/*Macro to get the clock source used as system clock*/
#define HAL_RCC_GET_SYSCLK_SOURCE() (RCC->CFGR & RCC_CFGR_SWS)

/*Macro to configure the system clock source*/
#define HAL_RCC_SYSCLK_CONFIG(_RCC_SYSCLKSOURCE_) MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, (_RCC_SYSCLKSOURCE_))



/* ------------ RCC registers bit address in the alias region ----------- */
#define SYSCFG_OFFSET             (SYSCFG_BASE - PERIPH_BASE)
/* ---  MEMRMP Register ---*/
/* Alias word address of UFB_MODE bit */
#define MEMRMP_OFFSET             SYSCFG_OFFSET
#define UFB_MODE_BIT_NUMBER       SYSCFG_MEMRMP_UFB_MODE_POS
#define UFB_MODE_BB               (uint32_t)(PERIPH_BB_BASE + (MEMRMP_OFFSET * 32U) + (UFB_MODE_BIT_NUMBER * 4U))

/* ---  CMPCR Register ---*/
/* Alias word address of CMP_PD bit */
#define CMPCR_OFFSET              (SYSCFG_OFFSET + 0x20U)
#define CMP_PD_BIT_NUMBER         SYSCFG_CMPCR_CMP_PD_POS
#define CMPCR_CMP_PD_BB           (uint32_t)(PERIPH_BB_BASE + (CMPCR_OFFSET * 32U) + (CMP_PD_BIT_NUMBER * 4U))

/* ---  MCHDLYCR Register ---*/
/* Alias word address of BSCKSEL bit */
#define MCHDLYCR_OFFSET            (SYSCFG_OFFSET + 0x30U)
#define BSCKSEL_BIT_NUMBER         SYSCFG_MCHDLYCR_BSCKSEL_POS
#define MCHDLYCR_BSCKSEL_BB        (uint32_t)(PERIPH_BB_BASE + (MCHDLYCR_OFFSET * 32U) + (BSCKSEL_BIT_NUMBER * 4U))
/* -----Global variables -----------------------------------------------------------*/
volatile uint32_t Tick_Value = 0U;

uint32_t Tick_Freq = 1U;  /* 1KHz */

const uint8_t AHBPrescTable[16U] = {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 1U, 2U, 3U, 4U, 6U, 7U, 8U, 9U};
const uint8_t APBPrescTable[8U]  = {0U, 0U, 0U, 0U, 1U, 2U, 3U, 4U};

uint32_t SystemCoreClock = 16000000U;
/* -----External variables ---------------------------------------------------------*/
/* -----Static Function prototypes -------------------------------------------------*/

static HAL_StatusTypeDef hal_init_tick(void);

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** HAL_RCC_OscConfig - Initializes the RCC Oscillators.
**
** Params : None.
**                                   .
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_RCC_OscConfig(void)
{
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef  hal_status = HAL_OK;
	const uint32_t pll_source = RCC_PLLSOURCE_HSE;
	const uint32_t pllm = 25U;
	const uint32_t plln = 336U;
	const uint32_t pllp = RCC_PLLP_DIV2;
	const uint32_t pllq = 4U;

	/*------------------------------- HSE Configuration ------------------------*/
	/*Set HSE - ON*/
	SET_BIT(RCC->CR, RCC_CR_HSEON);
	/* Check the HSE State */
	/* Get Start Tick */
	tickstart = HAL_GetTick();
	/* Wait till HSE is ready */
	while(((uint32_t)RESET == (uint32_t)(RCC->CR & RCC_FLAG_HSERDY)) && (HAL_OK == hal_status))
	{
		if((HAL_GetTick() - tickstart ) > HSE_TIMEOUT_VALUE_MILIS )
		{
			hal_status = HAL_TIMEOUT;
		}
	}

	/*------------------------------ LSE Configuration -------------------------*/
	if(HAL_OK == hal_status)
	{
		if(HAL_IS_BIT_CLR(PWR->CR, PWR_CR_DBP))/* Check if access to Backup domain is enabled*/
		{
			/* Enable write access to Backup domain */
			SET_BIT(PWR->CR, PWR_CR_DBP);

			/* Wait for Backup domain Write protection disable */
			tickstart = HAL_GetTick();

			while((HAL_IS_BIT_CLR(PWR->CR, PWR_CR_DBP)) && (HAL_OK == hal_status))
			{
				if((HAL_GetTick() - tickstart) > RCC_DBP_TIMEOUT_VALUE_MILIS)
				{
					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}

	/* Set the LSE configuration -----------------------------------------*/
	if(HAL_OK == hal_status)
	{
		/*Set LSE - ON*/
		SET_BIT(RCC->BDCR, RCC_BDCR_LSEON);
		/* Check the LSE State */
		/* Get Start Tick*/
		tickstart = HAL_GetTick();

		/* Wait till LSE is ready */
		while(((uint32_t)RESET == (uint32_t)(RCC->BDCR & RCC_FLAG_LSERDY)) && (HAL_OK == hal_status))
		{
			if((HAL_GetTick() - tickstart ) > RCC_LSE_TIMEOUT_VALUE_MILIS )
			{
				hal_status = HAL_TIMEOUT;
			}
		}
	}

	/*-------------------------------- PLL Configuration -----------------------*/
	if(HAL_OK == hal_status)
	{
		/* Disable the main PLL. */
		RCC->CR &= (uint32_t)RCC_PLLON_DISABLE_BIT_MASK;

		/* Get Start Tick */
		tickstart = HAL_GetTick();

		/* Wait till PLL is disabled */
		while(((uint32_t)RESET != (uint32_t)(RCC->CR & RCC_FLAG_PLLRDY)) && (HAL_OK == hal_status))
		{
			if((HAL_GetTick() - tickstart ) > PLL_TIMEOUT_VALUE_MILIS)
			{
				hal_status = HAL_TIMEOUT;
			}
		}
		if(HAL_OK == hal_status)
		{
			/* Configure the main PLL clock source, multiplication and division factors. */
			WRITE_REG(RCC->PLLCFGR, (pll_source |  pllm  | (plln << RCC_PLLCFGR_PLLN_POS)| \
					(((pllp >> 1U) - 1U) << RCC_PLLCFGR_PLLP_POS) | (pllq << RCC_PLLCFGR_PLLQ_POS)));

			/* Enable the main PLL. */
			RCC->CR |= (uint32_t)RCC_PLLON_ENABLE_BIT_MASK;

			/* Get Start Tick */
			tickstart = HAL_GetTick();

			/* Wait till PLL is ready */
			while(((uint32_t)RESET == (uint32_t)(RCC->CR & RCC_FLAG_PLLRDY)) && (HAL_OK == hal_status))
			{
				if((HAL_GetTick() - tickstart ) > PLL_TIMEOUT_VALUE_MILIS)
				{
					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	return hal_status;
}

/*************************************************************************************
** HAL_RCC_ClockConfig - Initializes the CPU, AHB and APB busses clocks.
**
** Params : None
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_RCC_ClockConfig(void)
{
	const uint32_t AHB_CLK_DIVIDER = RCC_SYSCLK_DIV1;
	const uint32_t APB1_CLK_DIVIDER  = RCC_HCLK_DIV4;
	const uint32_t APB2_CLK_DIVIDER = RCC_HCLK_DIV2;
	uint32_t tickstart = 0U;
	uint32_t pllm = 0U;
	uint32_t pllvco = 0U;
	uint32_t pllp = 0U;
	uint32_t sys_clock_freq = 0U;
	HAL_StatusTypeDef hal_status  = HAL_OK;
	/*FLASH Latency, this parameter depend on device selected*/
	const uint32_t flash_latency = FLASH_LATENCY_5;/*was input parameter*/



	/* To correctly read data from FLASH memory, the number of wait states (LATENCY)
		must be correctly programmed according to the frequency of the CPU clock
		(HCLK) and the supply voltage of the device. */

	/* Increasing the number of wait states because of higher CPU frequency */
	if(flash_latency > HAL_FLASH_GET_LATENCY())
	{
		/* Program the new number of wait states to the LATENCY bits in the FLASH_ACR register */
		*(__IO uint8_t *)ACR_BYTE0_ADDRESS = (uint8_t)(flash_latency);

		/* Check that the new number of wait states is taken into account to access the Flash
		memory by reading the FLASH_ACR register */
		if(flash_latency != HAL_FLASH_GET_LATENCY())
		{
			hal_status = HAL_ERROR;
		}
	}


	if(hal_status == HAL_OK)
	{
		/*-------------------------- HCLK Configuration --------------------------*/

		/* Set the highest APBx dividers in order to ensure that we do not go through
		   a non-spec phase whatever we decrease or increase HCLK. */

		/*Set PCLK1 Clock Divider value*/
		MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE1, RCC_HCLK_DIV16);

		/*Set PCLK2 Clock Divider value*/
		MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE2, (RCC_HCLK_DIV16 << 3U));

		/*Set AHB Clock Divider value*/
		MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, AHB_CLK_DIVIDER);
	}
	if(hal_status == HAL_OK)
	{
		/*------------------------- SYSCLK Configuration ---------------------------*/

		/* PLL is selected as System Clock Source */
		/* Check the PLL ready flag */
		if(((uint32_t)RESET == (uint32_t)(RCC->CR & RCC_FLAG_PLLRDY)))
		{
			hal_status = HAL_ERROR;
		}

		if(hal_status == HAL_OK)
		{
			HAL_RCC_SYSCLK_CONFIG(RCC_SYSCLKSOURCE_PLLCLK);

			/* Get Start Tick */
			tickstart = HAL_GetTick();

			while ((HAL_OK == hal_status) && (HAL_RCC_GET_SYSCLK_SOURCE() != (RCC_SYSCLKSOURCE_PLLCLK << RCC_CFGR_SWS_POS)))
			{
				if ((HAL_GetTick() - tickstart) > CLOCKSWITCH_TIMEOUT_VALUE_MILIS )
				{
					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}

	if(hal_status == HAL_OK)
	{
		/* Decreasing the number of wait states because of lower CPU frequency */
		if(flash_latency < HAL_FLASH_GET_LATENCY())
		{
			/* Program the new number of wait states to the LATENCY bits in the FLASH_ACR register */
			*(__IO uint8_t *)ACR_BYTE0_ADDRESS = (uint8_t)(flash_latency);


			/* Check that the new number of wait states is taken into account to access the Flash
               memory by reading the FLASH_ACR register */
			if(flash_latency != HAL_FLASH_GET_LATENCY())
			{
				hal_status = HAL_ERROR;
			}
		}
	}

	if(hal_status == HAL_OK)
	{
		/*-------------------------- PCLK1 Configuration ---------------------------*/
		MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE1, APB1_CLK_DIVIDER);

		/*-------------------------- PCLK2 Configuration ---------------------------*/
		MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE2, ((APB2_CLK_DIVIDER) << 3U));

		/*Check the SYSCLK frequency*/
		/* PLL_VCO = (HSE_VALUE or HSI_VALUE / PLLM) * PLLN
	      SYSCLK = PLL_VCO / PLLP */
		pllm = RCC->PLLCFGR & RCC_PLLCFGR_PLLM;

		/* HSE used as PLL clock source */
		pllvco = (uint32_t)((((uint64_t)HSE_VALUE * ((uint64_t)((uint32_t)(RCC->PLLCFGR & (uint32_t)RCC_PLLCFGR_PLLN) >> RCC_PLLCFGR_PLLN_POS)))) / (uint64_t)pllm);
		pllp = ((((uint32_t)(RCC->PLLCFGR & RCC_PLLCFGR_PLLP) >> RCC_PLLCFGR_PLLP_POS) + 1U) *2U);
		sys_clock_freq = pllvco/pllp;

		/* Update the SystemCoreClock global variable */
		SystemCoreClock = sys_clock_freq >> AHBPrescTable[(uint32_t)(RCC->CFGR & RCC_CFGR_HPRE) >> RCC_CFGR_HPRE_POS];

		/* Configure the source of time base considering new system clocks settings */
		hal_status |= hal_init_tick ();
	}
	return hal_status;
}



/*************************************************************************************
** HAL_RCC_GetPCLK1Freq - Returns the PCLK1 frequency
**
** Params : None
**
** Returns: uint32_t - PCLK1 frequency
**
** Notes:Each time PCLK1 changes, this function must be called to update the
** right PCLK1 value. Otherwise, any configuration based on this function will be incorrect.
*************************************************************************************/
uint32_t HAL_RCC_GetPCLK1Freq(void)
{
  /* Get HCLK source and Compute PCLK1 frequency ---------------------------*/
  return ((uint32_t)SystemCoreClock >> APBPrescTable[(uint32_t)(RCC->CFGR & RCC_CFGR_PPRE1) >> RCC_CFGR_PPRE1_POS]);
}
#if defined(_DIMC_) || defined(_DEBUG_)
/*************************************************************************************
** HAL_RCC_GetPCLK2Freq - Returns the PCLK2 frequency
**
** Params : None
**
** Returns: uint32_t - PCLK2 frequency
**
** Notes:
*************************************************************************************/
uint32_t HAL_RCC_GetPCLK2Freq(void)
{
  /* Get HCLK source and Compute PCLK2 frequency ---------------------------*/
  return (((uint32_t)SystemCoreClock)>> APBPrescTable[(uint32_t)(RCC->CFGR & RCC_CFGR_PPRE2)>> RCC_CFGR_PPRE2_POS]);
}
#endif
/*************************************************************************************
** HAL_RCCEx_PeriphCLKConfig - Initializes the RCC extended peripherals clocks(RTC clock)
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_RCCEx_PeriphCLKConfig(void)
{
	uint32_t tickstart = 0U;
	uint32_t tmpreg = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;


	/*------------------ RTC configuration with LSE as clock source --------------------*/

	/* Enable write access to Backup domain */
	PWR->CR |= (uint32_t)PWR_CR_DBP;

	/* Get tick */
	tickstart = HAL_GetTick();

	while(RESET == (PWR->CR & PWR_CR_DBP))
	{
		if((HAL_GetTick() - tickstart ) > RCC_DBP_TIMEOUT_VALUE_MILIS)
		{
			hal_status = HAL_TIMEOUT;
		}
	}
	/* Reset the Backup domain only if the RTC Clock source selection is modified from reset value */
	tmpreg = (uint32_t)(RCC->BDCR & (uint32_t)RCC_BDCR_RTCSEL);
	if((0x00000000U != tmpreg) && ((tmpreg) != (RCC_RTCCLKSOURCE_LSE & RCC_BDCR_RTCSEL)))
	{
		/* Store the content of BDCR register before the reset of Backup Domain */
		tmpreg = (uint32_t)(RCC->BDCR & ~((uint32_t)RCC_BDCR_RTCSEL));
		/* RTC Clock selection can be changed only if the Backup Domain is reset */

		RCC->BDCR |= RCC_BDRST_FORCE_RESET_BIT_MASK; /*Force back domain reset*/
		RCC->BDCR &= RCC_BDRST_RELEASE_REET_BIT_MASK;/*Release back domain reet*/

		/* Restore the Content of BDCR register */
		RCC->BDCR = (volatile uint32_t)tmpreg;

		/* Wait for LSE reactivation if LSE was enable prior to Backup Domain reset */
		if(HAL_IS_BIT_SET(RCC->BDCR, RCC_BDCR_LSEON))
		{
			/* Get tick */
			tickstart = HAL_GetTick();

			/* Wait till LSE is ready */
			while(((uint32_t)RESET == (uint32_t)((uint32_t)RCC->BDCR & (uint32_t)RCC_FLAG_LSERDY)) && (HAL_OK == hal_status))
			{
				if((HAL_GetTick() - tickstart ) > RCC_LSE_TIMEOUT_VALUE_MILIS )
				{
					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	/*Configure RTC Clock*/
	CLEAR_BIT(RCC->CFGR, RCC_CFGR_RTCPRE);
	RCC->BDCR |= ((RCC_RTCCLKSOURCE_LSE) & 0x00000FFFU);

	return hal_status;
}


/*************************************************************************************
** hal_init_tick - This function configures the source of the time base.
**                The time source is configured  to have 1ms time base with a dedicated
**                Tick interrupt priority.
**
** Params : None
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:This function is called when clock is reconfigured  by HAL_RCC_ClockConfig().
*************************************************************************************/
static HAL_StatusTypeDef hal_init_tick(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/* Configure the SysTick to have interrupt in 1ms time basis*/
	if (HAL_SYSTICK_Config(SystemCoreClock / (1000U / Tick_Freq)) > 0U)
	{
		hal_status = HAL_ERROR;
	}
	else
	{
		/* Configure the SysTick IRQ priority */
		HAL_NVIC_SetPriority(SysTick_IRQn, TICK_INT_PRIORITY, 0U);
	}
	/* Return function status */
	return hal_status;
}



/*************************************************************************************
** HAL_IncTick - This function is called in interrupt handler to increment  a global variable "Tick_Freq"
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void HAL_IncTick(void)
{
	Tick_Value += Tick_Freq;
}

/*************************************************************************************
** HAL_GetTick - Provides a tick value in millisecond
**
** Params : None
**
** Returns: uint32_t - tick value in millisecond
** Notes:
*************************************************************************************/
uint32_t HAL_GetTick(void)
{
	return Tick_Value;
}

/*************************************************************************************
** HAL_Delay - This function provides minimum delay (in milliseconds) based on variable incremented.
** This function initializes and configures all MCUs peripherals
**
** Params : uint32_t milliseconds - Specifies the delay time length, in milliseconds.
**
** Returns: None.
** Notes: This procedure is flag driven(not interrupt) for being able to use inside interrupts.
*************************************************************************************/
void HAL_Delay(uint32_t milliseconds)
{
	/* Initially clear flag */
	(void) SysTick->CTRL;

	while (0U != milliseconds)
	{
		/* COUNTFLAG returns 1 if timer counted to 0 since the last flag read */
		milliseconds -= (uint32_t)(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_MSK) >> SysTick_CTRL_COUNTFLAG_POS;
	}
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
