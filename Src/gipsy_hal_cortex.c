/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_cortex.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 15-08-2021
**
** Description: Cortex core low level functions.
**
*/


/* Revision Log:
**
** Rev 1.0  : 04-11-2020 -- Original version created.
** Rev 1.1  : 20-11-2020 -- Static analysis update
** Rev 1.2  : 23-12-2020 -- Updated due to verification remarks
** Rev 1.3  : 15-08-2021 -- Added include gcc
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_cortex.h"
#include "cmsis_gcc.h"
/* -----Definitions ----------------------------------------------------------------*/
#define   _NVIC_PRIO_BITS          4U       /*!< STM32F4XX uses 4 Bits for the Priority Levels */

/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
static uint32_t nvic_encode_priority (uint32_t PriorityGroup, uint32_t PreemptPriority, uint32_t SubPriority);

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** HAL_NVIC_SetPriorityGrouping - Sets the priority grouping field using the required unlock sequence.
**
**
** Params : uint32_t PriorityGroup - The priority grouping bits length.
** This parameter can be one of the following values:
**         @arg NVIC_PRIORITYGROUP_0: 0 bits for preemption priority
**                                    4 bits for subpriority
**         @arg NVIC_PRIORITYGROUP_1: 1 bits for preemption priority
**                                    3 bits for subpriority
**         @arg NVIC_PRIORITYGROUP_2: 2 bits for preemption priority
**                                    2 bits for subpriority
**         @arg NVIC_PRIORITYGROUP_3: 3 bits for preemption priority
**                                    1 bits for subpriority
**         @arg NVIC_PRIORITYGROUP_4: 4 bits for preemption priority
**                                    0 bits for subpriority
** Returns: None
** Notes:    Sets the priority grouping field using the required unlock sequence.
**           The parameter PriorityGroup is assigned to the field SCB->AIRCR [10:8] PRIGROUP field.
**           Only values from 0..7 are used.
**           In case of a conflict between priority grouping and available
**           priority bits (_NVIC_PRIO_BITS), the smallest possible priority group is set.
*************************************************************************************/
void HAL_NVIC_SetPriorityGrouping(uint32_t PriorityGroup)
{
	/*Check input parameters*/
	if(((uint32_t)NVIC_PRIORITYGROUP_0 == PriorityGroup) || ((uint32_t)NVIC_PRIORITYGROUP_1 == PriorityGroup) ||\
       ((uint32_t)NVIC_PRIORITYGROUP_2 == PriorityGroup) || ((uint32_t)NVIC_PRIORITYGROUP_3 == PriorityGroup) ||\
	   ((uint32_t)NVIC_PRIORITYGROUP_4 == PriorityGroup))
	{
		/* Set the PRIGROUP[10:8] bits according to the PriorityGroup parameter value */
		uint32_t reg_value;
		uint32_t PriorityGroupTmp = (PriorityGroup & (uint32_t)0x07UL);             /* only values 0..7 are used          */

		reg_value  =  SCB->AIRCR;                                                   /* read old register configuration    */
		reg_value &= ~((uint32_t)(SCB_AIRCR_VECTKEY_MSK | SCB_AIRCR_PRIGROUP_MSK)); /* clear bits to change               */
		reg_value  =  (reg_value | ((uint32_t)0x5FAUL << SCB_AIRCR_VECTKEY_POS) | (PriorityGroupTmp << 8U));/* Insert write key and priority group */
		SCB->AIRCR =  (uint32_t)reg_value;
	}
}

/*************************************************************************************
** HAL_NVIC_SetPriority - Sets the priority of an interrupt.
**
**
** Params : IRQn_Type IRQn - Interrupt number. This parameter can be an enumerator of IRQn_Type enumeration
** uint32_t PreemptPriority - The preemption priority for the IRQn channel.This parameter can be a value between 0 and 15
** uint32_t SubPriority - The sub-priority level for the IRQ channel.This parameter can be a value between 0 and 15
**
** Returns: None
** Notes: A lower priority value indicates a higher priority.
*************************************************************************************/
void HAL_NVIC_SetPriority(IRQn_Type IRQn, uint32_t PreemptPriority, uint32_t SubPriority)
{ 
	uint32_t prioritygroup = 0x00U;
	uint32_t priority      = 0x00U;

	/*Check Input parameters*/
	if((15U >= PreemptPriority) && (15U >= SubPriority))
	{
		/*Read the priority grouping field from the NVIC Interrupt Controller*/
		prioritygroup = ((uint32_t)((uint32_t)(SCB->AIRCR & SCB_AIRCR_PRIGROUP_MSK) >> SCB_AIRCR_PRIGROUP_POS));

		/*Encodes the priority for an interrupt with the given priority group,preemptive priority value, and sub-priority value*/
		priority = nvic_encode_priority(prioritygroup, PreemptPriority, SubPriority);

		/*Set Interrupt Priority*/
		if ((int32_t)(IRQn) < 0)
		{
			/*For Internal interrupts (Systick)*/
			SCB->SHP[(((uint32_t)(int32_t)IRQn) & 0xFUL)-4UL] = (uint8_t)((priority << (8U - _NVIC_PRIO_BITS)) & (uint32_t)0xFFUL);
		}
		else
		{
			/*For External interrupts*/
			NVIC->IP[((uint32_t)(int32_t)IRQn)] = (uint8_t)((priority << (8U - _NVIC_PRIO_BITS)) & (uint32_t)0xFFUL);
		}
	}
}

/*************************************************************************************
** nvic_encode_priority - Encodes the priority for an interrupt with the given priority group,
** 						  preemptive priority value, and sub-priority value.
**
**
** Params : uint32_t PriorityGroup - Used priority group.
**          uint32_t PreemptPriority - Preemptive priority value (starting from 0).
**          uint32_t Subpriority value (starting from 0).
**
** Returns: uint32_t - Encoded priority.
** Notes:  In case of a conflict between priority grouping and available
**         priority bits (__NVIC_PRIO_BITS), the smallest possible priority group is set.
*************************************************************************************/
static uint32_t nvic_encode_priority (uint32_t PriorityGroup, uint32_t PreemptPriority, uint32_t SubPriority)
{
	uint32_t priority_group_tmp = (PriorityGroup & (uint32_t)0x07U);   /* only values 0..7 are used*/
	uint32_t preempt_priority_bits = 0U;
	uint32_t sub_priority_bits = 0U;


	if((uint32_t)(_NVIC_PRIO_BITS) < (7U - priority_group_tmp))
	{
		preempt_priority_bits = (uint32_t)(_NVIC_PRIO_BITS);
	}
	else
	{
		preempt_priority_bits = (uint32_t)(7U - priority_group_tmp);
	}

	if((priority_group_tmp + (uint32_t)(_NVIC_PRIO_BITS)) < (uint32_t)7U)
	{
		sub_priority_bits = (uint32_t)0U;
	}
	else
	{
		sub_priority_bits = (uint32_t)((priority_group_tmp - 7U) + (uint32_t)(_NVIC_PRIO_BITS));
	}


	return (((PreemptPriority & (uint32_t)((1UL << (preempt_priority_bits)) - 1U)) << sub_priority_bits) |\
			((SubPriority & (uint32_t)((1U << (sub_priority_bits    )) - 1U))));
}

/*************************************************************************************
** HAL_NVIC_EnableIRQ - Enable External Interrupt.Enables a device-specific interrupt in the NVIC interrupt controller.
**
**
** Params : IRQn_Type IRQn - External interrupt number. Value cannot be negative.
** Returns: None
** Notes:
*************************************************************************************/
void HAL_NVIC_EnableIRQ(IRQn_Type IRQn)
{
	/* Check the parameters */
	if((0U < (uint32_t)IRQn) && (91U > (uint32_t)IRQn))
	{
		/* Enable interrupt */
		NVIC->ISER[(((uint32_t)(int32_t)IRQn) >> 5UL)] = (uint32_t)(1U << (((uint32_t)(int32_t)IRQn) & 0x1FU));
	}
}
/*************************************************************************************
** HAL_SYSTICK_Config - Initializes the System Timer and its interrupt, and starts the System Tick Timer.
** 						Counter is in free running mode to generate periodic interrupts.
**
**
** Params : uint32_t TicksNumb - Specifies the ticks Number of ticks between two interrupts.
**
** Returns: uint32_t config_status:   0 - Function succeeded.  1 - Function failed.
** Notes:
*************************************************************************************/
uint32_t HAL_SYSTICK_Config(uint32_t TicksNumb)
{
	uint32_t config_status = 0U;
	if (SysTick_LOAD_RELOAD_MSK < (TicksNumb - 1U)) /*Input parameter check*/
	{
		config_status = 1U; /* Reload value impossible */
	}
	else
	{
		SysTick->LOAD  = (uint32_t)(TicksNumb - 1U);                         /* Set reload register */
		HAL_NVIC_SetPriority(SysTick_IRQn, (1U << _NVIC_PRIO_BITS) - 1U, 0U); /* Set Priority for Systick Interrupt - default manufacturer value */
		SysTick->VAL   = 0U;                                             /* Load the SysTick Counter Value */
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_MSK | SysTick_CTRL_TICKINT_MSK | SysTick_CTRL_ENABLE_MSK;/* Enable SysTick IRQ and SysTick Timer */
	}
	return config_status;
}

#ifdef _DEBUG_
/*************************************************************************************
** HAL_NVIC_SystemReset - Initiates a system reset request to reset the MCU.
**
**
** Params : None
** Returns: None
** Notes:
*************************************************************************************/
void HAL_NVIC_SystemReset(void)
{
	/* System Reset */
	__DSB(); /* Ensure all outstanding memory accesses included buffered write are completed before reset */
	SCB->AIRCR  = (uint32_t)((0x5FAUL << SCB_AIRCR_VECTKEY_POS)|(SCB->AIRCR & SCB_AIRCR_PRIGROUP_MSK) | SCB_AIRCR_SYSRESETREQ_MSK);/* Keep priority group unchanged */
	__DSB();/* Ensure completion of memory access */

	for(;TRUE;) /* wait until reset */
	{
		__NOP();
	}
}

#endif /*_DEBUG_*/

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
