/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gipsy_hal_uart.c
** Author    : Leonid Savchenko
** Revision  : 1.8
** Updated   : 12-07-2021
**
** Description: This file contains procedures for STM32 UART interface.
*/


/* Revision Log:
**
** Rev 1.0  : 12-07-2020 -- Original version created.
** Rev 1.1  : 13-08-2020 -- Fixed bug in uart_wait_on_flag_until_timeout()
** Rev 1.2  : 15-08-2020 -- Added SR/DR registers flush at UART error occurrence in HAL_UART_IRQHandler()
** Rev 1.3  : 13-12-2020 -- Fixed USART1 init bug in "uart_set_config"
** Rev 1.4	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.5	: 25-04-2021 -- Update due to LDRA remarks.
** Rev 1.6	: 26-04-2021 -- Restoring prev code version of the following "defines" due bugs in communication:
** 							UART_DIVMANT_SAMPLING16
** 							UART_DIVFRAQ_SAMPLING16
** Rev 1.7  : 01-07-2021 -- Update the function 'HAL_UART_IRQHandler' due to LDRA remarks.
** 						  Added uint casting according to the LDRA remarks. (on 'SET' ans RESET' occurrences).
** Rev 1.8  : 12-07-2021 -- Updated uart_receive_it()
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_uart.h"
#include "gipsy_hal_rcc.h"
#ifdef _DEBUG_
#include "uart.h"
#endif

#ifdef _DIMC_
#include "irs_protocol_dimc.h"
#endif
/* -----Definitions ----------------------------------------------------------------*/

/*******************  Bit definition for USART_SR register  *******************/
#define USART_SR_PE_POS               (0U)
#define USART_SR_PE_MSK               (0x1UL << USART_SR_PE_POS)                /*!< 0x00000001 */
#define USART_SR_PE                   USART_SR_PE_MSK                          /*!<Parity Error                 */
#define USART_SR_FE_POS               (1U)
#define USART_SR_FE_MSK               (0x1UL << USART_SR_FE_POS)                /*!< 0x00000002 */
#define USART_SR_FE                   USART_SR_FE_MSK                          /*!<Framing Error                */
#define USART_SR_NE_POS               (2U)
#define USART_SR_NE_MSK               (0x1UL << USART_SR_NE_POS)                /*!< 0x00000004 */
#define USART_SR_NE                   USART_SR_NE_MSK                          /*!<Noise Error Flag             */
#define USART_SR_ORE_POS              (3U)
#define USART_SR_ORE_MSK              (0x1UL << USART_SR_ORE_POS)               /*!< 0x00000008 */
#define USART_SR_ORE                  USART_SR_ORE_MSK                         /*!<OverRun Error                */
#define USART_SR_RXNE_POS             (5U)
#define USART_SR_RXNE_MSK             (0x1UL << USART_SR_RXNE_POS)              /*!< 0x00000020 */
#define USART_SR_RXNE                 USART_SR_RXNE_MSK                        /*!<Read Data Register Not Empty */
#define USART_SR_TC_POS               (6U)
#define USART_SR_TC_MSK               (0x1UL << USART_SR_TC_POS)                /*!< 0x00000040 */
#define USART_SR_TC                   USART_SR_TC_MSK                          /*!<Transmission Complete        */
#define USART_SR_TXE_POS              (7U)
#define USART_SR_TXE_MSK              (0x1UL << USART_SR_TXE_POS)               /*!< 0x00000080 */
#define USART_SR_TXE                  USART_SR_TXE_MSK                         /*!<Transmit Data Register Empty */

/******************  Bit definition for USART_CR1 register  *******************/
#define USART_CR1_RE_POS              (2U)
#define USART_CR1_RE_MSK              (0x1UL << USART_CR1_RE_POS)               /*!< 0x00000004 */
#define USART_CR1_RE                  USART_CR1_RE_MSK                         /*!<Receiver Enable                        */
#define USART_CR1_TE_POS              (3U)
#define USART_CR1_TE_MSK              (0x1UL << USART_CR1_TE_POS)               /*!< 0x00000008 */
#define USART_CR1_TE                  USART_CR1_TE_MSK                         /*!<Transmitter Enable                     */
#define USART_CR1_RXNEIE_POS          (5U)
#define USART_CR1_RXNEIE_MSK          (0x1UL << USART_CR1_RXNEIE_POS)           /*!< 0x00000020 */
#define USART_CR1_RXNEIE              USART_CR1_RXNEIE_MSK                     /*!<RXNE Interrupt Enable                  */
#define USART_CR1_TXEIE_POS           (7U)
#define USART_CR1_TXEIE_MSK           (0x1UL << USART_CR1_TXEIE_POS)            /*!< 0x00000080 */
#define USART_CR1_TXEIE               USART_CR1_TXEIE_MSK                      /*!<TXE Interrupt Enable                   */
#define USART_CR1_PEIE_POS            (8U)
#define USART_CR1_PEIE_MSK            (0x1UL << USART_CR1_PEIE_POS)             /*!< 0x00000100 */
#define USART_CR1_PEIE                USART_CR1_PEIE_MSK                       /*!<PE Interrupt Enable                    */
#define USART_CR1_PS_POS              (9U)
#define USART_CR1_PS_MSK              (0x1UL << USART_CR1_PS_POS)               /*!< 0x00000200 */
#define USART_CR1_PS                  USART_CR1_PS_MSK                         /*!<Parity Selection                       */
#define USART_CR1_PCE_POS             (10U)
#define USART_CR1_PCE_MSK             (0x1UL << USART_CR1_PCE_POS)              /*!< 0x00000400 */
#define USART_CR1_PCE                 USART_CR1_PCE_MSK                        /*!<Parity Control Enable                  */
#define USART_CR1_M_POS               (12U)
#define USART_CR1_M_MSK               (0x1UL << USART_CR1_M_POS)                /*!< 0x00001000 */
#define USART_CR1_M                   USART_CR1_M_MSK                          /*!<Word length                            */
#define USART_CR1_UE_POS              (13U)
#define USART_CR1_UE_MSK              (0x1UL << USART_CR1_UE_POS)               /*!< 0x00002000 */
#define USART_CR1_UE                  USART_CR1_UE_MSK                         /*!<USART Enable                           */
#define USART_CR1_OVER8_POS           (15U)
#define USART_CR1_OVER8_MSK           (0x1UL << USART_CR1_OVER8_POS)            /*!< 0x00008000 */
#define USART_CR1_OVER8               USART_CR1_OVER8_MSK                      /*!<USART Oversampling by 8 enable         */

/******************  Bit definition for USART_CR2 register  *******************/
#define USART_CR2_CLKEN_POS           (11U)
#define USART_CR2_CLKEN_MSK           (0x1UL << USART_CR2_CLKEN_POS)            /*!< 0x00000800 */
#define USART_CR2_CLKEN               USART_CR2_CLKEN_MSK                      /*!<Clock Enable                         */
#define USART_CR2_STOP_POS            (12U)
#define USART_CR2_STOP_MSK            (0x3UL << USART_CR2_STOP_POS)             /*!< 0x00003000 */
#define USART_CR2_STOP                USART_CR2_STOP_MSK                       /*!<STOP[1:0] bits (STOP bits) */
#define USART_CR2_LINEN_POS           (14U)
#define USART_CR2_LINEN_MSK           (0x1UL << USART_CR2_LINEN_POS)            /*!< 0x00004000 */
#define USART_CR2_LINEN               USART_CR2_LINEN_MSK                      /*!<LIN mode enable */

/******************  Bit definition for USART_CR3 register  *******************/
#define USART_CR3_EIE_POS             (0U)
#define USART_CR3_EIE_MSK             (0x1UL << USART_CR3_EIE_POS)              /*!< 0x00000001 */
#define USART_CR3_EIE                 USART_CR3_EIE_MSK                        /*!<Error Interrupt Enable      */
#define USART_CR3_IREN_POS            (1U)
#define USART_CR3_IREN_MSK            (0x1UL << USART_CR3_IREN_POS)             /*!< 0x00000002 */
#define USART_CR3_IREN                USART_CR3_IREN_MSK                       /*!<IrDA mode Enable            */
#define USART_CR3_HDSEL_POS           (3U)
#define USART_CR3_HDSEL_MSK           (0x1UL << USART_CR3_HDSEL_POS)            /*!< 0x00000008 */
#define USART_CR3_HDSEL               USART_CR3_HDSEL_MSK                      /*!<Half-Duplex Selection       */
#define USART_CR3_SCEN_POS            (5U)
#define USART_CR3_SCEN_MSK            (0x1UL << USART_CR3_SCEN_POS)             /*!< 0x00000020 */
#define USART_CR3_SCEN                USART_CR3_SCEN_MSK                       /*!<Smartcard mode enable       */
#define USART_CR3_RTSE_POS            (8U)
#define USART_CR3_RTSE_MSK            (0x1UL << USART_CR3_RTSE_POS)             /*!< 0x00000100 */
#define USART_CR3_RTSE                USART_CR3_RTSE_MSK                       /*!<RTS Enable                  */
#define USART_CR3_CTSE_POS            (9U)
#define USART_CR3_CTSE_MSK            (0x1UL << USART_CR3_CTSE_POS)             /*!< 0x00000200 */
#define USART_CR3_CTSE                USART_CR3_CTSE_MSK                       /*!<CTS Enable                  */

/*UART FLags: Elements values convention: 0xXXXX - 0xXXXX  : Flag mask in the SR register*/
#define UART_FLAG_CTS                       ((uint32_t)USART_SR_CTS)
#define UART_FLAG_LBD                       ((uint32_t)USART_SR_LBD)
#define UART_FLAG_TXE                       ((uint32_t)USART_SR_TXE)
#define UART_FLAG_TC                        ((uint32_t)USART_SR_TC)
#define UART_FLAG_RXNE                      ((uint32_t)USART_SR_RXNE)
#define UART_FLAG_IDLE                      ((uint32_t)USART_SR_IDLE)
#define UART_FLAG_ORE                       ((uint32_t)USART_SR_ORE)
#define UART_FLAG_NE                        ((uint32_t)USART_SR_NE)
#define UART_FLAG_FE                        ((uint32_t)USART_SR_FE)
#define UART_FLAG_PE                        ((uint32_t)USART_SR_PE)


#define UART_CR1_REG_INDEX               1U
#define UART_CR2_REG_INDEX               2U
#define UART_CR3_REG_INDEX               3U

/*UART Error Code*/
#define HAL_UART_ERROR_NONE         0x00000000U   /*!< No error            */
#define HAL_UART_ERROR_PE           0x00000001U   /*!< Parity error        */
#define HAL_UART_ERROR_NE           0x00000002U   /*!< Noise error         */
#define HAL_UART_ERROR_FE           0x00000004U   /*!< Frame error         */
#define HAL_UART_ERROR_ORE          0x00000008U   /*!< Overrun error       */
/* -----Macros ---------------------------------------------------------------------*/

#define UART_DIV_SAMPLING16(_PCLK_, _BAUD_)            (((_PCLK_)*25U)/(4U*(_BAUD_)))

#define UART_DIVMANT_SAMPLING16(_PCLK_, _BAUD_)        (UART_DIV_SAMPLING16((_PCLK_), (_BAUD_))/100U)
#define UART_DIVFRAQ_SAMPLING16(_PCLK_, _BAUD_)        ((((UART_DIV_SAMPLING16((_PCLK_), (_BAUD_)) - (UART_DIVMANT_SAMPLING16((_PCLK_), (_BAUD_)) * 100U)) * 16U) + 50U) / 100U)
#define UART_BRR_SAMPLING16(_PCLK_, _BAUD_)            (((UART_DIVMANT_SAMPLING16((_PCLK_), (_BAUD_)) << 4U) + \
                                                        (UART_DIVFRAQ_SAMPLING16((_PCLK_), (_BAUD_)) & 0xF0U)) + \
                                                        (UART_DIVFRAQ_SAMPLING16((_PCLK_), (_BAUD_)) & 0x0FU))

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

static void uart_receive_it(UART_HandleTypeDef *huart);
static HAL_StatusTypeDef uart_wait_on_flag_until_timeout(UART_HandleTypeDef *huart, uint32_t flag, uint8_t status, uint32_t tickstart, uint32_t timeout);
static void uart_set_config (UART_HandleTypeDef *huart);



/*************************************************************************************
** HAL_UART_Init - Initializes the UART mode according to the specified parameters in
**                 the UART_InitTypeDef and create the associated handle.
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
**
** Returns: None.
** Notes:
*************************************************************************************/
void HAL_UART_Init(UART_HandleTypeDef *huart)
{
	/* Disable the peripheral */
	huart->Instance->CR1 &=  ~USART_CR1_UE;

	/* Set the UART Communication parameters */
	uart_set_config(huart);

	/* In asynchronous mode, the following bits must be kept cleared:
		 - LINEN and CLKEN bits in the USART_CR2 register,
		 - SCEN, HDSEL and IREN  bits in the USART_CR3 register.*/
	CLEAR_BIT(huart->Instance->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
	CLEAR_BIT(huart->Instance->CR3, (USART_CR3_SCEN | USART_CR3_HDSEL | USART_CR3_IREN));

	/* Enable the peripheral */
	huart->Instance->CR1 |=  (uint64_t)USART_CR1_UE;

	/* Initialize the UART state */
	huart->ErrorCode = HAL_UART_ERROR_NONE;
	huart->gState= E_HAL_UART_STATE_READY;
	huart->RxState= E_HAL_UART_STATE_READY;
}

/*************************************************************************************
** uart_set_config - Configures the UART peripheral.
**
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void uart_set_config(UART_HandleTypeDef *huart)
{


  uint32_t tmpreg = 0x00U;

  /*-------------------------- USART CR2 Configuration -----------------------*/
  tmpreg = huart->Instance->CR2;

   /*Clear STOP[13:12] bits*/
  tmpreg &= (uint32_t)~((uint32_t)USART_CR2_STOP);

   /*Configure the UART Stop Bits: Set STOP[13:12] bits according to huart->Init.StopBits value*/
  tmpreg |= (uint32_t)huart->Init.StopBits;

   /*Write to USART CR2*/
  WRITE_REG(huart->Instance->CR2, (uint32_t)tmpreg);

  /*-------------------------- USART CR1 Configuration -----------------------*/
  tmpreg = huart->Instance->CR1;

  /* Clear M, PCE, PS, TE and RE bits*/
  tmpreg &= (uint32_t)~((uint32_t)(USART_CR1_M | USART_CR1_PCE | USART_CR1_PS | USART_CR1_TE | \
                                   USART_CR1_RE | USART_CR1_OVER8));

/*   Configure the UART Word Length, Parity and mode:
     Set the M bits according to huart->Init.WordLength value
     Set PCE and PS bits according to huart->Init.Parity value
     Set TE and RE bits according to huart->Init.Mode value
     Set OVER8 bit according to huart->Init.OverSampling value*/
  tmpreg |= (uint32_t)huart->Init.WordLength | huart->Init.Parity | huart->Init.Mode | huart->Init.OverSampling;

   /*Write to USART CR1*/
  WRITE_REG(huart->Instance->CR1, (uint32_t)tmpreg);

  /*-------------------------- USART CR3 Configuration -----------------------*/
  tmpreg = huart->Instance->CR3;

  /* Clear CTSE and RTSE bits*/
  tmpreg &= (uint32_t)~((uint32_t)(USART_CR3_RTSE | USART_CR3_CTSE));

  /* Configure the UART HFC: Set CTSE and RTSE bits according to huart->Init.HwFlowCtl value*/
  tmpreg |= huart->Init.HwFlowCtl;

  /* Write to USART CR3*/
  WRITE_REG(huart->Instance->CR3, (uint32_t)tmpreg);

  if(huart->Instance == USART1)
  {
	  huart->Instance->BRR = (uint32_t)UART_BRR_SAMPLING16(HAL_RCC_GetPCLK2Freq(), huart->Init.BaudRate);
  }
  else
  {
#ifdef _DEBUG_
  huart->Instance->BRR = (uint32_t)UART_BRR_SAMPLING16(HAL_RCC_GetPCLK1Freq(), huart->Init.BaudRate);
#endif
  }
}

/*************************************************************************************
** HAL_UART_Transmit - Sends an amount of data in blocking mode.
**
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
** 			uint8_t *pdata - Pointer to data buffer
**          uint16_t size - Amount of data to be sent
**          uint32_t timeout - Timeout duration
**
** Returns: HAL_StatusTypeDef - Hal Status
**
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_UART_Transmit(UART_HandleTypeDef *huart, uint8_t *pdata,uint16_t size, uint32_t timeout)
{
	uint16_t *tmp = 0x00U;
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Check that a Tx process is not already ongoing */
	if (huart->gState == E_HAL_UART_STATE_READY)
	{
		if ((NULL == huart) || (NULL == pdata) || (0U == size))
		{
			hal_status = HAL_ERROR;
		}
		else
		{
			huart->ErrorCode = HAL_UART_ERROR_NONE;
			huart->gState = E_HAL_UART_STATE_BUSY_TX;

			/* Init tickstart for timeout managment */
			tickstart = HAL_GetTick();

			huart->TxXferSize = size;
			huart->TxXferCount = size;
			while ((0U < huart->TxXferCount) && (HAL_OK == hal_status))
			{
				huart->TxXferCount--;
				/*For UART_WORDLENGTH_9B and parity not equal to UART_PARITY_NONE*/

				if (HAL_OK != uart_wait_on_flag_until_timeout(huart, UART_FLAG_TXE, RESET, tickstart, timeout))
				{
					hal_status = HAL_TIMEOUT;
				}
				if(HAL_OK == hal_status)
				{
					tmp = (uint16_t*) pdata;
					huart->Instance->DR = (*tmp & (uint16_t) 0x01FF);
					pdata += 1U;
				}
			}
			if(HAL_OK == hal_status)
			{
				if (HAL_OK != uart_wait_on_flag_until_timeout(huart, UART_FLAG_TC, RESET, tickstart,timeout))
				{
					hal_status = HAL_TIMEOUT;
				}
			}

			/* At end of Tx process, restore huart->gState to Ready */
			huart->gState = E_HAL_UART_STATE_READY;
		}
	}
	else
	{
		hal_status = HAL_BUSY;
	}
	return hal_status;
}

/*************************************************************************************
** uart_wait_on_flag_until_timeout - This function handles UART Communication Timeout.
**
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
**								uint32_t  flag - Specifies the UART flag to check(See notes)
**								uint8_t status - The new Flag status (SET or RESET).
**								uint32_t tickstart - Tick start value
**								uint32_t timeout - Timeout duration
**
** Returns: HAL_StatusTypeDef - Hal Status
**
** Notes: UART flag can be one of the following values:
**           UART_FLAG_TXE:  Transmit data register empty flag
**           UART_FLAG_TC:   Transmission Complete flag
*************************************************************************************/
static HAL_StatusTypeDef uart_wait_on_flag_until_timeout(UART_HandleTypeDef *huart, uint32_t flag, uint8_t status, uint32_t tickstart, uint32_t timeout)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t flag_status = 0U;
	/*Check input parameters*/
	if((NULL != huart) && ((SET == status) || (RESET == status)) && ((UART_FLAG_TXE == flag) || (UART_FLAG_TC == flag)))
	{
		/*Get initial flag's state before entering waiting loop*/
		if(flag == (huart->Instance->SR & flag))
		{
			flag_status = (uint8_t)SET;
		}
		else
		{
			flag_status = (uint8_t)RESET;
		}
		/* Wait until flag is set */
		while((HAL_OK == hal_status) && (flag_status == status))
		{
			/*Get flag's and convert it to SET/RESET*/
			if(flag == (huart->Instance->SR & flag))
			{
				flag_status = (uint8_t)SET;
			}
			else
			{
				flag_status = (uint8_t)RESET;
			}

			/* Check for the Timeout */
			if((timeout == 0U)||((HAL_GetTick() - tickstart ) > timeout))
			{
				/* Disable TXE, RXNE, PE and ERR (Frame error, noise error, overrun error) interrupts for the interrupt process */
				CLEAR_BIT(huart->Instance->CR1, (USART_CR1_RXNEIE | USART_CR1_PEIE | USART_CR1_TXEIE));
				CLEAR_BIT(huart->Instance->CR3, USART_CR3_EIE);

				huart->gState  = E_HAL_UART_STATE_READY;
				huart->RxState = E_HAL_UART_STATE_READY;
				hal_status = HAL_TIMEOUT;
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}
/*************************************************************************************
** HAL_UART_IRQHandler - This function handles UART interrupt request.
**
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
**
** Returns: None.
** Notes:
*************************************************************************************/
void HAL_UART_IRQHandler(UART_HandleTypeDef *huart)
{
   uint32_t isrflags   = READ_REG(huart->Instance->SR);
   uint32_t cr1its     = READ_REG(huart->Instance->CR1);
   uint32_t cr3its     = READ_REG(huart->Instance->CR3);
   uint32_t errorflags = 0x00U;
  __IO uint32_t tmpreg = 0x00U;
   huart->ErrorCode = HAL_UART_ERROR_NONE; /*Clear ErrorCode field*/

  /* If no error occurs */
  errorflags = (isrflags & (uint32_t)(USART_SR_PE | USART_SR_FE | USART_SR_ORE | USART_SR_NE));
  /* If no error occurs - receive data */
  if(RESET == errorflags)
  {
    /* UART in mode Receiver -------------------------------------------------*/
    if(((isrflags & USART_SR_RXNE) != (uint32_t)RESET) && ((cr1its & USART_CR1_RXNEIE) != (uint32_t)RESET))
    {
      uart_receive_it(huart);
    }
  }
  else
  {
	  /*In case an error occurred, flush SR/DR registers by reading their value*/
	    tmpreg = huart->Instance->SR;
	    tmpreg = huart->Instance->DR;
  }


  /* If some errors occur - update ErrorCode for debug purposes*/
  if((errorflags != (uint32_t)RESET) && (((cr3its & USART_CR3_EIE) != (uint32_t)RESET) || ((cr1its & (USART_CR1_RXNEIE | USART_CR1_PEIE)) != (uint32_t)RESET)))
  {
    /* UART parity error interrupt occurred ----------------------------------*/
    if(((isrflags & USART_SR_PE) != (uint32_t)RESET) && ((cr1its & USART_CR1_PEIE) != (uint32_t)RESET))
    {
      huart->ErrorCode |= HAL_UART_ERROR_PE;
    }
    
    /* UART noise error interrupt occurred -----------------------------------*/
    if(((isrflags & USART_SR_NE) != (uint32_t)RESET) && ((cr3its & USART_CR3_EIE) != (uint32_t)RESET))
    {
      huart->ErrorCode |= HAL_UART_ERROR_NE;
    }
    
    /* UART frame error interrupt occurred -----------------------------------*/
    if(((isrflags & USART_SR_FE) != (uint32_t)RESET) && ((cr3its & USART_CR3_EIE) != (uint32_t)RESET))
    {
      huart->ErrorCode |= HAL_UART_ERROR_FE;
    }
    
    /* UART Over-Run interrupt occurred --------------------------------------*/
    if(((isrflags & USART_SR_ORE) != (uint32_t)RESET) && ((cr3its & USART_CR3_EIE) != (uint32_t)RESET))
    { 
      huart->ErrorCode |= HAL_UART_ERROR_ORE;
    }
  }
}
/*************************************************************************************
** HAL_UART_Receive_IT - Receives an amount of data in non blocking mode .
**
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
** 			uint8_t *pdata - Pointer to data buffer
**          uint16_t size - Amount of data to be received
**
** Returns: HAL_StatusTypeDef - Hal Status
**
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_UART_Receive_IT(UART_HandleTypeDef *huart, uint8_t *pdata, uint16_t size)
{
	HAL_StatusTypeDef hal_state = HAL_OK;
	/* Check that a Rx process is not already ongoing */
	if(huart->RxState == E_HAL_UART_STATE_READY)
	{
		if((NULL != pdata) && (0U != size))
		{
			huart->pRxBuffPtr = pdata;
			huart->RxXferSize = size;
			huart->RxXferCount = size;

			huart->ErrorCode = HAL_UART_ERROR_NONE;
			huart->RxState = E_HAL_UART_STATE_BUSY_RX;

			/* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
			SET_BIT(huart->Instance->CR3, USART_CR3_EIE);

			/* Enable the UART Parity Error and Data Register not empty Interrupts */
			SET_BIT(huart->Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);
		}
		else
		{
			hal_state = HAL_ERROR;
		}
	}
	else
	{
		hal_state = HAL_BUSY;
	}
	return hal_state;
}


/*************************************************************************************
** uart_receive_it - Receives an amount of data in non blocking mode
**
** Params : UART_HandleTypeDef *huart - Pointer to a UART_HandleTypeDef structure that contains
** 										the configuration information for the specified UART module.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void uart_receive_it(UART_HandleTypeDef *huart)
{
	uint16_t *tmp = 0x00U;

	/* Check that a Rx process is ongoing */
	if(huart->RxState == E_HAL_UART_STATE_BUSY_RX)
	{
		/* For UART parameters: UART_WORDLENGTH_9B and UART_PARITY_ODD*/
		tmp = (uint16_t*) huart->pRxBuffPtr;

		if (NULL != tmp)
		{
			*tmp = (uint16_t)(huart->Instance->DR & (uint16_t)0x00FFU);
			huart->pRxBuffPtr += 1U;
		}
	}

	/*Check if Rx Buffer is empty*/
	if((--huart->RxXferCount) == 0U)	/* LDRA - violation by using "--huart.." */
	{
		/* Disable the UART Parity Error Interrupt and RXNE interrupt*/
		CLEAR_BIT(huart->Instance->CR1, (USART_CR1_RXNEIE | USART_CR1_PEIE));

		/* Disable the UART Error Interrupt: (Frame error, noise error, overrun error) */
		CLEAR_BIT(huart->Instance->CR3, USART_CR3_EIE);

		/* Rx process is completed, restore huart->RxState to Ready */
		huart->RxState = E_HAL_UART_STATE_READY;

		/*Call Rx Interrupt Callback according to UART's instance name*/
#ifdef _DEBUG_
		if(huart->Instance == USART3)
		{
			HAL_UART3_RxCpltCallback(huart);/*Callback for debug uart port*/
		}
#endif

#ifdef _DIMC_
		if(huart->Instance == USART1)
		{
			HAL_UART1_RxCpltCallback(huart);/*Callback for DIMC IRS uart port*/
		}
#endif
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
