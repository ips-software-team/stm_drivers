/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gipsy_hal_rtc.c
** Author    : Leonid Savchenko
** Revision  : 1.8
** Updated   : 27-06-2021
**
** Description: This file procedures for STM32 internal RTC initialization, configuration
** 				and Time/Date Read/Write.
*/


/* Revision Log:
**
** Rev 1.0  : 21-01-2020 -- Original version created.
** Rev 1.1  : 03-02-2020 -- Updated RTC_StartUp_Initialization()
** Rev 1.2  : 06-03-2020 -- Added local defines and macros and updated hal_rtc_set_time()
** Rev 1.3  : 28-07-2020 -- Added RTC backup registers Read/Write functions
** Rev 1.4  : 16-08-2020 -- Removed CLK configuration from RTC_StartUp_Initialization()
** Rev 1.5  : 05-09-2020 -- Added registers definitions
** Rev 1.6	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.7	: 25-04-2021 -- Updated due to verification remarks.
** Rev 1.8	: 27-06-2021 -- Updated due to WDOG test changes.
**************************************************************************************
*/

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_rtc.h"
#include "gipsy_hal_rcc.h"
/* -----Definitions ----------------------------------------------------------------*/
/* Masks Definition */
#define RTC_TR_RESERVED_MASK    0x007F7F7FU
#define RTC_DR_RESERVED_MASK    0x00FFFF3FU
#define RTC_INIT_MASK           0xFFFFFFFFU
#define RTC_RSF_MASK            0xFFFFFF5FU

#define RTC_TIMEOUT_VALUE_MILIS       1000U
#define RTC_HOURFORMAT_24              0x00000000U /*24 Hour RTC Hour Format*/
#define RTC_OUTPUT_DISABLE             0x00000000U/*RTC_Output_selection_Definition*/

#define RTC_FORMAT_BCD                 0x00000001U/*RTC Input Parameter Format Definition*/

#define RTC_OUTPUT_POLARITY_HIGH       0x00000000U/*RTC Output Polarity Definition*/
#define RTC_OUTPUT_TYPE_PUSHPULL       0x00040000U/*RTC Output Type ALARM OU*/


/*RTC_WeekDay_Definitions*/
#define RTC_WEEKDAY_MONDAY             ((uint8_t)0x01U)
#define RTC_WEEKDAY_TUESDAY            ((uint8_t)0x02U)
#define RTC_WEEKDAY_WEDNESDAY          ((uint8_t)0x03U)
#define RTC_WEEKDAY_THURSDAY           ((uint8_t)0x04U)
#define RTC_WEEKDAY_FRIDAY             ((uint8_t)0x05U)
#define RTC_WEEKDAY_SATURDAY           ((uint8_t)0x06U)
#define RTC_WEEKDAY_SUNDAY             ((uint8_t)0x07U)

/*RTC_Flags_Definitions*/
#define RTC_FLAG_RECALPF                  0x00010000U
#define RTC_FLAG_TAMP2F                   0x00004000U
#define RTC_FLAG_TAMP1F                   0x00002000U
#define RTC_FLAG_TSOVF                    0x00001000U
#define RTC_FLAG_TSF                      0x00000800U
#define RTC_FLAG_WUTF                     0x00000400U
#define RTC_FLAG_ALRBF                    0x00000200U
#define RTC_FLAG_ALRAF                    0x00000100U
#define RTC_FLAG_INITF                    0x00000040U
#define RTC_FLAG_RSF                      0x00000020U
#define RTC_FLAG_INITS                    0x00000010U
#define RTC_FLAG_SHPF                     0x00000008U
#define RTC_FLAG_WUTWF                    0x00000004U
#define RTC_FLAG_ALRBWF                   0x00000002U
#define RTC_FLAG_ALRAWF                   0x00000001U


/* RTC clock is: f_clk = RTCCLK(LSI or LSE) / ((RTC_SYNC_PREDIV + 1) * (RTC_ASYNC_PREDIV + 1)) */
/* Sync pre division for clock */
#ifndef RTC_SYNC_PREDIV
#define RTC_SYNC_PREDIV                 0x3FFU
#endif
/* Async pre division for clock */
#ifndef RTC_ASYNC_PREDIV
#define RTC_ASYNC_PREDIV                0x1FU
#endif


/*RCC BitAddress AliasRegion*/
#define RCC_OFFSET                 (RCC_BASE - PERIPH_BASE)

/* Alias word address of RTCEN bit */
#define RCC_BDCR_OFFSET            (RCC_OFFSET + 0x70U)
#define RCC_RTCEN_BIT_NUMBER       0x0FU
#define RCC_BDCR_RTCEN_BB          (PERIPH_BB_BASE + (RCC_BDCR_OFFSET * 32U) + (RCC_RTCEN_BIT_NUMBER * 4U))

/********************  Bits definition for RTC_TR register  *******************/
#define RTC_TR_HT_POS                 (20U)
#define RTC_TR_HT_MSK                 (0x3UL << RTC_TR_HT_POS)                  /*!< 0x00300000 */
#define RTC_TR_HT                     RTC_TR_HT_MSK
#define RTC_TR_HU_POS                 (16U)
#define RTC_TR_HU_MSK                 (0xFUL << RTC_TR_HU_POS)                  /*!< 0x000F0000 */
#define RTC_TR_HU                     RTC_TR_HU_MSK
#define RTC_TR_MNT_POS                (12U)
#define RTC_TR_MNT_MSK                (0x7UL << RTC_TR_MNT_POS)                 /*!< 0x00007000 */
#define RTC_TR_MNT                    RTC_TR_MNT_MSK
#define RTC_TR_MNU_POS                (8U)
#define RTC_TR_MNU_MSK                (0xFUL << RTC_TR_MNU_POS)                 /*!< 0x00000F00 */
#define RTC_TR_MNU                    RTC_TR_MNU_MSK
#define RTC_TR_ST_POS                 (4U)
#define RTC_TR_ST_MSK                 (0x7UL << RTC_TR_ST_POS)                  /*!< 0x00000070 */
#define RTC_TR_ST                     RTC_TR_ST_MSK
#define RTC_TR_SU_POS                 (0U)
#define RTC_TR_SU_MSK                 (0xFUL << RTC_TR_SU_POS)                  /*!< 0x0000000F */
#define RTC_TR_SU                     RTC_TR_SU_MSK
/********************  Bits definition for RTC_DR register  *******************/
#define RTC_DR_YT_POS                 (20U)
#define RTC_DR_YT_MSK                 (0xFUL << RTC_DR_YT_POS)                  /*!< 0x00F00000 */
#define RTC_DR_YT                     RTC_DR_YT_MSK
#define RTC_DR_YU_POS                 (16U)
#define RTC_DR_YU_MSK                 (0xFUL << RTC_DR_YU_POS)                  /*!< 0x000F0000 */
#define RTC_DR_YU                     RTC_DR_YU_MSK
#define RTC_DR_WDU_POS                (13U)
#define RTC_DR_WDU_MSK                (0x7UL << RTC_DR_WDU_POS)                 /*!< 0x0000E000 */
#define RTC_DR_WDU                    RTC_DR_WDU_MSK
#define RTC_DR_MT_POS                 (12U)
#define RTC_DR_MT_MSK                 (0x1UL << RTC_DR_MT_POS)                  /*!< 0x00001000 */
#define RTC_DR_MT                     RTC_DR_MT_MSK
#define RTC_DR_MU_POS                 (8U)
#define RTC_DR_MU_MSK                 (0xFUL << RTC_DR_MU_POS)                  /*!< 0x00000F00 */
#define RTC_DR_MU                     RTC_DR_MU_MSK
#define RTC_DR_DT_POS                 (4U)
#define RTC_DR_DT_MSK                 (0x3UL << RTC_DR_DT_POS)                  /*!< 0x00000030 */
#define RTC_DR_DT                     RTC_DR_DT_MSK
#define RTC_DR_DU_POS                 (0U)
#define RTC_DR_DU_MSK                 (0xFUL << RTC_DR_DU_POS)                  /*!< 0x0000000F */
#define RTC_DR_DU                     RTC_DR_DU_MSK

/********************  Bits definition for RTC_CR register  *******************/
#define RTC_CR_OSEL_POS               (21U)
#define RTC_CR_OSEL_MSK               (0x3UL << RTC_CR_OSEL_POS)                /*!< 0x00600000 */
#define RTC_CR_OSEL                   RTC_CR_OSEL_MSK
#define RTC_CR_OSEL_0                 (0x1UL << RTC_CR_OSEL_POS)                /*!< 0x00200000 */
#define RTC_CR_OSEL_1                 (0x2UL << RTC_CR_OSEL_POS)                /*!< 0x00400000 */
#define RTC_CR_POL_POS                (20U)
#define RTC_CR_POL_MSK                (0x1UL << RTC_CR_POL_POS)                 /*!< 0x00100000 */
#define RTC_CR_POL                    RTC_CR_POL_MSK
#define RTC_CR_BKP_POS                 (18U)
#define RTC_CR_BKP_MSK                 (0x1UL << RTC_CR_BKP_POS)                /*!< 0x00040000 */
#define RTC_CR_BKP                     RTC_CR_BKP_MSK
#define RTC_CR_FMT_POS                (6U)
#define RTC_CR_FMT_MSK                (0x1UL << RTC_CR_FMT_POS)                 /*!< 0x00000040 */
#define RTC_CR_FMT                    RTC_CR_FMT_MSK
#define RTC_CR_BYPSHAD_POS            (5U)
#define RTC_CR_BYPSHAD_MSK            (0x1UL << RTC_CR_BYPSHAD_POS)             /*!< 0x00000020 */
#define RTC_CR_BYPSHAD                RTC_CR_BYPSHAD_MSK


/* Legacy defines */
#define RTC_CR_BCK                     RTC_CR_BKP

/********************  Bits definition for RTC_ISR register  ******************/
#define RTC_ISR_INIT_POS              (7U)
#define RTC_ISR_INIT_MSK              (0x1UL << RTC_ISR_INIT_POS)               /*!< 0x00000080 */
#define RTC_ISR_INIT                  RTC_ISR_INIT_MSK
#define RTC_ISR_INITF_POS             (6U)
#define RTC_ISR_INITF_MSK             (0x1UL << RTC_ISR_INITF_POS)              /*!< 0x00000040 */
#define RTC_ISR_INITF                 RTC_ISR_INITF_MSK
#define RTC_ISR_RSF_POS               (5U)
#define RTC_ISR_RSF_MSK               (0x1UL << RTC_ISR_RSF_POS)                /*!< 0x00000020 */
#define RTC_ISR_RSF                   RTC_ISR_RSF_MSK
/********************  Bits definition for RTC_TAFCR register  ****************/
#define RTC_TAFCR_ALARMOUTTYPE_POS    (18U)
#define RTC_TAFCR_ALARMOUTTYPE_MSK    (0x1UL << RTC_TAFCR_ALARMOUTTYPE_POS)     /*!< 0x00040000 */
#define RTC_TAFCR_ALARMOUTTYPE        RTC_TAFCR_ALARMOUTTYPE_MSK
/* -----Macros ---------------------------------------------------------------------*/
#define RTC_LEAP_YEAR(year)             (((((year) % 4U) == 0U) && (((year) % 100U) != 0U)) || (((year) % 400U) == 0U))

/**
  * @brief  Disable the write protection for RTC registers.
  * @param  __HANDLE__ specifies the RTC handle.
  * @retval None
  */
#define HAL_RTC_WRITEPROTECTION_DISABLE(HANDLE)             \
                        do{                                       \
                            (HANDLE)->Instance->WPR = 0xCAU;  \
                            (HANDLE)->Instance->WPR = 0x53U;  \
                          } while(FALSE)

/**
  * @brief  Enable the write protection for RTC registers.
  * @param  __HANDLE__ specifies the RTC handle.
  * @retval None
  */
#define HAL_RTC_WRITEPROTECTION_ENABLE(HANDLE)     (HANDLE)->Instance->WPR = 0xFFU;

/*Macros to enable the RTC clock*/
#define HAL_RCC_RTC_ENABLE() (*(__IO uint32_t *) RCC_BDCR_RTCEN_BB = ENABLE)
/* -----Global variables -----------------------------------------------------------*/
/* RTC Handle */
RTC_HandleTypeDef hRTC = {0U};
static RTC_TimeDate_t stRTC_TimeDate_Default=
{
 .Hours   = 0x00U,
 .Minutes = 0x00U,
 .Seconds = 0x00U,
 .WeekDay = RTC_WEEKDAY_WEDNESDAY,
 .Day     = 1U,
 .Month   = 1U,
 .Year    = 20U
};


/* Days in a month */
static const uint8_t RTC_Months[2U][12U] = {
	{31U, 28U, 31U, 30U, 31U, 30U, 31U, 31U, 30U, 31U, 30U, 31U},	/* Not leap year */
	{31U, 29U, 31U, 30U, 31U, 30U, 31U, 31U, 30U, 31U, 30U, 31U}	/* Leap year */
};


/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
static HAL_StatusTypeDef hal_rtc_init(RTC_HandleTypeDef *hrtc);
static HAL_StatusTypeDef hal_rtc_set_time(RTC_HandleTypeDef *hrtc, RTC_TimeDate_t *time_str);
static HAL_StatusTypeDef hal_rtc_set_date(RTC_HandleTypeDef *hrtc, RTC_TimeDate_t *date_str);
static HAL_StatusTypeDef hal_rtc_wait_for_synchro(RTC_HandleTypeDef *hrtc);
static HAL_StatusTypeDef rtc_enter_init_mode(RTC_HandleTypeDef *hrtc);
static uint8_t rtc_byte_to_bcd2(uint8_t value);
static uint8_t rtc_bcd2_to_byte(uint8_t value);
static uint8_t get_weekday_from_date(RTC_TimeDate_t *RTC_DT);
/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** RTC_StartUp_Initialization - Initializes, configures and sets default Time/Date values to the RTC peripheral
**
** Params : None
**
** Returns: HAL_StatusTypeDef - HAL Status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef RTC_StartUp_Initialization(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Set instance */
	hRTC.Instance = RTC;
	hRTC.Init.AsynchPrediv = RTC_ASYNC_PREDIV;
	hRTC.Init.SynchPrediv = RTC_SYNC_PREDIV;
	hRTC.Init.HourFormat = RTC_HOURFORMAT_24;
	hRTC.Init.OutPut = RTC_OUTPUT_DISABLE;
	hRTC.Init.OutPutType = RTC_OUTPUT_TYPE_PUSHPULL;
	hRTC.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;



	/* Enable RTC Clock */
	HAL_RCC_RTC_ENABLE();

	/* Set Time and Date*/
	hal_status |= RTC_SetDateTime(&stRTC_TimeDate_Default );

	/* Init RTC */
	hal_status |= hal_rtc_init(&hRTC);
	return hal_status;
}
/*************************************************************************************
** RTC_SetDateTime - Sets Time and Date values to RTC registers
**
**
** Params : RTC_TimeDate_t *data -  data pointer to a RTC_TimeDate_t structure that contains
** 					                time/date parameters.
**
** Returns: HAL_StatusTypeDef -  Status of method execution
** Notes:
*************************************************************************************/
HAL_StatusTypeDef RTC_SetDateTime(RTC_TimeDate_t *data)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	RTC_TimeDate_t rtc_td_bcd = {0U};

	if(NULL != data)
	{
		data->WeekDay = get_weekday_from_date(data);


		/* Check input time and date values */
		if ((data->Year > 99U) ||\
				(data->Month == 0U) ||\
				(data->Month > 12U) ||\
				(data->Day == 0U) ||\
				(data->Day > (RTC_Months[(RTC_LEAP_YEAR(2000U + (data->Year)) ? 1U : 0U)][((data->Month) - 1U)])) ||\
				(data->Hours > 23U) ||\
				(data->Minutes > 59U) ||\
				(data->Seconds > 59U) ||\
				(data->WeekDay == 0U) ||\
				(data->WeekDay > 7U))
		{
			/* Invalid date */
			hal_status = HAL_ERROR;
		}
		else
		{
			rtc_td_bcd.Day     = rtc_byte_to_bcd2(data->Day);
			rtc_td_bcd.Month   = rtc_byte_to_bcd2(data->Month);
			rtc_td_bcd.Year    = rtc_byte_to_bcd2(data->Year);
			rtc_td_bcd.Hours   = rtc_byte_to_bcd2(data->Hours);
			rtc_td_bcd.Minutes = rtc_byte_to_bcd2(data->Minutes);
			rtc_td_bcd.Seconds = rtc_byte_to_bcd2(data->Seconds);
			rtc_td_bcd.WeekDay = rtc_byte_to_bcd2(data->WeekDay);

			/* Set time */
			hal_status = hal_rtc_set_time(&hRTC, &rtc_td_bcd);

			/* Set date in BDC format */
			hal_status |= hal_rtc_set_date(&hRTC, &rtc_td_bcd);

			/* Re-Init RTC with new values */
			hal_status |= hal_rtc_init(&hRTC);
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
** RTC_GetDateTime - Reads Time and Date values from RTC registers
**
**
** Params : RTC_TimeDate_t *data - Pointer to a RTC_TimeDate_t structure that contains
** 					time/date parameters.
**
** Returns: None
** Notes:
*************************************************************************************/
void RTC_GetDateTime(RTC_TimeDate_t *data)
{
	uint32_t time_tmpreg = 0U;
	uint32_t date_tmpreg = 0U;
	if(NULL != data)
	{
		/* Get Time from TR register */
		time_tmpreg = (uint32_t)(hRTC.Instance->TR & RTC_TR_RESERVED_MASK);

		/* Fill the structure fields with the read parameters */
		data->Hours = rtc_bcd2_to_byte((uint8_t)((time_tmpreg & (RTC_TR_HT | RTC_TR_HU)) >> 16U));
		data->Minutes = rtc_bcd2_to_byte((uint8_t)((time_tmpreg & (RTC_TR_MNT | RTC_TR_MNU)) >> 8U));
		data->Seconds = rtc_bcd2_to_byte((uint8_t)(time_tmpreg & (RTC_TR_ST | RTC_TR_SU)));


		/* Get Date from DR register */
		date_tmpreg = (uint32_t)(hRTC.Instance->DR & RTC_DR_RESERVED_MASK);

		/* Fill the structure fields with the read parameters */
		data->Year = rtc_bcd2_to_byte((uint8_t)((date_tmpreg & (RTC_DR_YT | RTC_DR_YU)) >> 16U));
		data->Month = rtc_bcd2_to_byte((uint8_t)((date_tmpreg & (RTC_DR_MT | RTC_DR_MU)) >> 8U));
		data->Day = rtc_bcd2_to_byte((uint8_t)(date_tmpreg & (RTC_DR_DT | RTC_DR_DU)));
		data->WeekDay = rtc_bcd2_to_byte((uint8_t)((date_tmpreg & (RTC_DR_WDU)) >> 13U));
	}
}
/*************************************************************************************
** hal_rtc_init - Initializes the RTC peripheral
**
**
** Params : RTC_HandleTypeDef *hrtc -  Pointer to a RTC_HandleTypeDef structure that contains
** 									   the configuration information for RTC.
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/

static HAL_StatusTypeDef hal_rtc_init(RTC_HandleTypeDef *hrtc)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	if(NULL != hrtc)
	{
		/* Disable the write protection for RTC registers */
		HAL_RTC_WRITEPROTECTION_DISABLE(hrtc);

		/* Set Initialization mode */
		if(HAL_OK != rtc_enter_init_mode(hrtc))
		{
			/*RTC Enter Init Mode failed*/
			/* Enable the write protection for RTC registers */
			HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
			hal_status = HAL_ERROR;
		}
		else
		{
			/*RTC Enter Init Mode succeeded*/

			/* Clear RTC_CR FMT, OSEL and POL Bits */
			hrtc->Instance->CR &= ((uint32_t)~(RTC_CR_FMT | RTC_CR_OSEL | RTC_CR_POL));
			/* Set RTC_CR register */
			hrtc->Instance->CR |= (uint32_t)(hrtc->Init.HourFormat | hrtc->Init.OutPut | hrtc->Init.OutPutPolarity);

			/* Configure the RTC PRER */
			hrtc->Instance->PRER = (uint32_t)(hrtc->Init.SynchPrediv);
			hrtc->Instance->PRER |= (uint32_t)(hrtc->Init.AsynchPrediv << 16U);

			/* Exit Initialization mode */
			hrtc->Instance->ISR &= (uint32_t)~RTC_ISR_INIT;

			/* If  CR_BYPSHAD bit = 0, wait for synchro else this check is not needed */
			if(RESET == (hrtc->Instance->CR & RTC_CR_BYPSHAD))
			{
				if(HAL_OK != hal_rtc_wait_for_synchro(hrtc))
				{
					/*RTC Synchronization failed*/
					/* Enable the write protection for RTC registers */
					HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
					hal_status = HAL_ERROR;
				}
			}
			if(HAL_OK == hal_status)
			{
				hrtc->Instance->TAFCR &= (uint32_t)~RTC_TAFCR_ALARMOUTTYPE;
				hrtc->Instance->TAFCR |= (uint32_t)(hrtc->Init.OutPutType);

				/* Enable the write protection for RTC registers */
				HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
			}
		}
	}

	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
** hal_rtc_set_time - Sets RTC current time
**
**
** Params : RTC_HandleTypeDef *hrtc - Pointer to a RTC_HandleTypeDef structure that contains
** 					                  the configuration information for RTC.
** 		    RTC_TimeDate_t *time_str - Pointer to Time structure
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef hal_rtc_set_time(RTC_HandleTypeDef *hrtc, RTC_TimeDate_t *time_str)
{
	uint32_t tmpreg = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if (NULL != time_str)
	{
		tmpreg = (((uint32_t)(time_str->Hours) << 16U) | \
				((uint32_t)(time_str->Minutes) << 8U) | \
				((uint32_t)time_str->Seconds) | \
				((uint32_t)(RTC_HOURFORMAT_24) << 16U));


		/* Disable the write protection for RTC registers */
		HAL_RTC_WRITEPROTECTION_DISABLE(hrtc);

		/* Set Initialization mode */
		if(HAL_OK != rtc_enter_init_mode(hrtc))
		{
			/*RTC Enter Init Mode failed*/
			/* Enable the write protection for RTC registers */
			HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
			hal_status = HAL_ERROR;
		}
		else
		{
			/*RTC Enter Init Mode succeeded*/

			/* Set the RTC_TR register */
			hrtc->Instance->TR = (uint32_t)(tmpreg & RTC_TR_RESERVED_MASK);

			/* Clear the bits to be configured */
			hrtc->Instance->CR &= (uint32_t)~RTC_CR_BCK;


			/* Exit Initialization mode */
			hrtc->Instance->ISR &= (uint32_t)~RTC_ISR_INIT;

			/* If  CR_BYPSHAD bit = 0, wait for synchro else this check is not needed */
			if(RESET == (hrtc->Instance->CR & RTC_CR_BYPSHAD))
			{
				if(HAL_OK != hal_rtc_wait_for_synchro(hrtc))
				{
					/*RTC Synchronization failed*/

					/* Enable the write protection for RTC registers */
					HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
					hal_status = HAL_ERROR;
				}
			}
			if(HAL_OK == hal_status)
			{
				/* Enable the write protection for RTC registers */
				HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
			}
		}
	}

	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}
/*************************************************************************************
** hal_rtc_set_date - Sets RTC current date.
**
**
** Params : RTC_HandleTypeDef *hrtc - Pointer to a RTC_HandleTypeDef structure that contains
** 					                  the configuration information for RTC.
** 		    RTC_TimeDate_t *date_str - Pointer to Date structure
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef hal_rtc_set_date(RTC_HandleTypeDef *hrtc, RTC_TimeDate_t *date_str)
{
	uint32_t datetmpreg = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if (NULL != date_str)
	{
		/*Fill data as BCD format*/

		datetmpreg = ((((uint32_t)date_str->Year) << 16U) | \
				(((uint32_t)date_str->Month) << 8U) | \
				((uint32_t)date_str->Day) | \
				(((uint32_t)date_str->WeekDay) << 13U));


		/* Disable the write protection for RTC registers */
		HAL_RTC_WRITEPROTECTION_DISABLE(hrtc);

		/* Set Initialization mode */
		if(HAL_OK != rtc_enter_init_mode(hrtc))
		{
			/*RTC Enter Init Mode failed*/
			/* Enable the write protection for RTC registers */
			HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
			hal_status =  HAL_ERROR;
		}
		else
		{
			/*RTC Enter Init Mode succeeded*/
			/* Set the RTC_DR register */
			hrtc->Instance->DR = (uint32_t)(datetmpreg & RTC_DR_RESERVED_MASK);

			/* Exit Initialization mode */
			hrtc->Instance->ISR &= (uint32_t)~RTC_ISR_INIT;

			/* If  CR_BYPSHAD bit = 0, wait for synchro else this check is not needed */
			if(RESET == (hrtc->Instance->CR & RTC_CR_BYPSHAD))
			{
				if(HAL_OK != hal_rtc_wait_for_synchro(hrtc))
				{
					/*RTC Synchronization failed*/
					/* Enable the write protection for RTC registers */
					HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
					hal_status =  HAL_ERROR;
				}
			}
			if(HAL_OK == hal_status)
			{
				/* Enable the write protection for RTC registers */
				HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
			}
		}
	}

	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}
/*************************************************************************************
** hal_rtc_wait_for_synchro -  Waits until the RTC Time and Date registers (RTC_TR and RTC_DR) are
**         synchronized with RTC APB clock.
**
** Params : RTC_HandleTypeDef *hrtc - Pointer to a RTC_HandleTypeDef structure that contains
**               the configuration information for RTC.
**
** Returns: HAL_StatusTypeDef - HAL status
** Notes:  The RTC Initialization mode is write protected, use the
**         __HAL_RTC_WRITEPROTECTION_DISABLE() before calling this function.
*************************************************************************************/
static HAL_StatusTypeDef hal_rtc_wait_for_synchro(RTC_HandleTypeDef *hrtc)
{
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;
	if (NULL != hrtc)
	{
		/* Clear RSF flag */
		hrtc->Instance->ISR &= (uint32_t)RTC_RSF_MASK;
		/* Get tick */
		tickstart = HAL_GetTick();
		/* Wait the registers to be synchronized */
		while(((uint32_t)RESET == (hrtc->Instance->ISR & RTC_ISR_RSF)) && (HAL_OK == hal_status))
		{
			if((HAL_GetTick() - tickstart ) > RTC_TIMEOUT_VALUE_MILIS)
			{
				hal_status =  HAL_TIMEOUT;
			}
		}
	}

	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}


/*************************************************************************************
** rtc_enter_init_mode - Enters the RTC Initialization mode
**
** Params : RTC_HandleTypeDef* hrtc - Pointer to a RTC_HandleTypeDef structure that contains
**               the configuration information for RTC.
**
** Returns: HAL_StatusTypeDef -  HAL status
** Notes:  The RTC Initialization mode is write protected, use the
**         __HAL_RTC_WRITEPROTECTION_DISABLE() before calling this function.
*************************************************************************************/
static HAL_StatusTypeDef rtc_enter_init_mode(RTC_HandleTypeDef *hrtc)
{
	uint32_t tickstart = 0U;
	HAL_StatusTypeDef hal_staus = HAL_OK;

	if(NULL != hrtc)
	{
		/* Check if the Initialization mode is set */
		if((hrtc->Instance->ISR & RTC_ISR_INITF) == (uint32_t)RESET)
		{
			/* Set the Initialization mode */
			hrtc->Instance->ISR = (uint32_t)RTC_INIT_MASK;

			/* Get tick */
			tickstart = HAL_GetTick();

			/* Wait till RTC is in INIT state and if Time out is reached exit */
			while(((uint32_t)RESET == (hrtc->Instance->ISR & RTC_ISR_INITF)) && (HAL_OK == hal_staus))
			{
				if((HAL_GetTick() - tickstart ) > RTC_TIMEOUT_VALUE_MILIS)
				{
					hal_staus =  HAL_TIMEOUT;
				}
			}
		}
	}

	else
	{
		hal_staus = HAL_ERROR;
	}

	return hal_staus;
}

/*************************************************************************************
** rtc_byte_to_bcd2 - Converts a 2 digit decimal to BCD format
**
** Params : uint8_t value - Byte to be converted
**
** Returns: uint8_t - Converted byte in BCD format
** Notes:
*************************************************************************************/
static uint8_t rtc_byte_to_bcd2(uint8_t value)
{
	uint32_t bcdhigh = 0U;

	while(value >= 10U)
	{
		bcdhigh++;
		value -= 10U;
	}

	return  ((uint8_t)(bcdhigh << 4U) | value);
}

/*************************************************************************************
** rtc_bcd2_to_byte - Converts from 2 digit BCD to Binary
**
** Params : uint8_t value -  BCD value to be converted
**
** Returns: uint8_t - Convention result
** Notes:
*************************************************************************************/
static uint8_t rtc_bcd2_to_byte(uint8_t value)
{
	uint8_t tmp = 0U;	/* LDRA: type changed to uint8_t, was uint32_t */
	tmp = ((uint8_t)(value & (uint8_t)0xF0) >> (uint8_t)0x4) * 10U;
	return (tmp + (value & (uint8_t)0x0F));
}

/*************************************************************************************
** get_weekday_from_date - Calculation day of the week
**
** Params : RTC_TimeDate_t *RTC_DT -  Pointer to TimeDate structure
**
** Returns: uint8_t -  week-day value
** Notes:
*************************************************************************************/
/*Calculation day of the week using Zeller's rule.
 * Using the formula: f = k + [(13*m-1)/5] + D + [D/4] + [C/4] - 2*C
 *
 * k is the day of the month.
 * m is the month number. Months have to be counted specially for Zeller's Rule: March is 1, April is 2, and so on to February, which is 12.
 * (This makes the formula simpler, because on leap years February 29 is counted as the last day of the year.)
 * Because of this rule, January and February are always counted as the 11th and 12th months of the previous year.
 * D is the last two digits of the year. If the month is January or February subtract a year from D.
 * C stands for century: it's the first two digits of the year.
 * if f < 7 then use that day number if f > 7 then divide by 7 and use the remainder as the day in other words use the modulo operator.
 *
 * days are numbered:
 *
 * 0- Sunday
 * 1- Monday
 * 2- Tuesday
 * 3- Wednesday
 * 4- Thursday
 * 5- Friday
 * 6- Saturday
 */
static uint8_t get_weekday_from_date(RTC_TimeDate_t *RTC_DT)
{
	uint8_t century = 20U;
	uint8_t month = RTC_DT->Month;
	uint8_t year = RTC_DT->Year;
	uint8_t tmp = 0U;
	uint8_t weekday = 0U;

	if(NULL != RTC_DT)
	{
		if(month > 2U)
		{
			month -=2U;
		}
		else
		{
			month+=10U;/*For JAN/FEB*/
			year--;
		}
		tmp = ((RTC_DT->Day) + (((13U*month)-1U)/5U) + year + (year/4U) + (century/4U) - (2U*century));
		switch(tmp % 7U)
		{
		case 0U:
			weekday = RTC_WEEKDAY_SUNDAY;
			break;
		case 1U:
			weekday = RTC_WEEKDAY_MONDAY;
			break;
		case 2U:
			weekday = RTC_WEEKDAY_TUESDAY;
			break;
		case 3U:
			weekday = RTC_WEEKDAY_WEDNESDAY;
			break;
		case 4U:
			weekday = RTC_WEEKDAY_THURSDAY;
			break;
		case 5U:
			weekday = RTC_WEEKDAY_FRIDAY;
			break;
		case 6U:
			weekday = RTC_WEEKDAY_SATURDAY;
			break;
		default:
			weekday = 0U;/*Error in weekday calculation*/
			break;

		}
	}
	return weekday;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
