/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : gipsy_hal_gpio.c
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 22-11-2020
**
** Description: This file provides firmware functions to manage the following
**          	functionalities of the General Purpose Input/Output (GPIO) peripheral:
**           		+ Initialization
**           		+ IO operation functions
**
** Notes:
**
*/

/* Revision Log:
**
** Rev 1.0  : 21-01-2020 -- Original version created.
** Rev 1.1  : 29-09-2020 -- HAL_GPIO_Init()- removed both 'else's
** Rev 1.2  : 05-11-2020 -- Added GPIO Registers definitions.
** Rev 1.3  : 20-11-2020 -- Static analysis update
** Rev 1.4  : 22-11-2020 -- Updated due to verification remarks
**************************************************************************************
*/

/*Subject to the specific hardware characteristics of each I/O port listed in the data sheet, each
  port bit of the General Purpose IO (GPIO) Ports, can be individually configured by software
  in several modes:
  (+) Input mode 
  (+) Analog mode
  (+) Output mode
  (+) Alternate function mode
  (+) External interrupt/event lines

  During and just after reset, the alternate functions and external interrupt  
  lines are not active and the I/O ports are configured in input floating mode.

  All GPIO pins have weak internal pull-up and pull-down resistors, which can be 
  activated or not.

  In Output or Alternate mode, each IO can be configured on open-drain or push-pull
  type and the IO speed can be selected depending on the VDD value.
  

  (#) Configure the GPIO pin(s) using HAL_GPIO_Init().
	(++) Configure the IO mode using "Mode" member from GPIO_InitTypeDef structure
	(++) Activate Pull-up, Pull-down resistor using "Pull" member from GPIO_InitTypeDef
		 structure.
	(++) In case of Output or alternate function mode selection: the speed is
		 configured through "Speed" member from GPIO_InitTypeDef structure.
	(++) In alternate mode is selection, the alternate function connected to the IO
		 is configured through "Alternate" member from GPIO_InitTypeDef structure.
	(++) Analog mode is required when a pin is to be used as ADC channel
		 or DAC output.
	(++) In case of external interrupt/event selection the "Mode" member from
		 GPIO_InitTypeDef structure select the type (interrupt or event) and
		 the corresponding trigger event (rising or falling or both).

	(#) To get the level of a pin configured in input mode use HAL_GPIO_ReadPin().
            
    (#) To set/reset the level of a pin configured in output mode use 
        HAL_GPIO_WritePin()/HAL_GPIO_TogglePin().
*/



/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_gpio.h"
/* -----Definitions ----------------------------------------------------------------*/

/*GPIO Private Constants*/
#define GPIO_MODE             0x00000003U /*Mask for GPIO Mode*/
#define GPIO_OUTPUT_TYPE      0x00000010U /*Mask for GPIO type*/
#define GPIO_PIN_MASK         0x0000FFFFU /* PIN mask for assert test */
#define GPIO_PIN_MASK_INV     0xFFFF0000U /* PIN mask inverted for assert test */
#define GPIO_NUMBER           16U /*Number of GPIO pins in Port*/

/******************  Bits definition for GPIO_MODER register  *****************/
#define GPIO_MODER_MODER0_POS            (0U)
#define GPIO_MODER_MODER0_MSK            (0x3UL << GPIO_MODER_MODER0_POS)       /*!< 0x00000003 */
#define GPIO_MODER_MODER0                GPIO_MODER_MODER0_MSK
#define GPIO_MODER_MODE0_POS             GPIO_MODER_MODER0_POS
#define GPIO_MODER_MODE0_MSK             GPIO_MODER_MODER0_MSK
#define GPIO_MODER_MODE0                 GPIO_MODER_MODER0


/******************  Bits definition for GPIO_OTYPER register  ****************/
#define GPIO_OTYPER_OT0_POS              (0U)
#define GPIO_OTYPER_OT0_MSK              (0x1UL << GPIO_OTYPER_OT0_POS)         /*!< 0x00000001 */
#define GPIO_OTYPER_OT0                  GPIO_OTYPER_OT0_MSK
#define GPIO_OTYPER_OT_0                 GPIO_OTYPER_OT0


/******************  Bits definition for GPIO_OSPEEDR register  ***************/
#define GPIO_OSPEEDR_OSPEED0_POS         (0U)
#define GPIO_OSPEEDR_OSPEED0_MSK         (0x3UL << GPIO_OSPEEDR_OSPEED0_POS)    /*!< 0x00000003 */
#define GPIO_OSPEEDR_OSPEED0             GPIO_OSPEEDR_OSPEED0_MSK
#define GPIO_OSPEEDER_OSPEEDR0           GPIO_OSPEEDR_OSPEED0


/******************  Bits definition for GPIO_PUPDR register  *****************/
#define GPIO_PUPDR_PUPD0_POS             (0U)
#define GPIO_PUPDR_PUPD0_MSK             (0x3U << GPIO_PUPDR_PUPD0_POS)        /*!< 0x00000003 */
#define GPIO_PUPDR_PUPD0                 GPIO_PUPDR_PUPD0_MSK
#define GPIO_PUPDR_PUPDR0                GPIO_PUPDR_PUPD0
/* -----Macros ---------------------------------------------------------------------*/
/*Macros for input parameters testing*/
#define IS_GPIO_PIN_ACTION(ACTION) (((ACTION) == E_GPIO_PIN_RESET) || ((ACTION) == E_GPIO_PIN_SET))
#define IS_GPIO_PIN(PIN)           ((((uint32_t)(PIN) & (uint32_t)GPIO_PIN_MASK ) != 0x00000000U) && (((uint32_t)(PIN) & (uint32_t)GPIO_PIN_MASK_INV) == 0x00000000U))
#define IS_GPIO_PORT(PORT)        ((GPIOA == (PORT)) || (GPIOB == (PORT)) || (GPIOC == (PORT)) ||\
									(GPIOD == (PORT)) || (GPIOE == (PORT)))
#define IS_GPIO_MODE(MODE) (((MODE) == GPIO_MODE_INPUT)              ||\
                            ((MODE) == GPIO_MODE_OUTPUT_PP)          ||\
                            ((MODE) == GPIO_MODE_OUTPUT_OD)          ||\
                            ((MODE) == GPIO_MODE_AF_PP)              ||\
                            ((MODE) == GPIO_MODE_AF_OD)              ||\
                            ((MODE) == GPIO_MODE_ANALOG))
#define IS_GPIO_SPEED(SPEED) (((SPEED) == GPIO_SPEED_FREQ_LOW)  || ((SPEED) == GPIO_SPEED_FREQ_MEDIUM) || \
                              ((SPEED) == GPIO_SPEED_FREQ_HIGH) || ((SPEED) == GPIO_SPEED_FREQ_VERY_HIGH))
#define IS_GPIO_PULL(PULL) (((PULL) == GPIO_NOPULL) || ((PULL) == GPIO_PULLUP) || \
                            ((PULL) == GPIO_PULLDOWN))

/*GPIO Check Alternate Function parameter*/
#define IS_GPIO_AF(AF)   (((AF) == GPIO_AF0_MCO)        || \
                          ((AF) == GPIO_AF1_TIM1)       || ((AF) == GPIO_AF1_TIM2)       || \
                          ((AF) == GPIO_AF2_TIM3)       || ((AF) == GPIO_AF2_TIM4)       || \
                          ((AF) == GPIO_AF2_TIM5)       || ((AF) == GPIO_AF3_TIM8)       || \
                          ((AF) == GPIO_AF4_I2C1)       || ((AF) == GPIO_AF4_I2C2)       || \
                          ((AF) == GPIO_AF4_I2C3)       || ((AF) == GPIO_AF5_SPI1)       || \
                          ((AF) == GPIO_AF5_SPI2)       || ((AF) == GPIO_AF9_TIM13)      || \
                          ((AF) == GPIO_AF9_TIM12)      || ((AF) == GPIO_AF7_USART1)     || \
                          ((AF) == GPIO_AF7_USART3)     || ((AF) == GPIO_AF9_CAN1)       || \
                          ((AF) == GPIO_AF5_SPI5)       || ((AF) == GPIO_AF5_SPI6) )

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
**
** HAL_GPIO_Init - Initializes the GPIOx peripheral according to the specified parameters in the GPIO_Init.
**
**
** Params : GPIO_TypeDef  *GPIOx - GPIOx where x can be (A..E) to select the GPIO peripheral
**          GPIO_InitTypeDef *GPIO_Init - GPIO_Init pointer to a GPIO_InitTypeDef structure that contains
**									      the configuration information for the specified GPIO peripheral.
** Returns: HAL_StatusTypeDef  - init status
** Notes:
**
*************************************************************************************/
HAL_StatusTypeDef HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init)
{
	uint32_t position   = 0x00U;
	uint32_t ioposition = 0x00U;
	uint32_t iocurrent  = 0x00U;
	uint32_t temp       = 0x00U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Check input parameters */
	if((NULL != GPIOx) && (NULL != GPIO_Init))
	{
		if((TRUE == IS_GPIO_PORT(GPIOx)) && (TRUE == IS_GPIO_PIN(GPIO_Init->Pin)) &&\
				(TRUE == IS_GPIO_MODE(GPIO_Init->Mode)) && (TRUE == IS_GPIO_PULL(GPIO_Init->Pull)))
		{

			/* Configure the port pins */
			for(position = 0U; position < GPIO_NUMBER; position++)
			{
				/* Get the IO position */
				ioposition = 0x01U << position;
				/* Get the current IO position */
				iocurrent = (uint32_t)(GPIO_Init->Pin) & ioposition;

				if(iocurrent == ioposition)
				{
					/*--------------------- GPIO Mode Configuration ------------------------*/
					/* In case of Alternate function mode selection and Check the Alternate function parameter*/
					if(((GPIO_MODE_AF_PP == GPIO_Init->Mode) || (GPIO_MODE_AF_OD == GPIO_Init->Mode)) && (TRUE == IS_GPIO_AF(GPIO_Init->Alternate)))
					{
						/* Configure Alternate function mapped with the current IO */
						temp = GPIOx->AFR[position >> 3U];
						temp &= ~(0xFU << ((uint32_t)(position & 0x07U) * 4U)) ;
						temp |= ((uint32_t)(GPIO_Init->Alternate) << (((uint32_t)position & 0x07U) * 4U));
						GPIOx->AFR[position >> 3U] = temp;

					}


					/* Configure IO Direction mode (Input, Output, Alternate or Analog) */
					temp = GPIOx->MODER;
					temp &= ~(GPIO_MODER_MODER0 << (position * 2U));
					temp |= ((GPIO_Init->Mode & GPIO_MODE) << (position * 2U));
					GPIOx->MODER = temp;

					/* In case of Output or Alternate function mode selection and Check the Speed parameter */
					if(((GPIO_MODE_OUTPUT_PP == GPIO_Init->Mode) || (GPIO_MODE_AF_PP == GPIO_Init->Mode) ||
							(GPIO_MODE_OUTPUT_OD == GPIO_Init->Mode) || (GPIO_MODE_AF_OD == GPIO_Init->Mode)) && (TRUE == IS_GPIO_SPEED(GPIO_Init->Speed)))
					{

						/* Configure the IO Speed */
						temp = GPIOx->OSPEEDR;
						temp &= ~(GPIO_OSPEEDER_OSPEEDR0 << (position * 2U));
						temp |= (GPIO_Init->Speed << (position * 2U));
						GPIOx->OSPEEDR = temp;


						/* Configure the IO Output Type */
						temp = GPIOx->OTYPER;
						temp &= ~(GPIO_OTYPER_OT_0 << position) ;
						temp |= (((GPIO_Init->Mode & GPIO_OUTPUT_TYPE) >> 4U) << position);
						GPIOx->OTYPER = temp;
					}


					/* Activate the Pull-up or Pull down resistor for the current IO */
					temp = GPIOx->PUPDR;
					temp &= ~(GPIO_PUPDR_PUPDR0 << (position * 2U));
					temp |= ((GPIO_Init->Pull) << (position * 2U));
					GPIOx->PUPDR = temp;
				}
			}
		}
		else
		{
			hal_status = HAL_ERROR;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
**
** HAL_GPIO_ReadPin - Reads the specified input port pin.
**
**
** Params : GPIO_TypeDef *GPIOx - GPIOx where x can be (A..E) to select the GPIO peripheral
** 			uint16_t GPIO_Pin - GPIO_Pin specifies the port bit to read.
**                              This parameter can be GPIO_PIN_x where x can be (0..15).
**
** Returns: GPIO_PinState - The input port pin value.
** Notes:
**
*************************************************************************************/
GPIO_PinState HAL_GPIO_ReadPin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_PinState bitstatus = E_GPIO_PIN_RESET;
	if(NULL != GPIOx)
	{
		if((TRUE == IS_GPIO_PIN(GPIO_Pin))  && (TRUE == IS_GPIO_PORT(GPIOx)))
		{
			if((GPIOx->IDR & (uint32_t)GPIO_Pin) != 0U)
			{
				bitstatus = E_GPIO_PIN_SET;
			}
			else
			{
				bitstatus = E_GPIO_PIN_RESET;
			}
		}
	}
	return bitstatus;
}
/*************************************************************************************
**
** HAL_GPIO_WritePin - Sets or clears the selected data port bit.
**
**
** Params : GPIO_TypeDef *GPIOx - GPIOx where x can be (A..E) to select the GPIO peripheral
** 			uint16_t GPIO_Pin - GPIO_Pin specifies the port bit to read.
**                              This parameter can be GPIO_PIN_x where x can be (0..15).
**          GPIO_PinState PinState - PinState specifies the value to be written to the selected bit.
**          						 This parameter can be one of the GPIO_PinState enum values:
**            						 #E_GPIO_PIN_RESET: to clear the port pin
**            						 #E_GPIO_PIN_SET: to set the port pin
**
** Returns: None
** Notes:This function uses GPIOx_BSRR register to allow atomic read/modify
** 		 accesses. In this way, there is no risk of an IRQ occurring between
**       the read and the modify access.
**
*************************************************************************************/
void HAL_GPIO_WritePin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
	/* Check input parameters */
	if(NULL != GPIOx)
	{
		if((TRUE == IS_GPIO_PIN(GPIO_Pin)) && (TRUE == IS_GPIO_PIN_ACTION(PinState)) && (TRUE == IS_GPIO_PORT(GPIOx)))
		{
			if(E_GPIO_PIN_SET == PinState)
			{
				GPIOx->BSRR = (uint32_t)GPIO_Pin;
			}
			else
			{
				GPIOx->BSRR = (uint32_t)GPIO_Pin << 16U;
			}
		}
	}
}
/*************************************************************************************
**
** HAL_GPIO_TogglePin - Toggles the specified GPIO pin
**
**
** Params : GPIO_TypeDef *GPIOx - GPIOx where x can be (A..I) to select the GPIO peripheral
** 			uint16_t GPIO_Pin - GPIO_Pin specifies the port bit to be toggled.
**                              This parameter can be GPIO_PIN_x where x can be (0..15).
**
** Returns: None
** Notes:
*************************************************************************************/
void HAL_GPIO_TogglePin(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	/* Check the parameters */
	if(NULL != GPIOx)
	{
		if((TRUE == IS_GPIO_PIN(GPIO_Pin)) && (TRUE == IS_GPIO_PORT(GPIOx)))
		{
			GPIOx->ODR ^= (uint32_t)GPIO_Pin;
		}
	}
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
