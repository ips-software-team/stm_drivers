/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gipsy_hal_i2c_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 28-06-2021
**
** Description: This file contains procedures for STM32 I2C interface.
*/


/* Revision Log:
**
** Rev 1.0  : 12-07-2020 -- Original version created.
** Rev 1.1  : 05-11-2020 -- Added defines
** Rev 1.2  : 20-11-2020 -- Static analysis update
** Rev 1.3  : 25-11-2020 -- Updated transmit function : added Stop condition demand as input parameter
** Rev 1.4  : 28-06-2021 -- Updated transmit function : removed Stop condition demand as input parameter
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_i2c_ctry.h"
#include "gipsy_hal_rcc.h"
/* -----Definitions ----------------------------------------------------------------*/

#define I2C_TIMEOUT_BUSY_FLAG     25U         /*Timeout 25 ms             */
#define I2C_NO_OPTION_FRAME       0xFFFF0000U /*XferOptions default value */
/*******************  Bit definition for I2C_CR1 register  ********************/
#define I2C_CR1_PE_POS            (0U)
#define I2C_CR1_PE_MSK            (0x1U << I2C_CR1_PE_POS)                     /*!< 0x00000001 */
#define I2C_CR1_PE                I2C_CR1_PE_MSK                               /*!<Peripheral Enable                             */
#define I2C_CR1_START_POS         (8U)
#define I2C_CR1_START_MSK         (0x1U << I2C_CR1_START_POS)                  /*!< 0x00000100 */
#define I2C_CR1_START             I2C_CR1_START_MSK                            /*!<Start Generation                              */
#define I2C_CR1_STOP_POS          (9U)
#define I2C_CR1_STOP_MSK          (0x1U << I2C_CR1_STOP_POS)                   /*!< 0x00000200 */
#define I2C_CR1_STOP              I2C_CR1_STOP_MSK                             /*!<Stop Generation                               */
#define I2C_CR1_ACK_POS           (10U)
#define I2C_CR1_ACK_MSK           (0x1U << I2C_CR1_ACK_POS)                    /*!< 0x00000400 */
#define I2C_CR1_ACK               I2C_CR1_ACK_MSK                              /*!<Acknowledge Enable                            */
#define I2C_CR1_POS_POS           (11U)
#define I2C_CR1_POS_MSK           (0x1U << I2C_CR1_POS_POS)                    /*!< 0x00000800 */
#define I2C_CR1_POS               I2C_CR1_POS_MSK                              /*!<Acknowledge/PEC POSition (for data reception) */


/*******************  Bit definition for I2C_OAR1 register  *******************/
#define I2C_OAR1_ADD0_POS         (0U)
#define I2C_OAR1_ADD0_MSK         (0x1U << I2C_OAR1_ADD0_POS)                  /*!< 0x00000001 */
#define I2C_OAR1_ADD0             I2C_OAR1_ADD0_MSK                            /*!<Bit 0 */


/*******************  Bit definition for I2C_CCR register  ********************/
#define I2C_CCR_CCR_POS           (0U)
#define I2C_CCR_CCR_MSK           (0xFFFU << I2C_CCR_CCR_POS)                  /*!< 0x00000FFF */
#define I2C_CCR_CCR               I2C_CCR_CCR_MSK                              /*!<Clock Control Register in Fast/Standard mode (Master mode) */
#define I2C_CCR_DUTY_POS          (14U)
#define I2C_CCR_DUTY_MSK          (0x1U << I2C_CCR_DUTY_POS)                   /*!< 0x00004000 */
#define I2C_CCR_DUTY              I2C_CCR_DUTY_MSK                             /*!<Fast Mode Duty Cycle                                       */
#define I2C_CCR_FS_POS            (15U)
#define I2C_CCR_FS_MSK            (0x1U << I2C_CCR_FS_POS)                     /*!< 0x00008000 */
#define I2C_CCR_FS                I2C_CCR_FS_MSK                               /*!<I2C Master Mode Selection                                  */


#define I2C_STATE_MSK             ((uint32_t)((HAL_I2C_STATE_BUSY_TX | HAL_I2C_STATE_BUSY_RX) & (~(uint32_t)HAL_I2C_STATE_READY))) /*!< Mask State define, keep only RX and TX bits            */
#define I2C_STATE_NONE            ((uint32_t)(HAL_I2C_MODE_NONE))                                                        /*!< Default Value                                          */
#define I2C_STATE_MASTER_BUSY_TX  ((uint32_t)((HAL_I2C_STATE_BUSY_TX & I2C_STATE_MSK) | HAL_I2C_MODE_MASTER))            /*!< Master Busy TX, combinaison of State LSB and Mode enum */
#define I2C_STATE_MASTER_BUSY_RX  ((uint32_t)((HAL_I2C_STATE_BUSY_RX & I2C_STATE_MSK) | HAL_I2C_MODE_MASTER))            /*!< Master Busy RX, combinaison of State LSB and Mode enum */



/*I2C_Flag_definition I2C Flag definition*/
#define I2C_FLAG_SMBALERT               0x00018000U
#define I2C_FLAG_TIMEOUT                0x00014000U
#define I2C_FLAG_PECERR                 0x00011000U
#define I2C_FLAG_OVR                    0x00010800U
#define I2C_FLAG_AF                     0x00010400U
#define I2C_FLAG_ARLO                   0x00010200U
#define I2C_FLAG_BERR                   0x00010100U
#define I2C_FLAG_TXE                    0x00010080U
#define I2C_FLAG_RXNE                   0x00010040U
#define I2C_FLAG_STOPF                  0x00010010U
#define I2C_FLAG_ADD10                  0x00010008U
#define I2C_FLAG_BTF                    0x00010004U
#define I2C_FLAG_ADDR                   0x00010002U
#define I2C_FLAG_SB                     0x00010001U
#define I2C_FLAG_DUALF                  0x00100080U
#define I2C_FLAG_SMBHOST                0x00100040U
#define I2C_FLAG_SMBDEFAULT             0x00100020U
#define I2C_FLAG_GENCALL                0x00100010U
#define I2C_FLAG_TRA                    0x00100004U
#define I2C_FLAG_BUSY                   0x00100002U
#define I2C_FLAG_MSL                    0x00100001U

#define I2C_FLAG_MASK  0x0000FFFFU

/*I2C_Error_Code I2C Error Code*/
#define HAL_I2C_ERROR_NONE       0x00000000U    /*!< No error           */
#define HAL_I2C_ERROR_BERR       0x00000001U    /*!< BERR error         */
#define HAL_I2C_ERROR_ARLO       0x00000002U    /*!< ARLO error         */
#define HAL_I2C_ERROR_AF         0x00000004U    /*!< AF error           */
#define HAL_I2C_ERROR_OVR        0x00000008U    /*!< OVR error          */
#define HAL_I2C_ERROR_TIMEOUT    0x00000020U    /*!< Timeout Error      */

/*I2C XferOptions definition*/
#define  I2C_FIRST_FRAME                0x00000001U
#define  I2C_NEXT_FRAME                 0x00000002U
#define  I2C_FIRST_AND_LAST_FRAME       0x00000004U
#define  I2C_LAST_FRAME                 0x00000008U

/*I2C_duty_cycle_in_fast_mode I2C duty cycle in fast mode*/
#define I2C_DUTYCYCLE_2                 0x00000000U
#define I2C_DUTYCYCLE_16_9              I2C_CCR_DUTY



/* -----Macros ---------------------------------------------------------------------*/
#define I2C_FREQRANGE(_PCLK_)           ((_PCLK_)/1000000U)

#define IS_I2C_MEMADD_SIZE(SIZE) (((SIZE) == I2C_MEMADD_SIZE_8BIT) || \
                                  ((SIZE) == I2C_MEMADD_SIZE_16BIT))

#define IS_I2C_FLAG(FLAG)      (((FLAG) == I2C_FLAG_SMBALERT) || \
							    ((FLAG) == I2C_FLAG_TIMEOUT)  || \
							    ((FLAG) == I2C_FLAG_PECERR) || \
							    ((FLAG) == I2C_FLAG_OVR)  || \
							    ((FLAG) == I2C_FLAG_AF) || \
							    ((FLAG) == I2C_FLAG_ARLO)  || \
							    ((FLAG) == I2C_FLAG_BERR) || \
							    ((FLAG) == I2C_FLAG_TXE)  || \
							    ((FLAG) == I2C_FLAG_RXNE) || \
							    ((FLAG) == I2C_FLAG_STOPF)  || \
							    ((FLAG) == I2C_FLAG_ADD10) || \
							    ((FLAG) == I2C_FLAG_BTF)  || \
							    ((FLAG) == I2C_FLAG_ADDR) || \
							    ((FLAG) == I2C_FLAG_SB)  || \
							    ((FLAG) == I2C_FLAG_DUALF) || \
							    ((FLAG) == I2C_FLAG_SMBHOST) || \
							    ((FLAG) == I2C_FLAG_SMBDEFAULT) || \
							    ((FLAG) == I2C_FLAG_GENCALL) || \
							    ((FLAG) == I2C_FLAG_TRA) || \
							    ((FLAG) == I2C_FLAG_BUSY) || \
							    ((FLAG) == I2C_FLAG_MSL))

#define I2C_RISE_TIME(_FREQRANGE_, _SPEED_)            (((_SPEED_) <= 100000U) ? ((_FREQRANGE_) + 1U) : ((((_FREQRANGE_) * 300U) / 1000U) + 1U))

#define I2C_SPEED_STANDARD(_PCLK_, _SPEED_)            (((((_PCLK_)/((_SPEED_) << 1U)) & I2C_CCR_CCR) < 4U)? 4U:((_PCLK_) / ((_SPEED_) << 1U)))

#define I2C_SPEED_FAST(_PCLK_, _SPEED_, _DUTYCYCLE_)   ((_PCLK_) / ((_SPEED_) * 3U))

#define I2C_SPEED(_PCLK_, _SPEED_, _DUTYCYCLE_)      (uint32_t)(((_SPEED_) <= 100000U)? (I2C_SPEED_STANDARD((_PCLK_), (_SPEED_))) : \
                                                                  ((I2C_SPEED_FAST((_PCLK_), (_SPEED_), (_DUTYCYCLE_)) & I2C_CCR_CCR) == 0U)? 1U : \
                                                                  ((I2C_SPEED_FAST((_PCLK_), (_SPEED_), (_DUTYCYCLE_))) | I2C_CCR_FS))

/** @brief  Checks whether the specified I2C flag is set or not.
  * @param  _HANDLE_ specifies the I2C Handle.
  *         This parameter can be I2C where x: 1, 2, or 3 to select the I2C peripheral.
  * @param  _FLAG_ specifies the flag to check.
  *         This parameter can be one of the following values:
  *            @arg I2C_FLAG_SMBALERT: SMBus Alert flag
  *            @arg I2C_FLAG_TIMEOUT: Timeout or Tlow error flag
  *            @arg I2C_FLAG_PECERR: PEC error in reception flag
  *            @arg I2C_FLAG_OVR: Overrun/Underrun flag
  *            @arg I2C_FLAG_AF: Acknowledge failure flag
  *            @arg I2C_FLAG_ARLO: Arbitration lost flag
  *            @arg I2C_FLAG_BERR: Bus error flag
  *            @arg I2C_FLAG_TXE: Data register empty flag
  *            @arg I2C_FLAG_RXNE: Data register not empty flag
  *            @arg I2C_FLAG_STOPF: Stop detection flag
  *            @arg I2C_FLAG_ADD10: 10-bit header sent flag
  *            @arg I2C_FLAG_BTF: Byte transfer finished flag
  *            @arg I2C_FLAG_ADDR: Address sent flag
  *                                Address matched flag
  *            @arg I2C_FLAG_SB: Start bit flag
  *            @arg I2C_FLAG_DUALF: Dual flag
  *            @arg I2C_FLAG_SMBHOST: SMBus host header
  *            @arg I2C_FLAG_SMBDEFAULT: SMBus default header
  *            @arg I2C_FLAG_GENCALL: General call header flag
  *            @arg I2C_FLAG_TRA: Transmitter/Receiver flag
  *            @arg I2C_FLAG_BUSY: Bus busy flag
  *            @arg I2C_FLAG_MSL: Master/Slave flag
  * @retval The new state of _FLAG_ (TRUE or FALSE).
  */
#define HAL_I2C_GET_FLAG(_HANDLE_, _FLAG_) ((((uint8_t)((_FLAG_) >> 16U)) == 0x01U)?((((_HANDLE_)->Instance->SR1) & ((_FLAG_) & I2C_FLAG_MASK)) == ((_FLAG_) & I2C_FLAG_MASK)): \
                                                 ((((_HANDLE_)->Instance->SR2) & ((_FLAG_) & I2C_FLAG_MASK)) == ((_FLAG_) & I2C_FLAG_MASK)))



/** @brief  Clears the I2C ADDR pending flag.
  * @param  _HANDLE_ specifies the I2C Handle.
  *         This parameter can be I2C where x: 1, 2, or 3 to select the I2C peripheral.
  * @retval None
  */
#define HAL_I2C_CLEAR_ADDRFLAG(_HANDLE_)    \
  do{                                           \
    volatile uint32_t tmpreg = 0x00U;               \
    tmpreg = (_HANDLE_)->Instance->SR1;       \
    tmpreg = (_HANDLE_)->Instance->SR2;       \
    UNUSED(tmpreg);                             \
  } while(0U)

#define I2C_MEM_ADD_MSB(_ADDRESS_)                       ((uint8_t)((uint16_t)(((uint16_t)((_ADDRESS_) & (uint16_t)0xFF00)) >> 8)))
#define I2C_MEM_ADD_LSB(_ADDRESS_)                       ((uint8_t)((uint16_t)((_ADDRESS_) & (uint16_t)0x00FF)))

#define I2C_7BIT_ADD_WRITE(_ADDRESS_)                    ((uint8_t)((_ADDRESS_) & (~I2C_OAR1_ADD0)))
#define I2C_7BIT_ADD_READ(_ADDRESS_)                     ((uint8_t)((_ADDRESS_) | I2C_OAR1_ADD0))

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/


static HAL_StatusTypeDef i2c_wait_on_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t flag, uint8_t req_flag_state, uint32_t timeout, uint32_t tick_start);
static HAL_StatusTypeDef i2c_wait_on_master_address_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t flag, uint32_t timeout, uint32_t tick_start);
static HAL_StatusTypeDef i2c_wait_on_txe_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t timeout, uint32_t tick_start);
static HAL_StatusTypeDef i2c_wait_on_btf_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t timeout, uint32_t tick_start);
static HAL_StatusTypeDef i2c_wait_on_rxne_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t timeout, uint32_t tick_start);
static HAL_StatusTypeDef i2c_is_acknowledge_failed(I2C_HandleTypeDef *hi2c);
static HAL_StatusTypeDef i2c_master_request_write(I2C_HandleTypeDef *hi2c, uint16_t dev_address, uint32_t timeout, uint32_t tick_start);
static HAL_StatusTypeDef i2c_master_request_read(I2C_HandleTypeDef *hi2c, uint16_t dev_address, uint32_t timeout, uint32_t tick_start);

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** HAL_I2C_Init - Initializes the I2C according to the specified parameters
**                 in the I2C_InitTypeDef and create the associated handle.
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains
** 										the configuration information for I2C module
**
** Returns: None.
** Notes:
*************************************************************************************/
void HAL_I2C_Init(I2C_HandleTypeDef *hi2c)
{
	uint32_t freqrange = 0U;
	uint32_t pclk1 = 0U;


	hi2c->State = HAL_I2C_STATE_BUSY;

	/* Disable the selected I2C peripheral */
	hi2c->Instance->CR1 &=  ~((uint32_t)I2C_CR1_PE);

	/* Get PCLK1 frequency */
	pclk1 = HAL_RCC_GetPCLK1Freq();

	/* Calculate frequency range */
	freqrange = I2C_FREQRANGE(pclk1);

	/*---------------------------- I2Cx CR2 Configuration ----------------------*/
	/* Configure I2Cx: Frequency range */
	hi2c->Instance->CR2 = (uint32_t)freqrange;

	/*---------------------------- I2Cx TRISE Configuration --------------------*/
	/* Configure I2Cx: Rise Time */
	hi2c->Instance->TRISE = I2C_RISE_TIME(freqrange, hi2c->Init.ClockSpeed);

	/*---------------------------- I2Cx CCR Configuration ----------------------*/
	/* Configure I2Cx: Speed */
	hi2c->Instance->CCR = I2C_SPEED(pclk1, hi2c->Init.ClockSpeed, hi2c->Init.DutyCycle);


	/*---------------------------- I2Cx CR1 Configuration ----------------------*/
	/* Configure I2Cx: Generalcall and NoStretch mode */
	hi2c->Instance->CR1 = (uint32_t)(hi2c->Init.GeneralCallMode | hi2c->Init.NoStretchMode);

	/*---------------------------- I2Cx OAR1 Configuration ---------------------*/
	/* Configure I2Cx: Own Address1 and addressing mode */
	hi2c->Instance->OAR1 = (uint32_t)(hi2c->Init.AddressingMode | hi2c->Init.OwnAddress1);

	/*---------------------------- I2Cx OAR2 Configuration ---------------------*/
	/* Configure I2Cx: Dual mode and Own Address2 */
	hi2c->Instance->OAR2 = (uint32_t)(hi2c->Init.DualAddressMode | hi2c->Init.OwnAddress2);

	/*Configure I2C Filters*/
	/*Enable Analog filter, Disable Digital filter*/
	hi2c->Instance->FLTR = (uint32_t)(hi2c->Instance->FLTR & 0xFFFFFFE0U);

	/* Enable the selected I2C peripheral */
	hi2c->Instance->CR1 |=  (uint32_t)I2C_CR1_PE;

	hi2c->ErrorCode = HAL_I2C_ERROR_NONE;
	hi2c->State = HAL_I2C_STATE_READY;
	hi2c->PreviousState = I2C_STATE_NONE;
	hi2c->Mode = HAL_I2C_MODE_NONE;
}

/*************************************************************************************
** HAL_I2C_Master_Transmit - Transmits in master mode an amount of data in blocking mode.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains
** 										the configuration information for I2C module
** 			uint16_t DevAddress - Target device address The device 7 bits address value in
** 								  datasheet must be shifted to the left before calling the interface
** 			uint8_t *pData - Pointer to data buffer
** 			uint16_t Size - Amount of data to be sent
** 			uint32_t Timeout - Timeout duration
**
**
** Returns: HAL_StatusTypeDef  - Status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
	uint32_t tickstart = 0x00U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if((NULL != hi2c) && (NULL != pData))
	{
		/* Init tickstart for timeout management*/
		tickstart = HAL_GetTick();

		if(HAL_I2C_STATE_READY == hi2c->State)
		{
			/* Wait until BUSY flag is reset */
			if(HAL_OK != i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG, tickstart))
			{
				hal_status = HAL_BUSY;
			}
			else
			{
				/* Disable Pos */
				hi2c->Instance->CR1 &= ~I2C_CR1_POS;

				hi2c->State     = HAL_I2C_STATE_BUSY_TX;
				hi2c->Mode      = HAL_I2C_MODE_MASTER;
				hi2c->ErrorCode = HAL_I2C_ERROR_NONE;

				/* Prepare transfer parameters */
				hi2c->pBuffPtr    = pData;
				hi2c->XferCount   = Size;
				hi2c->XferOptions = I2C_NO_OPTION_FRAME;
				hi2c->XferSize    = hi2c->XferCount;

				/* Send Slave Address */
				if(HAL_OK != i2c_master_request_write(hi2c, DevAddress, Timeout, tickstart))
				{
					if(HAL_I2C_ERROR_AF == hi2c->ErrorCode)
					{
						hal_status = HAL_ERROR;
					}
					else
					{
						hal_status = HAL_TIMEOUT;
					}
				}
			}

			/* Clear ADDR flag */
			HAL_I2C_CLEAR_ADDRFLAG(hi2c);

			while ((hi2c->XferSize > 0U) && (HAL_OK == hal_status))
			{
				/* Wait until TXE flag is set */
				if (HAL_OK != i2c_wait_on_txe_flag_until_timeout(hi2c, Timeout, tickstart))
				{
					if (HAL_I2C_ERROR_AF == hi2c->ErrorCode)
					{
						/* Generate Stop */
						hi2c->Instance->CR1 |= I2C_CR1_STOP;
						hal_status = HAL_ERROR;
					}
					else
					{
						hal_status = HAL_TIMEOUT;
					}
				}
				if (HAL_OK == hal_status)
				{
					/* Write data to DR */
					hi2c->Instance->DR = (*hi2c->pBuffPtr++);
					hi2c->XferCount--;
					hi2c->XferSize--;

					if ((SET == HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_BTF)) && (0U != hi2c->XferSize))
					{
						/* Write data to DR */
						hi2c->Instance->DR = (*hi2c->pBuffPtr++);
						hi2c->XferCount--;
						hi2c->XferSize--;
					}

					/* Wait until BTF flag is set */
					if (HAL_OK != i2c_wait_on_btf_flag_until_timeout(hi2c, Timeout, tickstart))
					{
						if (HAL_I2C_ERROR_AF == hi2c->ErrorCode)
						{
							/* Generate Stop */
							hi2c->Instance->CR1 |= I2C_CR1_STOP;
							hal_status = HAL_ERROR;
						}
						else
						{
							hal_status = HAL_TIMEOUT;
						}
					}
				}
			}
			/*Generate stop bit by demand*/
			if(HAL_OK == hal_status)
			{
				/* Generate Stop */
				hi2c->Instance->CR1 |= I2C_CR1_STOP;
			}
			hi2c->State = HAL_I2C_STATE_READY;
			hi2c->Mode = HAL_I2C_MODE_NONE;
		}
		else
		{
			hal_status = HAL_BUSY;
		}
	}
	else
	{
		/*Input parameters check failed*/
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
** HAL_I2C_Master_Receive - Receives in master mode an amount of data in blocking mode.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains
** 										the configuration information for I2C module
** 			uint16_t DevAddress - Target device address The device 7 bits address value in
** 								  datasheet must be shifted to the left before calling the interface
** 			uint8_t *pData - Pointer to data buffer
** 			uint16_t Size - Amount of data to be sent
** 			uint32_t Timeout - Timeout duration
**
**
** Returns: HAL_StatusTypeDef  - Status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
{
	uint32_t tickstart = 0x00U;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if((NULL != hi2c) && (NULL != pData))
	{
		/* Init tickstart for timeout management*/
		tickstart = HAL_GetTick();

		if(HAL_I2C_STATE_READY == hi2c->State)
		{
			/* Wait until BUSY flag is reset */
			if(HAL_OK != i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_BUSY, SET, I2C_TIMEOUT_BUSY_FLAG, tickstart))
			{
				hal_status = HAL_BUSY;
			}
			else
			{
				/* Disable Pos */
				hi2c->Instance->CR1 &= ~I2C_CR1_POS;

				hi2c->State       = HAL_I2C_STATE_BUSY_RX;
				hi2c->Mode        = HAL_I2C_MODE_MASTER;
				hi2c->ErrorCode   = HAL_I2C_ERROR_NONE;

				/* Prepare transfer parameters */
				hi2c->pBuffPtr    = pData;
				hi2c->XferCount   = Size;
				hi2c->XferOptions = I2C_NO_OPTION_FRAME;
				hi2c->XferSize    = hi2c->XferCount;

				/* Send Slave Address */
				if(HAL_OK != i2c_master_request_read(hi2c, DevAddress, Timeout, tickstart))
				{
					if(HAL_I2C_ERROR_AF == hi2c->ErrorCode)
					{
						hal_status = HAL_ERROR;
					}
					else
					{
						hal_status = HAL_TIMEOUT;
					}
				}
			}

			if(HAL_OK == hal_status)
			{
				if(0U == hi2c->XferSize)
				{
					/* Clear ADDR flag */
					HAL_I2C_CLEAR_ADDRFLAG(hi2c);

					/* Generate Stop */
					hi2c->Instance->CR1 |= I2C_CR1_STOP;
				}
				else if(1U == hi2c->XferSize)
				{
					/* Disable Acknowledge */
					hi2c->Instance->CR1 &= ~I2C_CR1_ACK;

					/* Clear ADDR flag */
					HAL_I2C_CLEAR_ADDRFLAG(hi2c);

					/* Generate Stop */
					hi2c->Instance->CR1 |= I2C_CR1_STOP;
				}
				else if(2U == hi2c->XferSize)
				{
					/* Disable Acknowledge */
					hi2c->Instance->CR1 &= ~I2C_CR1_ACK;

					/* Enable Pos */
					hi2c->Instance->CR1 |= I2C_CR1_POS;

					/* Clear ADDR flag */
					HAL_I2C_CLEAR_ADDRFLAG(hi2c);
				}
				else
				{
					/* Enable Acknowledge */
					hi2c->Instance->CR1 |= I2C_CR1_ACK;

					/* Clear ADDR flag */
					HAL_I2C_CLEAR_ADDRFLAG(hi2c);
				}
			}

			if(HAL_OK == hal_status)
			{
				while((hi2c->XferSize > 0U) && (HAL_OK == hal_status))
				{
					if(hi2c->XferSize <= 3U)
					{
						/* One byte */
						if(1U == hi2c->XferSize)
						{
							/* Wait until RXNE flag is set */
							if(HAL_OK != i2c_wait_on_rxne_flag_until_timeout(hi2c, Timeout, tickstart))
							{
								if(HAL_I2C_ERROR_TIMEOUT == hi2c->ErrorCode)
								{
									hal_status = HAL_TIMEOUT;
								}
								else
								{
									hal_status = HAL_ERROR;
								}
							}
							if(HAL_OK == hal_status)
							{
								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;
							}
						}
						/* Two bytes */
						else if(2U == hi2c->XferSize)
						{
							/* Wait until BTF flag is set */
							if(HAL_OK != i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_BTF, RESET, Timeout, tickstart))
							{
								hal_status = HAL_TIMEOUT;
							}
							if(HAL_OK == hal_status)
							{
								/* Generate Stop */
								hi2c->Instance->CR1 |= I2C_CR1_STOP;

								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;

								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;
							}
						}
						/* 3 Last bytes */
						else
						{
							/* Wait until BTF flag is set */
							if(HAL_OK != i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_BTF, RESET, Timeout, tickstart))
							{
								hal_status = HAL_TIMEOUT;
							}
							if(HAL_OK == hal_status)
							{
								/* Disable Acknowledge */
								hi2c->Instance->CR1 &= ~I2C_CR1_ACK;

								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;

								/* Wait until BTF flag is set */
								if(i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_BTF, RESET, Timeout, tickstart) != HAL_OK)
								{
									hal_status = HAL_TIMEOUT;
								}
							}
							if(HAL_OK == hal_status)
							{
								/* Generate Stop */
								hi2c->Instance->CR1 |= I2C_CR1_STOP;

								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;

								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;
							}
						}
					}
					else
					{
						/* Wait until RXNE flag is set */
						if(HAL_OK != i2c_wait_on_rxne_flag_until_timeout(hi2c, Timeout, tickstart))
						{
							if(HAL_I2C_ERROR_TIMEOUT == hi2c->ErrorCode)
							{
								hal_status = HAL_TIMEOUT;
							}
							else
							{
								hal_status = HAL_ERROR;
							}
						}
						if(HAL_OK == hal_status)
						{
							/* Read data from DR */
							(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
							hi2c->XferSize--;
							hi2c->XferCount--;

							if(SET == HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_BTF))
							{
								/* Read data from DR */
								(*hi2c->pBuffPtr++) = hi2c->Instance->DR;
								hi2c->XferSize--;
								hi2c->XferCount--;
							}
						}
					}
				}/*while ednds*/
			}
			hi2c->State = HAL_I2C_STATE_READY;
			hi2c->Mode = HAL_I2C_MODE_NONE;
		}
		else
		{
			hal_status = HAL_BUSY;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
** i2c_master_request_write - This function handles I2C master write request.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
** 			uint16_t dev_address - Target device address The device 7 bits address value
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_master_request_write(I2C_HandleTypeDef *hi2c, uint16_t dev_address, uint32_t timeout, uint32_t tick_start)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if(NULL != hi2c)
	{
		/* Declaration of temporary variable to prevent undefined behavior of volatile usage */
		uint32_t CurrentXferOptions = hi2c->XferOptions;

		/* Generate Start condition if first transfer */
		if((CurrentXferOptions == I2C_FIRST_AND_LAST_FRAME) || (CurrentXferOptions == I2C_FIRST_FRAME) || (CurrentXferOptions == I2C_NO_OPTION_FRAME))
		{
			/* Generate Start */
			hi2c->Instance->CR1 |= I2C_CR1_START;
		}
		else if(hi2c->PreviousState == I2C_STATE_MASTER_BUSY_RX)
		{
			/* Generate ReStart */
			hi2c->Instance->CR1 |= I2C_CR1_START;
		}

		/* Wait until SB flag is set */
		if(HAL_OK != i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_SB, RESET, timeout, tick_start))
		{
			hal_status = HAL_TIMEOUT;
		}

		if(HAL_OK == hal_status)
		{
			/* Send slave address in I2C_ADDRESSINGMODE_7BIT */
			hi2c->Instance->DR = I2C_7BIT_ADD_WRITE(dev_address);


			/* Wait until ADDR flag is set */
			if(HAL_OK != i2c_wait_on_master_address_flag_until_timeout(hi2c, I2C_FLAG_ADDR, timeout, tick_start))
			{
				if(hi2c->ErrorCode == HAL_I2C_ERROR_AF)
				{
					hal_status = HAL_ERROR;
				}
				else
				{
					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}


/*************************************************************************************
** i2c_master_request_read - Master sends target device address for read request.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
** 			uint16_t dev_address - Target device address The device 7 bits address value
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_master_request_read(I2C_HandleTypeDef *hi2c, uint16_t dev_address, uint32_t timeout, uint32_t tick_start)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if(NULL != hi2c)
	{
		/* Declaration of temporary variable to prevent undefined behavior of volatile usage */
		uint32_t CurrentXferOptions = hi2c->XferOptions;

		/* Enable Acknowledge */
		hi2c->Instance->CR1 |= I2C_CR1_ACK;

		/* Generate Start condition if first transfer */
		if((I2C_FIRST_AND_LAST_FRAME == CurrentXferOptions) || (I2C_FIRST_FRAME == CurrentXferOptions)  || (I2C_NO_OPTION_FRAME == CurrentXferOptions))
		{
			/* Generate Start */
			hi2c->Instance->CR1 |= I2C_CR1_START;
		}
		if(I2C_STATE_MASTER_BUSY_TX == hi2c->PreviousState)
		{
			/* Generate Start */
			hi2c->Instance->CR1 |= I2C_CR1_START;
		}

		/* Wait until SB flag is set */
		if(HAL_OK != i2c_wait_on_flag_until_timeout(hi2c, I2C_FLAG_SB, RESET, timeout, tick_start))
		{
			hal_status = HAL_TIMEOUT;
		}

		if(HAL_OK == hal_status)
		{
			/* Send slave address (I2C_ADDRESSINGMODE_7BIT)*/
			hi2c->Instance->DR = I2C_7BIT_ADD_READ(dev_address);



			/* Wait until ADDR flag is set */
			if(HAL_OK != i2c_wait_on_master_address_flag_until_timeout(hi2c, I2C_FLAG_ADDR, timeout, tick_start))
			{
				if(HAL_I2C_ERROR_AF == hi2c->ErrorCode)
				{
					hal_status = HAL_ERROR;
				}
				else
				{
					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}

	return hal_status;
}


/*************************************************************************************
** i2c_wait_on_flag_until_timeout - This function handles I2C Communication Timeout.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the configuration information for I2C module
** 			uint32_t flag - Specifies the I2C flag to check
** 			uint8_t req_flag_state - The new Flag status (SET or RESET).
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_wait_on_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t flag, uint8_t req_flag_state, uint32_t timeout, uint32_t tick_start)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t flag_state = (uint8_t)RESET;

	/*Check input parameters*/
	if((NULL != hi2c) && (TRUE == IS_I2C_FLAG(flag)) && ((SET == req_flag_state) ||(RESET == req_flag_state)))
	{
		/*Check flag current state*/
		if((uint32_t)TRUE == (uint32_t)HAL_I2C_GET_FLAG(hi2c, flag))
		{
			flag_state = (uint8_t)SET;
		}
		else
		{
			flag_state = (uint8_t)RESET;
		}
		/* Wait until flag is set */
		while((flag_state == req_flag_state) && (HAL_OK == hal_status))
		{
			if((HAL_GetTick() - tick_start ) > timeout)
			{
				hi2c->PreviousState = I2C_STATE_NONE;
				hi2c->State= HAL_I2C_STATE_READY;
				hi2c->Mode = HAL_I2C_MODE_NONE;

				hal_status = HAL_TIMEOUT;
			}
			/*Check flag current state*/
			if(TRUE == HAL_I2C_GET_FLAG(hi2c, flag))
			{
				flag_state = (uint8_t)SET;
			}
			else
			{
				flag_state = (uint8_t)RESET;
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}


/*************************************************************************************
** i2c_wait_on_master_address_flag_until_timeout - This function handles I2C Communication Timeout for Master addressing phase.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
** 			uint32_t flag - Specifies the I2C flag to check.
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_wait_on_master_address_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t flag, uint32_t timeout, uint32_t tick_start)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((NULL != hi2c) && (TRUE == IS_I2C_FLAG(flag)))
	{
		while(((uint32_t)RESET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, flag)) && (HAL_OK == hal_status))
		{
			if((uint32_t)SET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_AF))
			{
				/* Generate Stop */
				hi2c->Instance->CR1 |= I2C_CR1_STOP;

				/* Clear AF Flag */
				hi2c->Instance->SR1 =(uint32_t)( ~((uint32_t)I2C_FLAG_AF & (uint32_t)I2C_FLAG_MASK));

				hi2c->ErrorCode = HAL_I2C_ERROR_AF;
				hi2c->PreviousState = I2C_STATE_NONE;
				hi2c->State= HAL_I2C_STATE_READY;

				hal_status = HAL_ERROR;
			}

			/* Check for the Timeout */
			if((HAL_GetTick() - tick_start ) > timeout)
			{
				hi2c->PreviousState = I2C_STATE_NONE;
				hi2c->State= HAL_I2C_STATE_READY;

				hal_status = HAL_TIMEOUT;
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}


/*************************************************************************************
** i2c_wait_on_txe_flag_until_timeout - This function handles I2C Communication Timeout for specific usage of TXE flag.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_wait_on_txe_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t timeout, uint32_t tick_start)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if(NULL != hi2c)
	{
		while(((uint32_t)RESET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_TXE)) && (HAL_OK == hal_status))
		{
			/* Check if a NACK is detected */
			hal_status = i2c_is_acknowledge_failed(hi2c);

			if(HAL_OK == hal_status)
			{
				/* Check for the Timeout */
				if((HAL_GetTick()-tick_start) > timeout)
				{
					hi2c->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
					hi2c->PreviousState = I2C_STATE_NONE;
					hi2c->State= HAL_I2C_STATE_READY;

					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}


/*************************************************************************************
** i2c_wait_on_btf_flag_until_timeout - This function handles I2C Communication Timeout for specific usage of TXE flag.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_wait_on_btf_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t timeout, uint32_t tick_start)
{  
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if(NULL != hi2c)
	{
		while(((uint32_t)RESET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_BTF)) && (HAL_OK == hal_status))
		{
			/* Check if a NACK is detected */
			hal_status = i2c_is_acknowledge_failed(hi2c);

			/* Check for the Timeout */
			if(HAL_OK == hal_status)
			{
				if((HAL_GetTick()-tick_start) > timeout)
				{
					hi2c->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
					hi2c->PreviousState = I2C_STATE_NONE;
					hi2c->State= HAL_I2C_STATE_READY;

					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
** i2c_wait_on_rxne_flag_until_timeout - This function handles I2C Communication Timeout for specific usage of RXNE flag.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
** 			uint32_t timeout - Timeout duration
** 			uint32_t tick_start - Tick start value
**
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_wait_on_rxne_flag_until_timeout(I2C_HandleTypeDef *hi2c, uint32_t timeout, uint32_t tick_start)
{  
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if(NULL != hi2c)
	{
		while(((uint32_t)RESET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_RXNE)) && (HAL_OK == hal_status))
		{
			/* Check if a STOPF is detected */
			if((uint32_t)SET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_STOPF))
			{
				/* Clear STOP Flag */
				hi2c->Instance->SR1 =(uint32_t)( ~((uint32_t)I2C_FLAG_STOPF & (uint32_t)I2C_FLAG_MASK));

				hi2c->ErrorCode = HAL_I2C_ERROR_NONE;
				hi2c->PreviousState = I2C_STATE_NONE;
				hi2c->State= HAL_I2C_STATE_READY;

				hal_status = HAL_ERROR;
			}
			if(HAL_OK == hal_status)
			{
				/* Check for the Timeout */
				if((HAL_GetTick()-tick_start) > timeout)
				{
					hi2c->ErrorCode |= HAL_I2C_ERROR_TIMEOUT;
					hi2c->State= HAL_I2C_STATE_READY;

					hal_status = HAL_TIMEOUT;
				}
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}


	return hal_status;
}


/*************************************************************************************
** i2c_is_acknowledge_failed - This function handles Acknowledge failed detection during an I2C Communication.
**
** Params : I2C_HandleTypeDef *hi2c - Pointer to a I2C_HandleTypeDef structure that contains the
**                                    configuration information for I2C module
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef i2c_is_acknowledge_failed(I2C_HandleTypeDef *hi2c)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if(NULL != hi2c)
	{
		if((uint32_t)SET == (uint32_t)HAL_I2C_GET_FLAG(hi2c, I2C_FLAG_AF))
		{
			/* Clear NACKF Flag */
			hi2c->Instance->SR1 =(uint32_t)( ~((uint32_t)I2C_FLAG_AF & (uint32_t)I2C_FLAG_MASK));

			hi2c->ErrorCode = HAL_I2C_ERROR_AF;
			hi2c->PreviousState = I2C_STATE_NONE;
			hi2c->State= HAL_I2C_STATE_READY;

			hal_status = HAL_ERROR;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}

	return hal_status;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
